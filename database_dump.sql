-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (armv7l)
--
-- Host: localhost    Database: rooftopserverdatabase
-- ------------------------------------------------------
-- Server version	5.5.41-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `rooftopserverdatabase`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `rooftopserverdatabase` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `rooftopserverdatabase`;

--
-- Table structure for table `access`
--

DROP TABLE IF EXISTS `access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `inputgroup` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `inputgroup` (`inputgroup`),
  CONSTRAINT `access_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
  CONSTRAINT `access_ibfk_2` FOREIGN KEY (`inputgroup`) REFERENCES `inputgroup` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access`
--

LOCK TABLES `access` WRITE;
/*!40000 ALTER TABLE `access` DISABLE KEYS */;
INSERT INTO `access` VALUES (39,1,1),(40,1,4),(41,1,5),(52,2,1),(53,2,2),(54,2,3),(55,2,4),(64,3,1),(65,3,2),(66,3,4),(67,3,5),(74,4,1),(75,4,3);
/*!40000 ALTER TABLE `access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `type` text NOT NULL,
  `left` text NOT NULL,
  `top` text NOT NULL,
  `width` text NOT NULL,
  `height` text NOT NULL,
  `method` text,
  `open` text,
  `file` text,
  `file2` text,
  `dir` text,
  `min` text,
  `max` text,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `area_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `imageinput` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,11,'clickable','0.0244','0.0368','0.2195','0.1838',NULL,'A','sym_bel.svg',NULL,NULL,NULL,NULL,NULL),(2,11,'clickable','0.5122','0.0368','0.4634','0.1838',NULL,'B','sym_bel.svg',NULL,NULL,NULL,NULL,NULL),(3,11,'clickable','0.0244','0.2555','0.2195','0.0772',NULL,'C','sym_fac.svg',NULL,NULL,NULL,NULL,NULL),(4,11,'clickable','0.5122','0.2555','0.4634','0.0772',NULL,'D','sym_fac.svg',NULL,NULL,NULL,NULL,NULL),(5,11,'clickable','0.0244','0.3676','0.2195','0.4779',NULL,'E','sym_bel.svg',NULL,NULL,NULL,NULL,NULL),(6,11,'clickable','0.2683','0.4596','0.1098','0.2941',NULL,'F','sym_bel.svg',NULL,NULL,NULL,NULL,NULL),(7,11,'clickable','0.5854','0.3676','0.3902','0.4779',NULL,'Wohnzimmerbeleuchtung','sym_bel.svg',NULL,NULL,NULL,NULL,NULL),(8,11,'clickable','0.4024','0.6801','0.1585','0.1654',NULL,'H','sym_bel.svg',NULL,NULL,NULL,NULL,NULL),(9,11,'clickable','0.0244','0.8824','0.9512','0.0772',NULL,'Fassadensteuerung Suedseite','sym_fac.svg',NULL,NULL,NULL,NULL,NULL),(10,12,'clickable','0.2277','-0.0288','0.0911','0.0885','lamp1',NULL,'lampe_aus.svg','lampe_an.svg',NULL,NULL,NULL,NULL),(11,12,'clickable','0.503','-0.0288','0.0911','0.0885','lamp2',NULL,'lampe_aus.svg','lampe_an.svg',NULL,NULL,NULL,NULL),(12,12,'clickable','0.7782','-0.0288','0.0911','0.0885','lamp3',NULL,'lampe_aus.svg','lampe_an.svg',NULL,NULL,NULL,NULL),(13,12,'clickable','0.2277','0.9404','0.0911','0.0885','lamp4',NULL,'lampe_aus.svg','lampe_an.svg',NULL,NULL,NULL,NULL),(14,12,'clickable','0.503','0.9404','0.0911','0.0885','lamp5',NULL,'lampe_aus.svg','lampe_an.svg',NULL,NULL,NULL,NULL),(15,12,'clickable','0.7782','0.9404','0.0911','0.0885','lamp6',NULL,'lampe_aus.svg','lampe_an.svg',NULL,NULL,NULL,NULL),(16,12,'slidable','0.404','0.4558','0.0911','0.0885','lamp7',NULL,'lampe_aus.svg','lampe_an.svg',NULL,NULL,NULL,'0.3'),(17,12,'slidable','0.602','0.4558','0.0911','0.0885','lamp8',NULL,'lampe_aus.svg','lampe_an.svg',NULL,NULL,NULL,'0.7'),(18,12,'slidable','0.0515','0.225','0.1','0.5519','lamp9',NULL,'led_r_aus.svg','led_r_an.svg','right',NULL,NULL,'0.5'),(19,12,'slidable','0.8911','0.0923','0.0792','0.8173','lamp10',NULL,'led_l_aus.svg','led_l_an.svg','left',NULL,NULL,'0.5'),(20,13,'slidable','0.0012','0.1519','0.1245','0.7778','fassade1',NULL,'fassade.svg',NULL,'down',NULL,NULL,'0.8'),(21,13,'slidable','0.1258','0.1519','0.1245','0.7778','fassade2',NULL,'fassade.svg',NULL,'down',NULL,NULL,'0.1'),(22,13,'slidable','0.2503','0.1519','0.1245','0.7778','fassade3',NULL,'fassade.svg',NULL,'down',NULL,NULL,'0.29'),(23,13,'slidable','0.3748','0.1519','0.1245','0.7778','fassade4',NULL,'fassade.svg',NULL,'down',NULL,NULL,'0.68'),(24,13,'slidable','0.4994','0.1519','0.1245','0.7778','fassade5',NULL,'fassade.svg',NULL,'down',NULL,NULL,'0.54'),(25,13,'slidable','0.6239','0.1519','0.1245','0.7778','fassade6',NULL,'fassade.svg',NULL,'down',NULL,NULL,'0.33'),(26,13,'slidable','0.7484','0.1519','0.1245','0.7778','fassade7',NULL,'fassade.svg',NULL,'down',NULL,NULL,'0.69'),(27,13,'slidable','0.873','0.1519','0.1245','0.7778','fassade8',NULL,'fassade.svg',NULL,'down',NULL,NULL,'0.99');
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imageinput`
--

DROP TABLE IF EXISTS `imageinput`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imageinput` (
  `id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `file` text NOT NULL,
  `name` text,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `imageinput_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `inputgroup` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imageinput`
--

LOCK TABLES `imageinput` WRITE;
/*!40000 ALTER TABLE `imageinput` DISABLE KEYS */;
INSERT INTO `imageinput` VALUES (11,3,'grundriss.svg','Grundriss des Hauses'),(12,4,'beleuchtung_wz.svg','Beleuchtung'),(13,5,'fassade_sued.svg','Fassadensteuerung');
/*!40000 ALTER TABLE `imageinput` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `input`
--

DROP TABLE IF EXISTS `input`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `input` (
  `id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `type` text NOT NULL,
  `method` text NOT NULL,
  `name` text,
  `min` text,
  `max` text,
  `value` text,
  `unit` text,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `input_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `inputgroup` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `input`
--

LOCK TABLES `input` WRITE;
/*!40000 ALTER TABLE `input` DISABLE KEYS */;
INSERT INTO `input` VALUES (0,1,'checkbox','schalteLampe01','Deckenbeleuchtung 1',NULL,NULL,'true',NULL),(1,1,'checkbox','schalteLampe02','Deckenbeleuchtung 2',NULL,NULL,'false',NULL),(2,1,'checkbox','schalteLampe03','Stehlampe (Schalter)',NULL,NULL,'true',NULL),(3,1,'slider','dimmLampe03','Stehlampe (Dimmer)','0','100','70',NULL);
/*!40000 ALTER TABLE `input` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inputgroup`
--

DROP TABLE IF EXISTS `inputgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inputgroup` (
  `id` int(11) NOT NULL,
  `groupname` text NOT NULL,
  `transmit` text NOT NULL,
  `category` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inputgroup`
--

LOCK TABLES `inputgroup` WRITE;
/*!40000 ALTER TABLE `inputgroup` DISABLE KEYS */;
INSERT INTO `inputgroup` VALUES (1,'Beleuchtung Wohnzimmer','onchange','Beleuchtung'),(2,'Beleuchtung Schlafzimmer','confirm','Beleuchtung'),(3,'Grundriss des Hauses','onchange','Haussteuerung'),(4,'Wohnzimmerbeleuchtung','onchange','Haussteuerung'),(5,'Fassadensteuerung Suedseite','confirm','Haussteuerung');
/*!40000 ALTER TABLE `inputgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `pwhash` text NOT NULL,
  `role` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','1000:d1baa221ad81ee28216d30bd31bc536636350d836ad2aabb:9bc1df933ba7b7e5b338acef1004e3588c91f8be49eb2581','1'),(2,'user1','1000:e8b840f1c51328610f2603ceeddd09e93bfcdcaef373bd12:8396d03937e38b7d054809e388fcc77f94b5969f7d445071','3'),(3,'user2','1000:398efdb048caf0b86530a6d8b79d17c5aa81487b3ac8c8ac:4da3f029d70f4fb7d1e4f18e4c448e966c5a1bac0b601a6c','2'),(4,'user3','1000:42a8dab7debfdcb040889f06b5eedf708de2c12436937629:c9008bfbf798ec3ab2ed026dcdea751a2867ae28777f2320','3'),(5,'user4','1000:f0223e68650a172398076c4c613754e14243af287e112dfb:649947fe6e07df8fbfb2409c99f95be3e334442625b21dd3','2');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;