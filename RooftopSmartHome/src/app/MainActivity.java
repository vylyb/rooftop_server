package app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import rooftop.app.R;
import share.exceptions.EncodingException;
import share.inputs.InputFactory;
import share.inputs.InputGroup;
import share.message.JsonNetworkMessage;
import share.message.NetworkMessage;
import share.user.UserAccount;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Toast;
import app.client.TcpClient;
import app.view.ExpandableListAdapter;
import app.view.activity.InputGroupActivity;
import app.view.activity.SettingsActivity;

/**
 * first {@link Activity} that is shown when the app starts. contains the
 * controls for login and an {@link ExpandableListView} with the input groups
 * 
 * @author Philipp Hempel
 *
 */
public class MainActivity extends ActionBarActivity {

	public final static int RESULT_SETTINGS = 1;
	private final static int MSG_SHOW_TOAST = 2, MSG_CLIENT_CONNECTED = 3, MSG_CLIENT_DISCONNECTED = 4,
			MSG_SHOW_INPUT_GROUPS = 5;
	public final static String INPUT_GROUP_KEY = "inputgroupkey";
	public final static String INPUT_IMAGE_FILE = "inputImageFile";
	public static final int ORANGE = Color.parseColor("#f58220");
	/**
	 * help variable to be able to access this activity object in a static way.
	 * used to e.g. send a toast
	 */
	public static MainActivity thisActivity = null;

	/**
	 * instance of the {@link TcpClient}
	 */
	public static TcpClient client;

	/**
	 * save the preferences
	 */
	private static SharedPreferences preferences;

	/**
	 * help variable to save the IP of the server
	 */
	private String serverIp;

	/**
	 * help variable to save the port of the server
	 */
	private String serverPort;

	/**
	 * {@link ExpandableListView} widget that contains the data for the
	 * different input groups. It is dynamically filled with information from
	 * the {@link JSONObject}s with user information that were received from the
	 * server.
	 */
	private ExpandableListView listView;

	/**
	 * list of strings with the names of the headers in {@link #listView}
	 */
	private ArrayList<String> listDataHeader;

	/**
	 * list of string lists with the data of the children in {@link #listView}
	 */
	private HashMap<String, List<String>> listDataChild;

	/**
	 * indicates whether the {@link TcpClient} is connected to the server
	 */
	protected static boolean connected = false;

	/**
	 * hash map of {@link InputGroup} objects that are received from the server
	 * and are displayed in dynamic activities, the key is a base64 encoding of
	 * the category and the group name
	 */
	protected HashMap<String, InputGroup> inputGroups;

	/**
	 * the user role the account of the current user has in the server (admin,
	 * regular, guest)
	 */
	private int userRole = -1;

	/**
	 * hash map that is used as a cache for image data that has already been
	 * loaded from the server. maps the filename to the content
	 */
	public static HashMap<String, String> savedImages;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		setContentView(R.layout.activity_main);

		if(savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
		}

		/*
		 * save this activity object to allow static access
		 */
		thisActivity = this;

		/*
		 * load the preferences
		 */
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		updatePreferences();

		savedImages = new HashMap<String, String>();

		showProgressBar(false);

	}

	/**
	 * @return {@link ArrayList} with all entries from the hash map
	 *         {@link #inputGroups}
	 */
	public static ArrayList<InputGroup> getInputGroups() {
		ArrayList<InputGroup> list = new ArrayList<InputGroup>();
		for(String key : thisActivity.inputGroups.keySet()) {
			list.add(thisActivity.inputGroups.get(key));
		}
		return list;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		/*
		 * Inflate the menu; this adds items to the action bar if it is present.
		 */
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/*
		 * Handle action bar item clicks here. The action bar will automatically
		 * handle clicks on the Home/Up button, so long as you specify a parent
		 * activity in AndroidManifest.xml.
		 */
		int id = item.getItemId();
		if(id == R.id.action_settings) {
			Intent i = new Intent(this, SettingsActivity.class);
			startActivityForResult(i, RESULT_SETTINGS);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int request, int result, Intent data) {

		switch (result){
		case RESULT_SETTINGS:
			updatePreferences();
			break;

		default:
			break;
		}
	}

	/**
	 * method to load the preferences which have been set in the
	 * {@link SettingsActivity}
	 */
	private void updatePreferences() {
		serverIp = preferences.getString(getString(R.string.settingServerIp), "");
		serverPort = preferences.getString(getString(R.string.settingServerPort), "");

		/*
		 * TODO use the saved preferences!!!
		 */
		serverIp = "192.168.2.106";
		serverPort = "4444";
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.activity_main, container, false);
			return rootView;
		}
	}

	/**
	 * method which is called when the Button 'Connect' is clicked. Creates a
	 * new {@link TcpClient} object
	 * 
	 * @param view
	 */
	public void onClickConnect(View view) {

		showProgressBar(true);

		if(connected) {
			client.disconnect();
			client = null;
		} else {
			updatePreferences();
			info("Connection to " + serverIp + ":" + serverPort);
			client = new TcpClient(serverIp, serverPort);
		}

		showProgressBar(false);
	}

	/**
	 * displays a progress bar in the action bar
	 */
	private void showProgressBar(boolean show) {
		((ActionBarActivity) thisActivity).setProgressBarIndeterminateVisibility(show);
	}

	/**
	 * helper method to write an info message to the LogCat
	 */
	public static void info(String message) {
		Log.i("LOG_INFO", message);
	}

	/**
	 * helper method to write the result of the toString() method of the given
	 * object to the LogCat
	 */
	public static void info(Object obj) {
		if(obj == null)
			Log.i("LOG_INFO", "null");
		else
			Log.i("LOG_INFO", obj.toString());
	}

	/**
	 * helper method to write an error message to the LogCat
	 */
	public static void error(String message) {
		Log.e("LOG_ERROR", message);
	}

	/**
	 * helper method to write information about the given {@link Exception} to
	 * the LogCat
	 */
	public static void exceptionInfo(Exception e) {
		exceptionInfo("", e);
	}

	/**
	 * helper method to write information about the given {@link Exception} to
	 * the LogCat, also displays the given {@link String}
	 */
	public static void exceptionInfo(String message, Exception e) {
		error(message + " (" + e.getClass().getSimpleName() + ": " + e.getMessage() + ")");
		e.printStackTrace();
	}

	private static Handler messageHandler = new Handler() {

		@Override
		public void handleMessage(android.os.Message msg) {
			if(msg.what == MSG_SHOW_TOAST) {
				Toast.makeText(thisActivity.getBaseContext(), (String) msg.obj, Toast.LENGTH_SHORT).show();
			}
			/*
			 * message sent by the client if it has been successfully connected
			 */
			else if(msg.what == MSG_CLIENT_CONNECTED) {
				info((String) msg.obj);
				sendToast((String) msg.obj);
				/*
				 * enable views that can only be used when the client is
				 * connected
				 */

				connected = true;

				thisActivity.setProgressBarIndeterminateVisibility(false);
			}
			/*
			 * message sent by the client if it has been disconnected
			 */
			else if(msg.what == MSG_CLIENT_DISCONNECTED) {
				info((String) msg.obj);
				sendToast((String) msg.obj);
				/*
				 * disable views that can only be used when the client is
				 * connected
				 */

				clearListView();
				setUserRole(-1);
				connected = false;

				thisActivity.setProgressBarIndeterminateVisibility(false);
			} else if(msg.what == MSG_SHOW_INPUT_GROUPS) {

				thisActivity.listView = (ExpandableListView) thisActivity.findViewById(R.id.listViewInputGroups);

				thisActivity.listView.removeAllViewsInLayout();

				thisActivity.listDataHeader = new ArrayList<String>();
				thisActivity.listDataChild = new HashMap<String, List<String>>();

				/*
				 * generate an array of input groups
				 */
				try {
					info("Received Input Configuration");
					JSONObject json = new JSONObject((String) msg.obj);

					HashMap<Integer, InputGroup> inputGroupMap = InputFactory.createInputGroupListFromJson(json);

					thisActivity.inputGroups = new HashMap<String, InputGroup>();
					for(Integer id : inputGroupMap.keySet()) {
						InputGroup group = inputGroupMap.get(id);
						/*
						 * if the InputValueGroup object contains a category add
						 * the group under this category to the expandable list,
						 * else add the group under a general category
						 */
						String category = group.getCategory();
						if(category == null) {
							category = "Other Controls";
						}

						if(thisActivity.listDataChild.get(category) == null) {
							thisActivity.listDataChild.put(category, new ArrayList<String>());
							thisActivity.listDataHeader.add(category);
						}

						/*
						 * add the data to the category
						 */
						thisActivity.listDataChild.get(category).add(group.getName());

						/*
						 * add the input group to the hash map
						 */
						thisActivity.inputGroups.put(group.getName(), group);
					}
				} catch(Exception e) {
					e.printStackTrace();
					exceptionInfo(e);
				}

				/*
				 * clear the list view
				 */
				clearListView();
				/*
				 * fill the list view
				 */
				thisActivity.listView.setAdapter(new ExpandableListAdapter(thisActivity.getBaseContext(),
						thisActivity.listDataHeader, thisActivity.listDataChild));
				/*
				 * set listener(s) for the list view
				 */
				thisActivity.listView.setOnChildClickListener(new OnChildClickListener() {

					@Override
					public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
							int childPosition, long id) {
						try {
							String clickedActivity = thisActivity.listDataChild.get(
									thisActivity.listDataHeader.get(groupPosition)).get(childPosition);

							openInputGroupActivity(clickedActivity);
						} catch(Exception e) {
							exceptionInfo(e);
						}
						return false;
					}
				});

			}
		}
	};

	/**
	 * method which can be called in a static way from everywhere in the app, it
	 * displays a {@link Toast} on screen
	 */
	public static void sendToast(String message) {
		Message msg = new Message();
		msg.what = MSG_SHOW_TOAST;
		msg.obj = message;
		messageHandler.sendMessage(msg);
	}

	public static void openInputGroupActivity(String inputgroupkey) {
		InputGroup group = thisActivity.inputGroups.get(inputgroupkey);
		if(group == null) {
			error("No Object for Group Key " + inputgroupkey);
			return;
		}
		info("Clicked on group " + group.getName());

		thisActivity.showProgressBar(true);
		Intent intent = new Intent(thisActivity, InputGroupActivity.class);
		intent.putExtra(INPUT_GROUP_KEY, inputgroupkey);
		thisActivity.startActivity(intent);
		thisActivity.showProgressBar(false);
	}

	protected static void clearListView() {
		try {
			thisActivity.listView.setAdapter(new ExpandableListAdapter());
		} catch(Exception e) {
		}
	}

	public static void showInputGroups(String configuration) {
		Message message = new Message();
		message.what = MSG_SHOW_INPUT_GROUPS;
		message.obj = configuration;
		messageHandler.sendMessage(message);

	}

	public static void clientConnected() {
		Message msg = new Message();
		msg.what = MSG_CLIENT_CONNECTED;
		msg.obj = "Successfully connected to Server";
		messageHandler.sendMessage(msg);

	}

	public static void clientDisconnected() {
		Message msg = new Message();
		msg.what = MSG_CLIENT_DISCONNECTED;
		msg.obj = "Disconnected from Server";
		messageHandler.sendMessage(msg);
	}

	/**
	 * @param key
	 *            the key under which the {@link InputGroup} is saved
	 * @return the {@link InputGroup} object from {@link #inputGroups} under the
	 *         given key
	 */
	public static InputGroup getInputGroup(String key) {
		return thisActivity.inputGroups.get(key);
	}

	/**
	 * requests the content of the image file from the server
	 * 
	 * @param fileName
	 *            the filename of the image
	 * @return the content of the file
	 * @throws EncodingException
	 */
	public static String requestImageData(String fileName) {
		try {
			JSONObject json = new JSONObject();
			json.put(NetworkMessage.MSG_FILE_CONTENT, fileName);
			NetworkMessage answer = client.sendEncryptedMessage(new JsonNetworkMessage(json));
			if(answer.isJsonMessage()) {
				json = ((JsonNetworkMessage) answer).getJsonObject();
				if(json.has(NetworkMessage.MSG_FILE_CONTENT)) {
					return json.getString(NetworkMessage.MSG_FILE_CONTENT);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * sets the role of the account of the client
	 * 
	 * @param role
	 */
	public static void setUserRole(int role) {
		thisActivity.userRole = role;
	}

	/**
	 * 
	 * @return <code>true</code> if the server sent the role indicator for an
	 *         admin account for this user after login
	 */
	public static boolean isLoggedInAsAdmin() {
		return thisActivity.userRole == UserAccount.ROLE_ADMIN;
	}
}
