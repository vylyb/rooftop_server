package app.view.activity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rooftop.app.R;
import share.exceptions.EncodingException;
import share.inputs.ImageInput;
import share.inputs.Input;
import share.inputs.InputGroup;
import share.inputs.StandardInput;
import share.message.JsonNetworkMessage;
import share.message.NetworkMessage;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import app.MainActivity;
import app.view.drawings.DrawingPanel;
import app.view.drawings.areas.DrawingAreaInterface;
import app.view.drawings.util.FloatPoint;
import app.view.inputs.filter.NumberFieldMaxFilter;
import app.view.inputs.filter.NumberFieldMinFilter;

/**
 * {@link ActionBarActivity} that dynamically displays the {@link StandardInput}
 * objects or a {@link DrawingPanel} with an {@link ImageInput}
 * 
 * @author Philipp Hempel
 *
 */
public class InputGroupActivity extends ActionBarActivity implements OnGestureListener {

	/**
	 * the {@link InputGroup} that will be displayed in this activity
	 */
	protected InputGroup inputGroup;
	/**
	 * the layout that is used to display the inputs below each other
	 */
	protected LinearLayout layout;

	protected DecimalFormat decimalFormatShort, decimalFormatLong;
	/**
	 * get static access to this {@link InputGroupActivity}
	 */
	protected InputGroupActivity thisActivity;
	/**
	 * detector for gestures (ie scroll, fling...)
	 */
	protected GestureDetector gestureDetector;
	/**
	 * if the input group contains an {@link ImageInput}, it will be displayed
	 * in this panel.
	 */
	protected DrawingPanel panel;
	/**
	 * saves if the transmission setting of the {@link InputGroup} is on change.
	 * is once saved in {@link #onCreate(Bundle)} and will later save time since
	 * the decision is made after a {@link String} comparison
	 */
	protected boolean sendOnChange;
	protected LinearLayout.LayoutParams params;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_input_group);

		/*
		 * get the input group to display from the parameters
		 */
		try {
			String groupName = getIntent().getStringExtra(MainActivity.INPUT_GROUP_KEY);

			inputGroup = MainActivity.getInputGroup(groupName);
			setTitle(inputGroup.getName());
			sendOnChange = inputGroup.getTransmit().compareTo(Input.TRANSMIT_ON_CHANGE) == 0;
		} catch(Exception e) {
			return;
		}

		gestureDetector = new GestureDetector(this, this);

		/*
		 * formatting of decimal strings
		 */
		DecimalFormatSymbols decimalStyle = new DecimalFormatSymbols(Locale.US);
		decimalStyle.setDecimalSeparator('.');
		decimalStyle.setGroupingSeparator(',');
		decimalFormatShort = new DecimalFormat("0.0#", decimalStyle);
		decimalFormatLong = new DecimalFormat("0.00", decimalStyle);

		/*
		 * create the vertical linear layout
		 */
		layout = (LinearLayout) findViewById(R.id.inputGroupLayout);
		params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		/*
		 * if the inputs are to be transmitted on confirm, add a button for this
		 * task
		 */
		if(!sendOnChange) {
			Button button = new Button(this);
			LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			buttonParams.setMargins(0, 10, 0, 15);
			button.setLayoutParams(buttonParams);
			button.setText("Send Values To Server");
			button.setBackgroundColor(MainActivity.ORANGE);
			button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					sendInputValuesToServer();
				}
			});

			layout.addView(button);
		}
		params.setMargins(10, 0, 10, 0);

		thisActivity = this;

		for(final Input input : inputGroup.inputsToArray()) {
			createViewForInput(input);
		}

	}

	/**
	 * generates a {@link View} from the given {@link Input} and adds it to the
	 * list layout
	 */
	protected void createViewForInput(final Input input) {
		if(input instanceof StandardInput) {

			MainActivity.info("New " + input.getType() + ", Start Value: " + input.getStartValue());

			LinearLayout inputLayout = new LinearLayout(thisActivity);

			String name = input.getName();
			if(name == null)
				name = input.getRpcMethod();
			TextView textName = new TextView(thisActivity);
			textName.setLayoutParams(params);

			/*
			 * create a view that contains the user input view
			 */
			View interactionView = null;

			switch (input.getType()){

			case Input.TYPE_BUTTON:
				/*
				 * create a button
				 */
				interactionView = new Button(thisActivity);
				((Button) interactionView).setText(name);
				/*
				 * add an action listener for when the button is clicked
				 */
				((Button) interactionView).setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						inputButtonClicked(input);
					}
				});

				inputLayout.addView(interactionView);

				break;

			case Input.TYPE_NUMFIELD:
				/*
				 * create a text input field that accepts decimal numbers
				 */
				interactionView = new EditText(thisActivity);
				((EditText) interactionView).setInputType(InputType.TYPE_CLASS_NUMBER
						| InputType.TYPE_NUMBER_FLAG_DECIMAL);
				/*
				 * set filters for the min and max values if they are specified
				 */
				String range = "";
				if(input.areMinAndMaxValueSet()) {
					range = " (" + decimalFormatShort.format(input.getMinValue()) + " - "
							+ decimalFormatShort.format(input.getMaxValue()) + ")";
				} else if(input.isMinValueSet()) {
					range = " ( > " + decimalFormatShort.format(input.getMinValue()) + ")";
				} else if(input.isMaxValueSet()) {
					range = " ( < " + decimalFormatShort.format(input.getMaxValue()) + ")";
				}

				ArrayList<InputFilter> filters = new ArrayList<InputFilter>();
				if(input.isMinValueSet()) {
					filters.add(new NumberFieldMinFilter(input.getMinValue()));
				}
				if(input.isMaxValueSet()) {
					filters.add(new NumberFieldMaxFilter(input.getMaxValue()));
				}
				InputFilter[] filterList = new InputFilter[filters.size()];
				for(int i = 0; i < filterList.length; i++) {
					filterList[i] = filters.get(i);
				}
				((EditText) interactionView).setFilters(filterList);

				/*
				 * set the start value if it is specified, make sure it is a
				 * correct decimal value, else just use 0.0
				 */
				double startValue = 0.0;
				if(input.isStartValueSet()) {
					try {
						startValue = Double.parseDouble(input.getStartValue());
					} catch(NumberFormatException e) {
						e.printStackTrace();
					}
					((EditText) interactionView).setText(decimalFormatLong.format(startValue));
				}

				/*
				 * add a listener for when the user hits 'enter'
				 */
				((EditText) interactionView).setOnKeyListener(new OnKeyListener() {

					@Override
					public boolean onKey(View view, int keyCode, KeyEvent event) {
						return editorAction((TextView) view, keyCode, event, input);
					}
				});

				((EditText) interactionView).setSelectAllOnFocus(true);

				textName.setText(name + range);
				inputLayout.addView(textName);

				inputLayout.addView(interactionView);

				break;

			case Input.TYPE_TEXTFIELD:
				/*
				 * create a text input field that accepts any text
				 */
				textName.setText(name);
				inputLayout.addView(textName);

				interactionView = new EditText(thisActivity);
				((EditText) interactionView).setInputType(InputType.TYPE_CLASS_TEXT);

				/*
				 * add a listener for when the user hits 'enter'
				 */
				((EditText) interactionView).setOnKeyListener(new OnKeyListener() {

					@Override
					public boolean onKey(View view, int keyCode, KeyEvent event) {
						return editorAction((TextView) view, keyCode, event, input);
					}
				});

				((EditText) interactionView).setSelectAllOnFocus(true);

				/*
				 * set the start value if it is specified
				 */
				if(input.isStartValueSet()) {
					((EditText) interactionView).setText(input.getStartValue());
				}

				inputLayout.addView(interactionView);

				break;

			case Input.TYPE_SLIDER:
				/*
				 * create a slider
				 */
				final TextView textValue = new TextView(thisActivity);
				textValue.setLayoutParams(params);
				final String valLabel = "Value: ";

				/*
				 * add a listener for when the slider is moved
				 */
				interactionView = new SeekBar(thisActivity);
				((SeekBar) interactionView).setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
					}

					@Override
					public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
						/*
						 * dont do anything if the progressbar was not changed
						 * by the user
						 */
						if(!fromUser)
							return;

						/*
						 * inform the activity that the value was changed
						 */
						double value = (double) progress;
						/*
						 * if min and max values are set, calculate the correct
						 * value from the ratio between the range and the
						 * progress
						 */
						double range = 0;
						if(input.areMinAndMaxValueSet()) {
							range = input.getMaxValue() - input.getMinValue();
						} else if(input.isMaxValueSet()) {
							range = input.getMaxValue();
						}

						if(range > 0) {
							value = (double) (range * ((double) progress / 100.0));
						}

						value = round(value, 2);
						textValue.setText(valLabel + decimalFormatLong.format(value));

						inputValueChanged(input, "" + value);
					}
				});

				/*
				 * if specified, set the start value
				 */
				startValue = 0;
				try {
					startValue = Double.parseDouble(input.getStartValue());
				} catch(Exception e) {
				}
				textValue.setText(valLabel + decimalFormatLong.format(startValue));
				setSeekBarProgress((SeekBar) interactionView, (StandardInput) input);

				/*
				 * if specified, write the min and the max value. else write 0
				 * and 100
				 */
				String rangeString = " (0 - 100)";
				if(input.areMinAndMaxValueSet()) {
					rangeString = " (" + decimalFormatShort.format(input.getMinValue()) + " - "
							+ decimalFormatShort.format(input.getMaxValue()) + ")";

				} else if(input.isMaxValueSet()) {
					rangeString = " (0 - " + decimalFormatShort.format(input.getMaxValue()) + ")";

				}

				textName.setText(name + rangeString);
				inputLayout.addView(textName);

				inputLayout.addView(textValue);

				inputLayout.addView(interactionView);

				break;

			case Input.TYPE_CHECKBOX:
				/*
				 * create a checkbox
				 */
				textName.setText(name);
				inputLayout.addView(textName);

				interactionView = new CheckBox(thisActivity);

				/*
				 * check the box if the start value was anything like 'true'
				 */
				boolean check = false;
				try {
					check = input.getStartValue().compareToIgnoreCase("true") == 0;
				} catch(Exception e) {
				}
				((CheckBox) interactionView).setChecked(check);

				/*
				 * add a listener for when the checkbox is (un)checked
				 */
				((CheckBox) interactionView).setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						inputValueChanged(input, (isChecked ? Input.TRUE : Input.FALSE));
					}
				});

				inputLayout.addView(interactionView);

				break;

			}

			layout.addView(inputLayout);

			if(interactionView != null) {
				interactionView.setLayoutParams(params);
				if(interactionView instanceof Button) {
					/*
					 * wrap buttons around content
					 */
					interactionView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));
				} else {
					/*
					 * stretch other views. calculate hwo much space is left to
					 * the right side
					 */
					interactionView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT));

					/*
					 * display the unit if specified
					 */
					if(((StandardInput) input).getUnit() != null) {
						TextView textUnit = new TextView(thisActivity);
						textUnit.setLayoutParams(params);
						textUnit.setText(((StandardInput) input).getUnit());
						inputLayout.addView(textUnit);
					}
				}
			}
			/*
			 * dont focus any specific input on start up
			 */
			layout.requestFocus();

		} else if(input instanceof ImageInput) {
			/*
			 * create a drawing panel, assign the image data. assume there is
			 * only one image input in this group, so save it to handle touch
			 * events
			 */
			panel = new DrawingPanel(thisActivity);
			panel.setServerGeneratedDrawing((ImageInput) input);
			panel.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			layout.addView(panel);

		}
	}

	protected boolean inputButtonClicked(Input input) {
		/*
		 * send the changed clicked button in a json message to the server
		 */
		return sendMethodCallToServer(input.getRpcMethod(), false);
	}

	protected boolean sendJsonMessageToServer(JSONObject json) {
		try {
			NetworkMessage answer = MainActivity.client.sendEncryptedMessage(new JsonNetworkMessage(json));

			if(answer.isAckMessage()) {
				return true;
			} else if(answer.isErrorMessage()) {
				String error = "Could not send json object to the server - reason: " + answer.getPayload();
				MainActivity.error(error);
				MainActivity.sendToast(error);
			}
		} catch(Exception e) {
			MainActivity.exceptionInfo(e);
		}
		return false;
	}

	/**
	 * packs the given method name and the parameter into a
	 * {@link JsonNetworkMessage}, sends it to the server and evaluates the
	 * answer
	 * 
	 * @param method
	 * @param callOnChange
	 *            indicates whether the method was called after a value was
	 *            changed (<code>true</code>) or after the user confirm the
	 *            transmission of values
	 * @return <code>true</code> if the server acknowledged the method, else
	 *         <code>false</code>
	 */
	public boolean sendMethodCallToServer(String method, String parameter, boolean callOnChange) {
		/*
		 * only send the data if call of this method matches the transmission
		 * setting of the input group
		 */
		if(callOnChange && !sendOnChange)
			return false;

		try {
			JSONObject json = new JSONObject();
			json.put(NetworkMessage.JSON_CALL_METHOD, method);
			json.put(NetworkMessage.JSON_CALL_PARAM, parameter);
			return sendJsonMessageToServer(json);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * packs the given method name into a {@link JsonNetworkMessage}, sends it
	 * to the server and evaluates the answer
	 * 
	 * @param method
	 * @param callOnChange
	 *            indicates whether the method was called after a value was
	 *            changed (<code>true</code>) or after the user confirm the
	 *            transmission of values
	 * @return <code>true</code> if the server acknowledged the method, else
	 *         <code>false</code>
	 */
	public boolean sendMethodCallToServer(String method, boolean callOnChange) {
		/*
		 * only send the data if call of this method matches the transmission
		 * setting of the input group
		 */
		if(callOnChange && !sendOnChange)
			return false;

		try {
			JSONObject json = new JSONObject();
			json.put(NetworkMessage.JSON_CALL_METHOD, method);
			return sendJsonMessageToServer(json);
		} catch(JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * sends the current values of all inputs to the server using one json
	 * message
	 */
	protected void sendInputValuesToServer() {
		JSONArray valueArray = new JSONArray();
		if(panel == null) {
			/*
			 * no drawing panel, just use the normal values
			 */
			/*
			 * read all values from all inputs except buttons and put them to
			 * the array
			 */
			for(Input input : inputGroup.inputsToArray()) {
				if(input.getType().compareTo(Input.TYPE_BUTTON) != 0) {
					JSONObject jsonInput = new JSONObject();
					try {
						jsonInput.put(NetworkMessage.JSON_CALL_METHOD, input.getRpcMethod());
						jsonInput.put(NetworkMessage.JSON_CALL_PARAM, input.getCurrentValue());
					} catch(JSONException e) {
						e.printStackTrace();
					}

					valueArray.put(jsonInput);
				}
			}
		} else {
			/*
			 * read the values from the graphic areas of the image input
			 */
			for(int i = 0; i < panel.countImageInputAreas(); i++) {
				JSONObject jsonInput = new JSONObject();
				DrawingAreaInterface area = panel.getAreaForIndex(i);
				try {
					jsonInput.put(NetworkMessage.JSON_CALL_METHOD, area.getRpcMethod());
					jsonInput.put(NetworkMessage.JSON_CALL_PARAM, area.getValue());
				} catch(JSONException e) {
					e.printStackTrace();
				}

				valueArray.put(jsonInput);
			}
		}
		/*
		 * put the array into a json object and send it to the server
		 */
		try {
			JSONObject json = new JSONObject();
			json.put(NetworkMessage.JSON_METHOD_CALLS, valueArray);

			NetworkMessage answer = MainActivity.client.sendEncryptedMessage(new JsonNetworkMessage(json));

			if(answer.isAckMessage()) {
				String info = "Successfully sent method calls for all inputs to the server";
				MainActivity.info(info);
				MainActivity.sendToast(info);
			} else if(answer.isErrorMessage()) {
				String error = "Could not send method calls for input group to the server - reason: "
						+ answer.getPayload();
				MainActivity.error(error);
				MainActivity.sendToast(error);
			}
		} catch(JSONException e) {
			e.printStackTrace();
		} catch(EncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * method that is called when an editor action happens
	 * 
	 * @param v
	 *            the view where it happened
	 * @param event
	 *            the {@link KeyEvent} that was triggered
	 * @param input
	 *            the {@link Input} this view represents
	 * @return <code>true</code> if the {@link KeyEvent} was handled, else
	 *         <code>false</code>
	 */
	protected boolean editorAction(TextView v, int keyCode, KeyEvent event, Input input) {
		/*
		 * get the text from the edit text
		 */
		String text = "" + v.getText();

		/*
		 * check if the user hit 'enter'
		 */
		if((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
			inputValueChanged(input, text);
			return true;
		}

		return false;
	}

	/**
	 * method that handles a focus change on a text field.
	 * 
	 * @param input
	 *            the {@link Input} object from which this view was generated
	 * @param textField
	 *            the text field
	 * @param hasfocus
	 *            <code>true</code>: focus gained, <code>false</code>: focus
	 *            lost
	 */
	protected void onEditTextEnter(Input input, EditText textField, boolean hasfocus) {
		/*
		 * get the text from the edit text
		 */
		String text = "" + textField.getText();

		if(!hasfocus) {
			inputValueChanged(input, text);
		} else {
			textField.setSelection(0, text.length());
		}
	}

	/**
	 * calculates the progress of the given {@link SeekBar} depending on the
	 * value and the range of the given {@link StandardInput}
	 */
	protected void setSeekBarProgress(SeekBar seekbar, StandardInput input) {
		if(!input.isStartValueSet()) {
			seekbar.setProgress(0);
			return;
		}

		/*
		 * try to convert the string with the start value to a double, if not
		 * just use 0 as the start value
		 */
		double startValue = 0.0;
		try {
			startValue = Double.parseDouble(input.getStartValue());
		} catch(NumberFormatException e) {
		}

		/*
		 * if min and max values are set, calculate the correct value from the
		 * ratio between the range and the progress
		 */
		double range = 0.0;
		if(input.areMinAndMaxValueSet()) {
			range = input.getMaxValue() - input.getMinValue();
		} else if(input.isMaxValueSet()) {
			range = input.getMaxValue();
		}

		int progress = 0;
		if(range == 0.0) {
			progress = (int) (100 * startValue);
		} else {
			progress = (int) (100.0 * startValue / range);
		}
		MainActivity.info("Setting slider to " + startValue + " (progress: " + progress + ")");
		seekbar.setProgress(progress);
	}

	/**
	 * gets called whenever the value of any input is changed. depending on the
	 * transmission setting of the {@link InputGroup} the change is sent to the
	 * server
	 * 
	 * @param input
	 *            the method to be called in the rpc server
	 * @param value
	 *            the new value of the input
	 */
	protected void inputValueChanged(Input input, String value) {
		/*
		 * dont send the values of empty text fields. empty strings that are not
		 * null indicate that no text is inside a textedit. value can be null if
		 * no parameter is required, e.g. after a button click
		 */
		if(value != null) {
			if(value.length() < 1)
				return;
		}
		/*
		 * save the new value in the input
		 */
		input.setCurrentValue(value);

		/*
		 * if the transmit setting is onchange, send the new value for this
		 * input to the server
		 */
		if(inputGroup.getTransmit().compareTo(Input.TRANSMIT_ON_CHANGE) == 0) {
			String call = input.getRpcMethod() + "(" + (value != null ? value : "") + ")";
			MainActivity.info("Input changed: " + call);
			/*
			 * send the changed values in a json message to the server
			 */
			try {
				JSONObject json = new JSONObject();
				json.put(NetworkMessage.JSON_CALL_METHOD, input.getRpcMethod());
				if(value != null) {
					json.put(NetworkMessage.JSON_CALL_PARAM, value);
				}

				NetworkMessage answer = MainActivity.client.sendEncryptedMessage(new JsonNetworkMessage(json));

				if(answer.isAckMessage()) {
					MainActivity.info("Successfully sent method call " + call + " to the server");
				} else if(answer.isErrorMessage()) {
					String error = "Could not send method call " + call + " to the server - reason: "
							+ answer.getPayload();
					MainActivity.error(error);
					MainActivity.sendToast(error);
				}
			} catch(Exception e) {
				MainActivity.exceptionInfo(e);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		/*
		 * Inflate the menu; this adds items to the action bar if it is present.
		 */
		getMenuInflater().inflate(R.menu.input_group, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/*
		 * Handle action bar item clicks here. The action bar will automatically
		 * handle clicks on the Home/Up button, so long as you specify a parent
		 * activity in AndroidManifest.xml.
		 */
		int id = item.getItemId();
		if(id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected double round(double value, int places) {
		if(places < 0)
			return Math.floor(value);

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	/**
	 * calculates the position of a {@link MotionEvent} relative to the on
	 * screen position of the {@link DrawingPanel} {@link #panel} if it exists,
	 * or else returns a {@link FloatPoint} with the original coordinates where
	 * the event happened
	 * 
	 * @return
	 */
	public FloatPoint offset(MotionEvent e) {
		if(panel == null) {
			return new FloatPoint(e);
		} else {
			int[] location = { 0, 0 };
			panel.getLocationOnScreen(location);
			return new FloatPoint(e).offset(-location[0], -location[1]);
		}
	}

	@Override
	public boolean onDown(MotionEvent e) {
		if(panel == null)
			return false;

		return panel.onDown(offset(e));
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		if(panel == null)
			return false;

		return panel.onFling(offset(e1), offset(e2), velocityX, velocityY);
	}

	@Override
	public void onLongPress(MotionEvent e) {
		if(panel == null)
			return;

		panel.onLongPress(offset(e));
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		if(panel == null)
			return false;

		return panel.onScroll(offset(e1), offset(e2), distanceX, distanceY);
	}

	@Override
	public void onShowPress(MotionEvent e) {
		if(panel == null)
			return;

		panel.onShowPress(offset(e));
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		if(panel == null)
			return false;

		return panel.onSingleTapUp(offset(e));
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return gestureDetector.onTouchEvent(event);
	}

}
