package app.view.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import rooftop.app.R;
import share.exceptions.EncodingException;
import share.inputs.Input;
import share.inputs.InputFactory;
import share.inputs.InputGroup;
import share.inputs.StandardInput;
import share.message.InfoNetworkMessage;
import share.message.JsonNetworkMessage;
import share.message.NetworkMessage;
import share.user.UserAccount;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import app.MainActivity;
import app.view.ExpandableListAdapter;

/**
 * {@link ActionBarActivity} that allows the admin to create/delete user
 * accounts or change the access rights to existing accounts
 * 
 * @author Philipp Hempel
 *
 */
public class UserManagementActivity extends ActionBarActivity {

	/**
	 * {@link ExpandableListView} widget that contains the data for the
	 * different user accounts. It is dynamically filled with information from
	 * the {@link JSONObject}s with user information that were received from the
	 * server.
	 */
	private ExpandableListView listView;
	/**
	 * list of strings with the names of the headers in {@link #listView}
	 */
	private ArrayList<String> listDataHeader;
	/**
	 * list of string lists with the data of the children in {@link #listView}
	 */
	private HashMap<String, List<String>> listDataChild;
	/**
	 * reference to this {@link UserManagementActivity}, to allow static access
	 * and access from inside listeners
	 */
	private UserManagementActivity thisActivity;
	/**
	 * saves the user account objects, maps username to account
	 */
	private HashMap<String, UserAccount> users;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_account);

		setTitle("User Account Management");

		/*
		 * save the reference to this activity
		 */
		thisActivity = this;

		/*
		 * set up the list view
		 */
		listView = (ExpandableListView) findViewById(R.id.listViewInputGroups);
		listView.removeAllViewsInLayout();

		listDataHeader = new ArrayList<String>();

		listDataChild = new HashMap<String, List<String>>();

		/*
		 * request a list of all user accounts from the server and dislay them
		 * in the list view
		 */
		users = new HashMap<String, UserAccount>();

		try {
			NetworkMessage answer = MainActivity.client.sendEncryptedMessage(new InfoNetworkMessage(
					NetworkMessage.MSG_REQUEST_USER_ACCOUNT_LIST));

			if(answer.isJsonMessage()) {
				JSONObject json = ((JsonNetworkMessage) answer).getJsonObject();

				if(json.has(NetworkMessage.JSON_ACCOUNT_LIST)) {
					JSONArray array = json.getJSONArray(NetworkMessage.JSON_ACCOUNT_LIST);
					MainActivity.info("Received Data of " + array.length() + " User Accounts");

					for(int i = 0; i < array.length(); i++) {
						UserAccount user = UserAccount.fromJsonObject(array.getJSONObject(i));
						MainActivity.info(user);
						if(user != null) {

							users.put(user.getName(), user);

							String role = UserAccount.getTextForRoleNumber(user.getRole());
							/*
							 * create the list header if it does not exist yet
							 */
							if(listDataChild.get(role) == null) {
								listDataHeader.add(role);
								listDataChild.put(role, new ArrayList<String>());
							}
							/*
							 * add the user data
							 */
							listDataChild.get(role).add(user.getName());
						}
					}

					/*
					 * clear the list view
					 */
					clearListView();
					/*
					 * fill the list view
					 */
					listView.setAdapter(new ExpandableListAdapter(getBaseContext(), listDataHeader, listDataChild));
					/*
					 * set listener(s) for the list view
					 */
					listView.setOnChildClickListener(new OnChildClickListener() {

						@Override
						public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
								int childPosition, long id) {
							/*
							 * add the data of the user to the intent and start
							 * an user account activity
							 */
							Intent intent = new Intent(thisActivity, UserAccountActivity.class);

							/*
							 * get the user role from the list header
							 */
							intent.putExtra(NetworkMessage.JSON_USER_ROLE,
									thisActivity.listDataHeader.get(groupPosition));
							String username;
							/*
							 * get the username from the list child
							 */
							intent.putExtra(
									NetworkMessage.JSON_USER_NAME,
									username = thisActivity.listDataChild.get(
											thisActivity.listDataHeader.get(groupPosition)).get(childPosition));
							intent.putExtra(NetworkMessage.JSON_ID, users.get(username).getId());

							thisActivity.startActivity(intent);

							return false;
						}
					});
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	protected void clearListView() {
		try {
			listView.setAdapter(new ExpandableListAdapter());
		} catch(Exception e) {
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		/*
		 * Inflate the menu; this adds items to the action bar if it is present.
		 */
		getMenuInflater().inflate(R.menu.user_account, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/*
		 * Handle action bar item clicks here. The action bar will automatically
		 * handle clicks on the Home/Up button, so long as you specify a parent
		 * activity in AndroidManifest.xml.
		 */
		int id = item.getItemId();
		if(id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
