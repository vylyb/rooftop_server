package app.view.activity;

import rooftop.app.R;
import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * class that contains the preferences of the app that are set using the
 * {@link SettingsActivity}
 * 
 * @author Philipp Hempel
 *
 */
public class Preferences extends PreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	}

}
