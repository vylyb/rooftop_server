package app.view.activity;

import rooftop.app.R;
import share.user.UserAccount;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import app.MainActivity;

/**
 * {@link Activity} that is shown when the user clicks on the setting button in
 * the {@link MainActivity}. the settings are saved in the class
 * {@link Preferences}
 * 
 * @author Philipp Hempel
 *
 */
public class SettingsActivity extends PreferenceActivity {

	/**
	 * a button that is displayed inside the preference activity, to allow the
	 * admin to access the {@link UserAccount} management
	 */
	private Button userAccountsButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

		/*
		 * add a button to directly access the user account activity
		 */
		userAccountsButton = new Button(this);
		userAccountsButton.setText("Manage User Accounts");
		userAccountsButton.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.MATCH_PARENT));
		userAccountsButton.setEnabled(MainActivity.isLoggedInAsAdmin());
		userAccountsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent intent = new Intent(MainActivity.thisActivity, UserManagementActivity.class);
				MainActivity.thisActivity.startActivity(intent);
			}
		});

		/*
		 * display a label if the user is not logged in as an admin
		 */
		TextView label = new TextView(this);
		label.setText("Only the Admin Account can access the User Account Management");
		label.setVisibility(MainActivity.isLoggedInAsAdmin() ? View.INVISIBLE : View.VISIBLE);
		label.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));

		LinearLayout footerLayout = new LinearLayout(this);
		footerLayout.addView(userAccountsButton);
		footerLayout.addView(label);

		getListView().addFooterView(footerLayout);
	}
}
