package app.view.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.jws.Oneway;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.FrameLayout.LayoutParams;
import app.MainActivity;
import rooftop.app.R;
import share.inputs.Input;
import share.inputs.InputGroup;
import share.inputs.StandardInput;
import share.message.JsonNetworkMessage;
import share.message.NetworkMessage;
import share.user.UserAccount;

/**
 * class that inherits an {@link InputGroupActivity} and uses the automatically
 * created inputs from an {@link InputGroup} object to display controls to
 * change user accounts
 * 
 * @author Philipp Hempel
 *
 */
public class UserAccountActivity extends ActionBarActivity {

	/**
	 * the id of the user that is managed by this activity
	 */
	private int userid;
	/**
	 * the name of the user that is managed by this activity
	 */
	private String username;
	/**
	 * the role of the user that is managed by this activity
	 */
	private int userrole;
	/**
	 * allow static access and access from inside listeners to this activity
	 */
	private UserAccountActivity thisActivity;
	/**
	 * label that is shown when the user role is a regualar user
	 */
	private TextView label2;
	/**
	 * shown when the user role is a guest user. contains checkboxes for all
	 * input groups so the access to each input group can be specified
	 */
	private LinearLayout selectAccessLayout;
	/**
	 * hash map that saves the access rights of a guest user to the input
	 * groups. maps the name of the input group to <code>true</code> if the
	 * input group can be accessed, else it maps to <code>false</code>
	 */
	private HashMap<String, Boolean> accessRights;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_input_group);

		/*
		 * read the parameters of the intent to get the data for the user
		 */
		userid = getIntent().getIntExtra(NetworkMessage.JSON_ID, -1);
		/*
		 * make sure the user id has been transfered correctly
		 */
		if(userid == -1) {
			setTitle("Error, could not get the id of the user " + username + "!");
			return;
		}
		username = getIntent().getStringExtra(NetworkMessage.JSON_USER_NAME);
		userrole = UserAccount.getRoleNumberForText(getIntent().getStringExtra(NetworkMessage.JSON_USER_ROLE));

		setTitle("Change User " + username);

		/*
		 * fill the hash map for the access rights with all input groups
		 */
		accessRights = new HashMap<String, Boolean>();
		for(InputGroup group : MainActivity.getInputGroups()) {
			accessRights.put(group.getName(), Boolean.FALSE);
		}

		/*
		 * request the access rights of this user
		 */
		try {
			JSONObject json = new JSONObject();
			json.put(NetworkMessage.MSG_REQUEST_USER_ACCESS_RIGHTS, username);
			NetworkMessage answer = MainActivity.client.sendEncryptedMessage(new JsonNetworkMessage(json));
			if(answer.isJsonMessage()) {
				json = ((JsonNetworkMessage) answer).getJsonObject();
				if(json.has(NetworkMessage.JSON_USER_ACCESS_RIGHTS)) {
					/*
					 * get all input groups to which the user has access and set
					 * their value to true in the map
					 */
					JSONArray rights = json.getJSONArray(NetworkMessage.JSON_USER_ACCESS_RIGHTS);
					for(int i = 0; i < rights.length(); i++) {
						accessRights.put(rights.getString(i), Boolean.TRUE);
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		/*
		 * create the vertical linear layout
		 */
		LinearLayout layout = (LinearLayout) findViewById(R.id.inputGroupLayout);
		layout.setOrientation(LinearLayout.VERTICAL);

		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		params.setMargins(10, 0, 10, 0);

		thisActivity = this;

		LinearLayout inputLayout = new LinearLayout(thisActivity);

		/*
		 * create a header with buttons
		 */
		LinearLayout headerLayout = new LinearLayout(thisActivity);

		Button saveButton = new Button(thisActivity);
		saveButton.setText("Save the settings");
		saveButton.setLayoutParams(params);
		saveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*
				 * collect all information about the user account and send it to
				 * the server
				 */
				try {
					JSONObject json = new JSONObject();
					json.put(NetworkMessage.JSON_ID, userid);
					json.put(NetworkMessage.JSON_USER_NAME, username);
					json.put(NetworkMessage.JSON_USER_ROLE, userrole);
					/*
					 * put all groups where the user has access to into the
					 * array
					 */
					JSONArray rights = new JSONArray();
					for(String groupName : accessRights.keySet()) {
						if(accessRights.get(groupName).booleanValue()) {
							rights.put(groupName);
						}
					}
					json.put(NetworkMessage.JSON_USER_ACCESS_RIGHTS, rights);
					MainActivity.info("Update User Data: " + json.toString(2));

					NetworkMessage answer = MainActivity.client.sendEncryptedMessage(new JsonNetworkMessage(json));
					if(answer.isAckMessage()) {
						MainActivity.sendToast("Successfully updated the User " + username + " in the server");
					} else {
						MainActivity.sendToast("Could not update the User " + username + " in the server: "
								+ answer.getPayload());
					}
				} catch(Exception e) {
				}
			}
		});
		headerLayout.addView(saveButton);

		Button deleteButton = new Button(thisActivity);
		deleteButton.setText("Delete this user");
		deleteButton.setLayoutParams(params);
		deleteButton.setBackgroundColor(Color.RED);
		deleteButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*
				 * display an alert dialog to let the user confirm
				 */
				AlertDialog ad = new AlertDialog.Builder(thisActivity).create();
				ad.setMessage("Do you really want to delete the user '" + username + "'?");
				ad.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						try {
							JSONObject json = new JSONObject();
							json.put(NetworkMessage.MSG_DELETE_USER, username);
							MainActivity.info("Deleting User " + username);

							NetworkMessage answer = MainActivity.client.sendEncryptedMessage(new JsonNetworkMessage(
									json));
							if(answer.isAckMessage()) {
								MainActivity.sendToast("Successfully deleted the User " + username + " in the server");
							} else {
								MainActivity.sendToast("Could not delete the User " + username + " in the server: "
										+ answer.getPayload());
							}
						} catch(Exception e) {
							e.printStackTrace();
						}
						dialog.dismiss();
					}
				});
				ad.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				ad.show();
			}
		});
		headerLayout.addView(deleteButton);

		layout.addView(headerLayout);

		/*
		 * add the available user roles to the drop down menu
		 */
		TextView label;
		inputLayout.addView(label = new TextView(thisActivity));
		label.setText("User belongs to the Role Group: ");
		label.setLayoutParams(params);

		Spinner roleSpinner = new Spinner(thisActivity);
		final List<String> list = new ArrayList<String>();
		list.add(UserAccount.getTextForRoleNumber(UserAccount.ROLE_REGULAR));
		list.add(UserAccount.getTextForRoleNumber(UserAccount.ROLE_GUEST));
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		roleSpinner.setAdapter(dataAdapter);
		roleSpinner.setSelection((regularUser() ? 0 : 1));
		roleSpinner.setLayoutParams(params);
		/*
		 * add a listener for the spinner
		 */
		roleSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				userrole = UserAccount.getRoleNumberForText(list.get(pos));
				roleChanged();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		inputLayout.addView(roleSpinner);

		/*
		 * add a textview that is displayed when the user gets the role of a
		 * regular user
		 */
		inputLayout.addView(label2 = new TextView(thisActivity));
		label2.setLayoutParams(params);

		layout.addView(inputLayout);

		/*
		 * add a vertical layout with checkboxes for every input group that is
		 * displayed when the user role is a guest user
		 */
		layout.addView(selectAccessLayout = new LinearLayout(thisActivity));
		selectAccessLayout.setOrientation(LinearLayout.VERTICAL);
		selectAccessLayout.setLayoutParams(params);
		for(final String groupName : accessRights.keySet()) {

			LinearLayout groupLayout = new LinearLayout(thisActivity);

			/*
			 * create a new checkbox for the input group and set it checked if
			 * the user has access to it
			 */
			CheckBox checkbox = new CheckBox(thisActivity);
			checkbox.setLayoutParams(params);
			checkbox.setChecked(accessRights.get(groupName).booleanValue());
			MainActivity.info(groupName + " " + checkbox.isChecked());
			checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					accessRights.put(groupName, Boolean.valueOf(isChecked));
				}
			});
			groupLayout.addView(checkbox);

			TextView groupLabel = new TextView(thisActivity);
			groupLabel.setText(groupName);
			groupLabel.setLayoutParams(params);
			groupLayout.addView(groupLabel);

			selectAccessLayout.addView(groupLayout);
		}

		roleChanged();

	}

	/**
	 * method that is called when the role of the user was changed and that
	 * displays/hides the according views
	 */
	private void roleChanged() {
		MainActivity.info(username + " " + userrole);
		boolean reg = regularUser();
		label2.setText(reg ? "User has access to all controls" : "Select the access to the controls below");
		selectAccessLayout.setVisibility(reg ? View.INVISIBLE : View.VISIBLE);
	}

	/**
	 * @return <code>true</code> if the {@link String} that describes the user
	 *         role is equal to the list entry for regular users
	 */
	private boolean regularUser() {
		return userrole == UserAccount.ROLE_REGULAR;
	}
}
