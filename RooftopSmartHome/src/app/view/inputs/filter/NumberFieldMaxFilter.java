package app.view.inputs.filter;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * class that filters the content of a textfield and makes sure that the text
 * represents a double value that is not bigger than the maximum value
 * 
 * @author Philipp Hempel
 *
 */
public class NumberFieldMaxFilter implements InputFilter {

	/**
	 * the maximum value
	 */
	private double maxValue;

	public NumberFieldMaxFilter(double maxValue) {
		this.maxValue = maxValue;
	}

	@Override
	public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
		try {
			double input = Double.parseDouble(dest.toString() + source.toString());
			if(input <= maxValue)
				return null;
		} catch(NumberFormatException e) {
		}
		return "";
	}

}
