package app.view.inputs.filter;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * class that filters the content of a textfield and makes sure that the text
 * represents a double value that is not smaller than the minimum value
 * 
 * @author Philipp Hempel
 *
 */
public class NumberFieldMinFilter implements InputFilter {

	/**
	 * the minimum value
	 */
	private double minValue;

	public NumberFieldMinFilter(double minValue) {
		this.minValue = minValue;
	}

	@Override
	public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
		try {
			double input = Double.parseDouble(dest.toString() + source.toString());
			if(input >= minValue)
				return null;
		} catch(NumberFormatException e) {
		}
		return "";
	}

}
