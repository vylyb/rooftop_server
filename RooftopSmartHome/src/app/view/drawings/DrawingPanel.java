package app.view.drawings;

import java.util.ArrayList;

import share.encoding.Base64Util;
import share.inputs.ImageInput;
import share.inputs.ImageInputArea;
import share.inputs.Input;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import app.MainActivity;
import app.view.activity.InputGroupActivity;
import app.view.drawings.areas.ClickableArea;
import app.view.drawings.areas.DrawingAreaInterface;
import app.view.drawings.areas.SlidableArea;
import app.view.drawings.util.FloatPoint;

import com.caverock.androidsvg.SVG;

/**
 * class that uses the library androidsvg.jar (<a
 * href="https://bitbucket.org/paullebeau/androidsvg/downloads"
 * >https://bitbucket.org/paullebeau/androidsvg/downloads</a>) to draw the
 * graphic controls that were sent from the server.
 * 
 * @author Philipp Hempel
 *
 */
public class DrawingPanel extends View {

	private Paint paint;
	private SVG image;
	private ArrayList<DrawingAreaInterface> areas;
	private FloatPoint touch, scroll;
	private int canvasHeight, canvasWidth;
	private double imageHeight, imageWidth, imageRatio;
	private Rect imageRect, targetRectangle;
	private InputGroupActivity activity;

	public DrawingPanel(Context context) {
		super(context);
		init(context);
	}

	public DrawingPanel(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public DrawingPanel(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}

	/**
	 * @return the number of {@link ImageInputArea}s in {@link #imageInput} that
	 *         defines the drawing
	 */
	public int countImageInputAreas() {
		return areas.size();
	}

	/**
	 * @return the {@link ImageInputArea} in {@link #imageInput} at the given
	 *         index
	 */
	public DrawingAreaInterface getAreaForIndex(int index) {
		return areas.get(index);

	}

	/**
	 * helper method for the constructors
	 * 
	 * @param context
	 */
	private void init(Context context) {

		activity = (InputGroupActivity) context;

		paint = new Paint();
		paint.setAntiAlias(true);

		imageRect = new Rect(0, 0, 0, 0);

		setLayerType(View.LAYER_TYPE_SOFTWARE, paint);

	}

	public void setServerGeneratedDrawing(ImageInput input) {

		image = generateSvgFromInput(input);
		areas = new ArrayList<DrawingAreaInterface>();
		/*
		 * create clickableAreas for the areas of the input
		 */
		for(int i = 0; i < input.countAreas(); i++) {
			ImageInputArea area = input.getArea(i);
			DrawingAreaInterface clickable = null;

			if(area.getType().compareTo(Input.AREA_TYPE_CLICK) == 0) {

				clickable = new ClickableArea(generateSvgFromInput(area),
						generateSvgFromFilename(area.getTogglefile()), area);
				/*
				 * add the strings of the method and the group to open, they can
				 * be null if they are not specified
				 */
				clickable.setOpenGroup(area.getOpenGroup());

			} else if(area.getType().compareTo(Input.AREA_TYPE_SLIDER) == 0) {

				clickable = new SlidableArea(generateSvgFromInput(area), generateSvgFromFilename(area.getTogglefile()),
						area.getDirection(), area);
				try {
					((SlidableArea) clickable).setValue(Double.parseDouble(area.getStartValue()));
				} catch(Exception e) {
					((SlidableArea) clickable).setValue(50.0);
				}
			}

			clickable.setRpcMethod(area.getRpcMethod());

			/*
			 * set the parent activity for the area
			 */
			clickable.setParentActivity(activity);

			areas.add(clickable);
		}
	}

	/**
	 * helper method for {@link #generateSvgFromFilename(String)}
	 */
	private SVG generateSvgFromInput(ImageInputArea area) {
		return generateSvgFromFilename(area.getFile());

	}

	/**
	 * helper method for {@link #generateSvgFromFilename(String)}
	 */
	public SVG generateSvgFromInput(ImageInput input) {
		return generateSvgFromFilename(input.getFile());
	}

	/**
	 * requests the content of the image specified by the given filename from
	 * the server if it is not loaded yet, decodes the base64 string and creates
	 * a {@link SVG} image
	 * 
	 * @return {@link SVG} object or <code>null</code> if anything went wrong
	 */
	public SVG generateSvgFromFilename(String filename) {
		if(filename == null)
			return null;

		try {
			String content = MainActivity.savedImages.get(filename);
			/*
			 * if the data is not saved yet request it from the server and save
			 * it
			 */
			if(content == null) {
				/*
				 * load the image from the server
				 */
				content = Base64Util.stringFromBase64(MainActivity.requestImageData(filename));

				MainActivity.savedImages.put(filename, content);
			}
			return SVG.getFromString(content);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onDraw(Canvas canvas) {

		/*
		 * draw the main image over the areas, so that non-transparent parts of
		 * the image can cover the areas
		 */
		if(image != null) {

			canvasWidth = getWidth();
			canvasHeight = getHeight();

			imageHeight = image.getDocumentHeight();
			imageWidth = image.getDocumentWidth();

			/*
			 * stretch the greater side of the image so that it fills the whole
			 * canvas
			 */
			imageRatio = imageWidth / imageHeight;
			if(imageRatio <= (double) ((double) canvasWidth / (double) canvasHeight)) {

				imageHeight = canvasHeight;
				imageWidth = imageHeight * imageRatio;

				imageRect.left = (int) ((canvasWidth - imageWidth) / 2);
				imageRect.right = (int) (imageRect.left + imageWidth);
				imageRect.top = 0;
				imageRect.bottom = (int) imageHeight;

			} else {

				imageWidth = canvasWidth;
				imageHeight = imageWidth / imageRatio;

				imageRect.left = 0;
				imageRect.right = (int) imageWidth;
				imageRect.top = (int) ((canvasHeight - imageHeight) / 2);
				imageRect.bottom = (int) (imageRect.top + imageHeight);

			}

			canvas.drawPicture(image.renderToPicture(), imageRect);
		}

		/*
		 * add the images or rectangles for the areas
		 */
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(3);

		for(DrawingAreaInterface area : areas) {
			targetRectangle = area.getRectangle(imageRect);

			/*
			 * if this is a slidable area, stretch the rectangle to the value
			 * the slider currently has
			 */
			if(area instanceof SlidableArea) {

				imageWidth = targetRectangle.right - targetRectangle.left;
				imageHeight = targetRectangle.bottom - targetRectangle.top;

				double value = Double.parseDouble(((SlidableArea) area).getValue());

				if(((SlidableArea) area).getDirection() != null) {

					/*
					 * stretch the slider to the given direction
					 */
					switch (((SlidableArea) area).getDirection()){

					case Input.UP:
						targetRectangle.top = (int) (targetRectangle.bottom - (value * imageHeight));
						break;

					case Input.DOWN:
						targetRectangle.bottom = (int) (targetRectangle.top + (value * imageHeight));
						break;

					case Input.LEFT:
						targetRectangle.left = (int) (targetRectangle.right - (value * imageWidth));
						break;

					case Input.RIGHT:
						targetRectangle.right = (int) (targetRectangle.left + (value * imageWidth));
						break;

					default:
						break;
					}
				} else {
					/*
					 * stretch the slider to all directions
					 */
					double diffH = (imageHeight - (imageHeight * value)) / 2.0;
					double diffW = (imageWidth - (imageWidth * value)) / 2.0;

					targetRectangle.left += diffW;
					targetRectangle.top += diffH;
					targetRectangle.right -= diffW;
					targetRectangle.bottom -= diffH;
				}
			}
			/*
			 * draw the image or the frame of the area
			 */
			if(area.isToggled()) {
				if(area.getToggleImage() != null) {
					canvas.drawPicture(area.getToggleImage().renderToPicture(), targetRectangle);
				} else {
					if(area.getImage() != null) {
						canvas.drawPicture(area.getImage().renderToPicture(), targetRectangle);
					}
					paint.setColor(Color.GRAY);
					canvas.drawRect(targetRectangle, paint);
				}
			} else {
				if(area.getImage() != null) {
					canvas.drawPicture(area.getImage().renderToPicture(), targetRectangle);
				} else {
					paint.setColor(Color.BLACK);
					canvas.drawRect(targetRectangle, paint);
				}
			}

			// paint.setTextSize(20);
			// canvas.drawText((area.getRpcMethod() != null ?
			// area.getRpcMethod()
			// + "|" : "")
			// + area.getOpenGroup(), rect.left,
			// rect.top + paint.getTextSize(), paint);
		}

		/*
		 * draw the x and y coordinates of the touch and scroll point
		 */
		paint.setStrokeWidth(1);
		if(touch != null) {
			paint.setColor(Color.RED);
			canvas.drawLine(0, touch.getY(), touch.getX(), touch.getY(), paint);
			canvas.drawLine(touch.getX(), 0, touch.getX(), touch.getY(), paint);
		}
		if(scroll != null) {
			paint.setColor(Color.GRAY);
			canvas.drawLine(0, scroll.getY(), scroll.getX(), scroll.getY(), paint);
			canvas.drawLine(scroll.getX(), 0, scroll.getX(), scroll.getY(), paint);
		}
		if(touch != null && scroll != null) {
			paint.setColor(Color.BLACK);
			canvas.drawLine(touch.getX(), touch.getY(), scroll.getX(), scroll.getY(), paint);
		}
		/*
		 * draw an overlay that shows the boundaries of this panel
		 */
		// paint.setColor(Color.BLUE);
		// canvas.drawLine(0, 0, canvasWidth, 0, paint);
		// canvas.drawLine(0, 0, 0, canvasHeight, paint);
		// canvas.drawLine(canvasWidth, 0, canvasWidth, canvasHeight, paint);
		// canvas.drawLine(0, canvasHeight, canvasWidth, canvasHeight, paint);
		// canvas.drawLine(0, 0, canvasWidth, canvasHeight, paint);
		// canvas.drawLine(0, canvasHeight, canvasWidth, 0, paint);
	}

	/**
	 * gets called by the onDown() method of the parent
	 * {@link InputGroupActivity} when a down gesture happened
	 * 
	 * @param where
	 *            {@link FloatPoint} where the gesture happened
	 */
	public boolean onDown(FloatPoint where) {

		/*
		 * check if the point is inside a clickable area, if then call the
		 * according method
		 */
		touch = where;
		scroll = null;

		for(DrawingAreaInterface area : areas) {

			if(area.containsPoint(touch, imageRect)) {

				area.onTouch(touch, area.getRectangle(imageRect));
			} else {
				/*
				 * if the area does not use a toggle image, set the toggle value
				 * to false so that an area that is not touched is not framed
				 */
				if(!area.usesToggleImage()) {
					area.setToggled(false);
				}
			}
		}

		invalidate();

		return true;
	}

	/**
	 * gets called by the onFling() method of the parent
	 * {@link InputGroupActivity} when a fling gesture happened
	 * 
	 * @param from
	 *            {@link FloatPoint} where the gesture started
	 * @param to
	 *            {@link FloatPoint} where the gesture ended
	 * @param velocityX
	 *            velocity of the gesture in x direction
	 * @param velocityY
	 *            velocity of the gesture in y direction
	 * @return <code>true</code> if the event has been consumed, else
	 *         <code>false</code>
	 */
	public boolean onFling(FloatPoint from, FloatPoint to, float velocityX, float velocityY) {
		touch = from;
		scroll = to;

		for(DrawingAreaInterface area : areas) {

			if(area.containsPoint(scroll, imageRect)) {
				area.onFling(touch, scroll, velocityX, velocityY, area.getRectangle(imageRect));
			}
		}

		return true;
	}

	/**
	 * gets called by the onLongPress() method of the parent
	 * {@link InputGroupActivity} when a long press gesture happened
	 * 
	 * @param where
	 *            {@link FloatPoint} where the gesture happened
	 */
	public void onLongPress(FloatPoint where) {

		touch = where;

		for(DrawingAreaInterface area : areas) {

			if(area.containsPoint(touch, imageRect)) {
				area.onLongPress(touch, area.getRectangle(imageRect));
			}
		}

		invalidate();
	}

	/**
	 * gets called by the onScroll() method of the parent
	 * {@link InputGroupActivity} when a scroll gesture happened
	 * 
	 * @param from
	 *            {@link FloatPoint} where the gesture started
	 * @param to
	 *            {@link FloatPoint} where the gesture ended
	 * @param distanceX
	 *            distance of the gesture in x direction
	 * @param distanceY
	 *            distance of the gesture in y direction
	 * @return <code>true</code> if the event has been consumed, else
	 *         <code>false</code>
	 */
	public boolean onScroll(FloatPoint from, FloatPoint to, float distanceX, float distanceY) {
		touch = from;
		scroll = to;

		for(DrawingAreaInterface area : areas) {

			if(area.containsPoint(scroll, imageRect)) {
				area.onSlide(touch, scroll, distanceX, distanceY, area.getRectangle(imageRect));
			}
		}

		invalidate();

		return true;
	}

	/**
	 * gets called by the onShowPress() method of the parent
	 * {@link InputGroupActivity} when a press gesture happened
	 * 
	 * @param where
	 *            {@link FloatPoint} where the gesture happened
	 */
	public void onShowPress(FloatPoint where) {
	}

	/**
	 * gets called by the onSingleTapUp() method of the parent
	 * {@link InputGroupActivity} when a tap up gesture happened
	 * 
	 * @param where
	 *            {@link FloatPoint} where the gesture happened
	 * @return <code>true</code> if the event has been consumed, else
	 *         <code>false</code>
	 */
	public boolean onSingleTapUp(FloatPoint where) {

		touch = where;

		for(DrawingAreaInterface area : areas) {

			if(area.containsPoint(touch, imageRect)) {
				area.onSingleTapUp(touch, area.getRectangle(imageRect));
			}
		}

		invalidate();

		return true;
	}

}
