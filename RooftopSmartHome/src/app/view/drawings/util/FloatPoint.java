package app.view.drawings.util;

import android.graphics.Rect;
import android.view.MotionEvent;

/**
 * class that represents a point on the screen and that contains some more
 * helper methods
 * 
 * @author Philipp Hempel
 *
 */
public class FloatPoint {

	private float x;
	private float y;

	public FloatPoint(float x, float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * helper constructor that gets the x and y coordinate from the given
	 * {@link MotionEvent}
	 * 
	 * @param e
	 */
	public FloatPoint(MotionEvent e) {
		this.x = e.getX();
		this.y = e.getY();
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public void setX(float x) {
		this.x = x;
	}

	public void setY(float y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "FloatPoint [x=" + x + ", y=" + y + "]";
	}

	/**
	 * calculates the relative position of the point inside the given target
	 * rectangle
	 */
	public FloatPoint mapToRectangle(Rect target) {
		float width = target.right - target.left;
		float height = target.bottom - target.top;

		return new FloatPoint((float) (x - target.left) / width, (float) (y - target.top) / height);
	}

	public FloatPoint offset(int left, int top) {
		// MainActivity.info(this + " Offset: " + left + " | " + top);
		return new FloatPoint(x + left, y + top);
	}

}
