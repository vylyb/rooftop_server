package app.view.drawings.areas;

import android.graphics.Rect;
import app.view.activity.InputGroupActivity;
import app.view.drawings.util.FloatPoint;

import com.caverock.androidsvg.SVG;

/**
 * interface for a drawable area. implemented by {@link ClickableArea} and
 * {@link SlidableArea}
 * 
 * @author Philipp Hempel
 *
 */
public interface DrawingAreaInterface {

	/**
	 * @param point
	 *            the point where a touch event happened
	 * @param target
	 *            the area of the underlying drawing to which the point will be
	 *            mapped
	 * @return <code>true</code> if the point is inside the rectangle that
	 *         defines the area
	 */
	boolean containsPoint(FloatPoint point, Rect target);

	/**
	 * method that is called if the area is touched
	 * 
	 * @param imageRect
	 */
	public void onTouch(FloatPoint point, Rect targetRectangle);

	/**
	 * method that is called if the area is pressed (long touch)
	 */
	public void onPress(FloatPoint point);

	/**
	 * method that is called when a sliding movement happened inside this area
	 */
	public void onSlide(FloatPoint from, FloatPoint to, float distanceX, float distanceY, Rect rect);

	public Rect getRectangle(Rect target);

	public String getRpcMethod();

	public SVG getImage();

	public SVG getToggleImage();

	public void setToggled(boolean b);

	public boolean isToggled();

	public void setOpenGroup(String openGroup);

	public String getOpenGroup();

	public void setRpcMethod(String rpcMethod);

	public void setParentActivity(InputGroupActivity activity);

	public boolean toggle();

	boolean usesToggleImage();

	public void onLongPress(FloatPoint touch, Rect rectangle);

	public void onFling(FloatPoint touch, FloatPoint scroll, float velocityX, float velocityY, Rect rectangle);

	public void onSingleTapUp(FloatPoint touch, Rect rectangle);

	public String getValue();

}
