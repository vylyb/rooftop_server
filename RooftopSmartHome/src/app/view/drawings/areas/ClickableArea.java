package app.view.drawings.areas;

import share.exceptions.EncodingException;
import share.inputs.ImageInputArea;
import share.inputs.Input;
import android.graphics.Rect;
import app.MainActivity;
import app.view.activity.InputGroupActivity;
import app.view.drawings.util.FloatPoint;

import com.caverock.androidsvg.SVG;

/**
 * class that implements a {@link DrawingAreaInterface} and represents a
 * rectangle with optional images that can be clicked
 * 
 * @author Philipp Hempel
 *
 */
public class ClickableArea implements DrawingAreaInterface {

	protected double left;
	protected double right;
	protected double bottom;
	protected double top;
	protected SVG image, toggleimage;
	protected String method, openGroup, direction;
	protected boolean toggled;
	protected InputGroupActivity parentActivity;

	/**
	 * constructor that specifies the rectangular area
	 */
	public ClickableArea(ImageInputArea area) {
		init(null, null, area);
	}

	/**
	 * constructor that specifies the rectangular area and an image that is
	 * shown instead of a frame
	 */
	public ClickableArea(SVG image, ImageInputArea area) {
		init(image, null, area);
	}

	/**
	 * constructor that specifies the rectangular area, an image that is shown
	 * instead of a frame and a second image that is shown when this area was
	 * toggled
	 */
	public ClickableArea(SVG image, SVG toggleimage, ImageInputArea area) {
		init(image, toggleimage, area);
	}

	/**
	 * method that creates the rectangular area, an image that is shown instead
	 * of a frame and a second image that is shown when this area was toggled
	 */
	private void init(SVG image, SVG toggleimage, ImageInputArea area) {
		this.left = area.getLeft();
		this.top = area.getTop();
		this.right = area.getLeft() + area.getWidth();
		this.bottom = area.getTop() + area.getHeight();
		this.image = image;
		this.toggleimage = toggleimage;
	}

	public boolean hasImage() {
		return image != null;
	}

	public SVG getImage() {
		return image;
	}

	public double getLeft() {
		return left;
	}

	public void setLeft(double left) {
		this.left = left;
	}

	public double getRight() {
		return right;
	}

	public void setRight(double right) {
		this.right = right;
	}

	public double getTop() {
		return top;
	}

	public void setTop(double top) {
		this.top = top;
	}

	public double getBottom() {
		return bottom;
	}

	public void setBottom(double bottom) {
		this.bottom = bottom;
	}

	public double getWidth() {
		return right - left;
	}

	public double getHeight() {
		return bottom - top;
	}

	public String getOpenGroup() {
		return openGroup;
	}

	public void setRpcMethod(String method) {
		this.method = method;
	}

	public void setOpenGroup(String openGroup) {
		this.openGroup = openGroup;
	}

	@Override
	public boolean containsPoint(FloatPoint point, Rect target) {
		FloatPoint p = point.mapToRectangle(target);

		if(p.getX() < left)
			return false;

		if(p.getY() < top)
			return false;

		if(p.getX() > right)
			return false;

		if(p.getY() > bottom)
			return false;

		return true;
	}

	@Override
	public void onTouch(FloatPoint point, Rect targetRectangle) {
		/*
		 * do nothing here
		 */
	}

	@Override
	public void onPress(FloatPoint point) {
		/*
		 * do nothing here
		 */
	}

	/**
	 * creates a {@link Rect} with real coordinates from the relative positions
	 * of this rectangle inside the target rectangle
	 */
	@Override
	public Rect getRectangle(Rect target) {

		double width = target.right - target.left;
		double height = target.bottom - target.top;

		return new Rect((int) (left * width) + target.left, (int) (top * height) + target.top, (int) (right * width)
				+ target.left, (int) (bottom * height) + target.top);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Area [method=" + method + ", openGroup=" + openGroup + "]";
	}

	@Override
	public String getRpcMethod() {

		return method;
	}

	@Override
	public void setToggled(boolean toggle) {
		this.toggled = toggle;
	}

	@Override
	public boolean toggle() {
		toggled = !toggled;
		return toggled;
	}

	@Override
	public boolean isToggled() {
		return toggled;
	}

	@Override
	public void setParentActivity(InputGroupActivity activity) {
		this.parentActivity = activity;
	}

	@Override
	public SVG getToggleImage() {
		return toggleimage;
	}

	@Override
	public boolean usesToggleImage() {
		return toggleimage != null;
	}

	public String getDirection() {
		return direction;
	}

	@Override
	public void onSlide(FloatPoint from, FloatPoint to, float distanceX, float distanceY, Rect rect) {
		/*
		 * do nothing here
		 */
	}

	@Override
	public void onLongPress(FloatPoint touch, Rect rectangle) {
		/*
		 * do nothing here
		 */
	}

	@Override
	public void onFling(FloatPoint touch, FloatPoint scroll, float velocityX, float velocityY, Rect rectangle) {
		/*
		 * do nothing here
		 */
	}

	@Override
	public void onSingleTapUp(FloatPoint touch, Rect rectangle) {
		toggle();

		if(method != null) {
			parentActivity.sendMethodCallToServer(method, (toggled ? Input.TRUE : Input.FALSE), true);
		}

		if(openGroup != null) {
			MainActivity.openInputGroupActivity(openGroup);
		}
	}

	@Override
	public String getValue() {
		return "" + isToggled();
	}

}
