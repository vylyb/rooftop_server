package app.view.drawings.areas;

import share.inputs.ImageInputArea;
import share.inputs.Input;
import android.graphics.Rect;
import app.view.drawings.util.FloatPoint;

import com.caverock.androidsvg.SVG;

/**
 * class that implements a {@link DrawingAreaInterface} and represents a
 * rectangle with optional images that can be expanded in one or four directions
 * and has the same purpose as a slider
 * 
 * @author Philipp Hempel
 *
 */
public class SlidableArea extends ClickableArea implements DrawingAreaInterface {

	/**
	 * the value to which this is extended (between 0.0 and 1.0)
	 */
	private double value = 1.0;

	public SlidableArea(SVG image, String direction, ImageInputArea area) {
		super(image, area);
		this.direction = direction;
	}

	public SlidableArea(String direction, ImageInputArea area) {
		super(area);
		this.direction = direction;
	}

	public SlidableArea(SVG image, SVG toggleimage, String direction, ImageInputArea area) {
		super(image, toggleimage, area);
		this.direction = direction;
	}

	@Override
	public void onPress(FloatPoint point) {
	}

	@Override
	public void onSlide(FloatPoint from, FloatPoint to, float distanceX, float distanceY, Rect targetRectangle) {
		stretchSliderAlong(from, to, targetRectangle);
	}

	@Override
	public void onTouch(FloatPoint point, Rect targetRectangle) {
		if(usesToggleImage()) {
		} else {
			stretchSliderAlong(null, point, targetRectangle);
		}
	}

	@Override
	public void onSingleTapUp(FloatPoint point, Rect targetRectangle) {
		if(usesToggleImage()) {
			/*
			 * this slider is also a button, so call the onSingleTapUp method of
			 * the clickableArea
			 */
			super.onSingleTapUp(point, targetRectangle);
		} else {
		}
	}

	@Override
	public void onLongPress(FloatPoint touch, Rect rectangle) {
		/*
		 * either set the slider to 0 or 1, depending on whether the current
		 * value is < 0.5
		 */
		if(value < 0.5)
			value = 0.0;
		else
			value = 1.0;

		valueChanged();
	}

	/**
	 * sends a method call to the server with the current value as the parameter
	 */
	private void valueChanged() {

		if(method != null) {
			parentActivity.sendMethodCallToServer(method, "" + value, true);
		}
	}

	@Override
	public void onFling(FloatPoint touch, FloatPoint scroll, float velocityX, float velocityY, Rect rectangle) {
		/*
		 * either set the slider to 0 or 1, depending on the direction of the
		 * fling gesture
		 */
		switch (direction){

		case Input.UP:
			if(velocityY > 0)
				value = 0.0;
			else
				value = 1.0;

			valueChanged();
			break;

		case Input.DOWN:
			if(velocityY < 0)
				value = 0.0;
			else
				value = 1.0;

			valueChanged();
			break;

		case Input.LEFT:
			if(velocityX > 0)
				value = 0.0;
			else
				value = 1.0;

			valueChanged();
			break;

		case Input.RIGHT:
			if(velocityX < 0)
				value = 0.0;
			else
				value = 1.0;

			valueChanged();
			break;

		default:
			break;
		}
	}

	private void stretchSliderAlong(FloatPoint from, FloatPoint to, Rect targetRectangle) {

		double imageHeight = targetRectangle.bottom - targetRectangle.top;
		double imageWidth = targetRectangle.right - targetRectangle.left;

		if(direction != null) {
			/*
			 * depending on the direction calculate a new current value from the
			 * point where the user touched
			 */

			switch (direction){

			case Input.UP:
				setValue(1.0 - ((to.getY() - targetRectangle.top) / imageHeight));

				valueChanged();
				break;

			case Input.DOWN:
				setValue((to.getY() - targetRectangle.top) / imageHeight);

				valueChanged();
				break;

			case Input.LEFT:
				setValue(1.0 - ((to.getX() - targetRectangle.left) / imageWidth));

				valueChanged();
				break;

			case Input.RIGHT:
				setValue((to.getX() - targetRectangle.left) / imageWidth);

				valueChanged();
				break;

			default:
				break;
			}
		} else {
			/*
			 * stretch the slidable in all directions simultaneously
			 */
			FloatPoint center = new FloatPoint((targetRectangle.left + targetRectangle.right) / 2,
					(targetRectangle.top + targetRectangle.bottom) / 2);

			double dx = (double) Math.abs(to.getX() - center.getX());
			double dy = (double) Math.abs(to.getY() - center.getY());

			double ratio = 0;
			if(dy == 0.0) {
				ratio = Double.MAX_VALUE;
			} else {
				ratio = (double) (dx / dy);
			}

			if(ratio <= (imageWidth / imageHeight)) {
				setValue((2.0 * dy) / imageHeight);
			} else {
				setValue((2.0 * dx) / imageWidth);
			}

			valueChanged();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[method=" + method + ", direction=" + direction + "]";
	}

	@Override
	public String getValue() {
		return "" + value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(double value) {
		if(value < 0.0)
			this.value = 0.0;
		else if(value > 1.0)
			this.value = 1.0;
		else
			this.value = value;
	}

}
