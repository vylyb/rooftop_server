'''
Created on 02.06.2014

@author: Philipp Hempel

This Server communicates with the Java Server which is responsible for communicating with the app.
The communication is based on remote procedure calls (RPC) over localhost.

Code based on https://docs.python.org/2/library/simplexmlrpcserver.html#module-SimpleXMLRPCServer
'''

from SimpleXMLRPCServer import SimpleXMLRPCServer
from SimpleXMLRPCServer import SimpleXMLRPCRequestHandler
from RpcFunctions import RpcCalls
import sys

'''
    class that defines a request handler
'''
class RequestHandler(SimpleXMLRPCRequestHandler):
    '''
        register the request handlers
    '''
    rpc_paths = ('/RpcCalls',)


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "Please give the port number as the first argument"

    else:
        '''
            get the port from the command line
        '''
        port = int(sys.argv[1])

        '''
            Create server, get the port from the arguments and start the main loop
        '''
        server = SimpleXMLRPCServer(("localhost", port), requestHandler = RequestHandler)
        server.register_introspection_functions()
        '''
            register the request handlers for the input groups
        '''
        server.register_instance(RpcCalls())

        print "Starting local hardware control server @ localhost:" + str(port)
        try:
            server.serve_forever()
        except:
            print "RPC Server shutting down"
