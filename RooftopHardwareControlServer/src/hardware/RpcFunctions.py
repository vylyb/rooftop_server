# Generated Request Handler
# Time: Fri May 15 22:07:25 CEST 2015

class RpcCalls:

	# function that returns true if called, to check if the connection is working and the rpc server answers method calls
	def checkConnection(self):
		print "Call for connection check, returning True"
		return True

	# ################################################ #
	# Methods for Input Group 'Beleuchtung Wohnzimmer' #
	# ################################################ #

	# Method for StandardInput 'Deckenbeleuchtung 1'
	def schalteLampe01(self, value):
		print "Method Call for schalteLampe01(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for StandardInput 'Deckenbeleuchtung 2'
	def schalteLampe02(self, value):
		print "Method Call for schalteLampe02(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for StandardInput 'Stehlampe (Schalter)'
	def schalteLampe03(self, value):
		print "Method Call for schalteLampe03(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for StandardInput 'Stehlampe (Dimmer)'
	def dimmLampe03(self, value):
		print "Method Call for dimmLampe03(" + str(value) + ")"
		# TODO implement functionality
		return True


	# ################################################## #
	# Methods for Input Group 'Beleuchtung Schlafzimmer' #
	# ################################################## #


	# ############################################## #
	# Methods for Input Group 'Grundriss des Hauses' #
	# ############################################## #

	# No Method defined for ImageInputArea 'Area1'

	# No Method defined for ImageInputArea 'Area2'

	# No Method defined for ImageInputArea 'Area3'

	# No Method defined for ImageInputArea 'Area4'

	# No Method defined for ImageInputArea 'Area5'

	# No Method defined for ImageInputArea 'Area6'

	# No Method defined for ImageInputArea 'Area7'

	# No Method defined for ImageInputArea 'Area8'

	# No Method defined for ImageInputArea 'Area9'


	# ############################################### #
	# Methods for Input Group 'Wohnzimmerbeleuchtung' #
	# ############################################### #

	# Method for ImageInputArea 'Area1'
	def lamp1(self, value):
		print "Method Call for lamp1(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area2'
	def lamp2(self, value):
		print "Method Call for lamp2(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area3'
	def lamp3(self, value):
		print "Method Call for lamp3(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area4'
	def lamp4(self, value):
		print "Method Call for lamp4(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area5'
	def lamp5(self, value):
		print "Method Call for lamp5(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area6'
	def lamp6(self, value):
		print "Method Call for lamp6(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area7'
	def lamp7(self, value):
		print "Method Call for lamp7(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area8'
	def lamp8(self, value):
		print "Method Call for lamp8(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area9'
	def lamp9(self, value):
		print "Method Call for lamp9(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area10'
	def lamp10(self, value):
		print "Method Call for lamp10(" + str(value) + ")"
		# TODO implement functionality
		return True


	# ##################################################### #
	# Methods for Input Group 'Fassadensteuerung Suedseite' #
	# ##################################################### #

	# Method for ImageInputArea 'Area1'
	def fassade1(self, value):
		print "Method Call for fassade1(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area2'
	def fassade2(self, value):
		print "Method Call for fassade2(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area3'
	def fassade3(self, value):
		print "Method Call for fassade3(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area4'
	def fassade4(self, value):
		print "Method Call for fassade4(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area5'
	def fassade5(self, value):
		print "Method Call for fassade5(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area6'
	def fassade6(self, value):
		print "Method Call for fassade6(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area7'
	def fassade7(self, value):
		print "Method Call for fassade7(" + str(value) + ")"
		# TODO implement functionality
		return True

	# Method for ImageInputArea 'Area8'
	def fassade8(self, value):
		print "Method Call for fassade8(" + str(value) + ")"
		# TODO implement functionality
		return True

