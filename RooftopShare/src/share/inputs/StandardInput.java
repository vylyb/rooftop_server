package share.inputs;

/**
 * class that represents standard input object like a button, a textfield or a
 * slider
 * 
 * @author Philipp Hempel
 *
 */
public class StandardInput extends Input {

	/**
	 * @param unit
	 *            the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * the real life unit this value represents
	 */
	private String unit;

	/**
	 * constructor that sets all fields that have to be set
	 */
	public StandardInput(String type, String method) {
		this.type = type;
		this.rpcMethod = method;
	}

	@Override
	public String toString() {
		return "\n\tStandardInput [unit=" + unit + ", value=" + startValue + ", rpcMethod=" + rpcMethod + ", type="
				+ type + ", name=" + name + ", minValue=" + minValue + ", maxValue=" + maxValue + "]";
	}

}
