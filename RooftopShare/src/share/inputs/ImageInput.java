package share.inputs;

import java.util.ArrayList;

/**
 * class that represents an input in the form of an image
 * 
 * @author Philipp Hempel
 *
 */
public class ImageInput extends Input {

	/**
	 * array that saves all the areas inside the image
	 */
	private ArrayList<ImageInputArea> areas;

	/**
	 * the path to the file with the image.
	 */
	private String file;

	/**
	 * @return the file
	 */
	public String getFile() {
		return file;
	}

	/**
	 * constructor that sets all fields that must be set
	 */
	public ImageInput(String file) {

		areas = new ArrayList<ImageInputArea>();

		this.setFile(file);
	}

	/**
	 * method that adds a new {@link ImageInputArea} to the list {@link #areas}
	 * 
	 * @return the new number of areas
	 */
	public int addArea(ImageInputArea area) {
		areas.add(area);
		return countAreas();
	}

	/**
	 * @return the {@link ImageInputArea} from the array {@link #areas} at the
	 *         given index, or <code>null</code> if the index is not valid
	 */
	public ImageInputArea getArea(int index) {
		if(index < 0 || index >= countAreas())
			return null;
		return areas.get(index);
	}

	/**
	 * @return the number of {@link ImageInputArea} in {@link #areas}
	 */
	public int countAreas() {
		return areas.size();
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "\n\tImageInput [areas=" + areas + ", file=" + file + ", name=" + name + "]";
	}

}
