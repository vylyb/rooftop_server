package share.inputs;

/**
 * class that maps the an input in the app to the name of a command inside the
 * rpc server. it also specifies the data type this input requires
 * 
 * @author Philipp Hempel
 * 
 */
public abstract class Input {

	/**
	 * the name of the function in the rpc server. MUST BE SET
	 */
	protected String rpcMethod;
	/**
	 * the type of the input. MUST BE SET
	 */
	protected String type;

	/**
	 * the name of this input as it will be displayed in the app. optional
	 */
	protected String name;
	/**
	 * minimum value for this input, only usefull if this is a numeral input.
	 * optional
	 */
	protected double minValue = Double.NaN;
	/**
	 * maximum value for this input, only usefull if this is a numeral input.
	 * optional
	 */
	protected double maxValue = Double.NaN;
	/**
	 * the start value of this input. could be a text or a number or... its
	 * saved as a string and converted to the right type inside the app
	 */
	protected String startValue = null;
	/**
	 * the current value of this input
	 */
	protected String currentValue;
	/**
	 * name of the input group that is opened if this is clicked
	 */
	protected String openGroup;

	/*
	 * string constants used in the database and the json objects that are
	 * exchanged between server and app
	 */
	public final static String MAP = "inputmap";
	public final static String INPUT_GROUP = "inputgroup";
	public final static String GROUP_NAME = "groupname";
	public final static String CATEGORY = "category";
	public final static String INPUT = "input";
	public final static String INPUTS = "inputs";
	public final static String IMAGE_INPUT = "imageinput";
	public final static String AREA = "area";
	/*
	 * input types
	 */
	public final static String TYPE_SLIDER = "slider";
	public final static String TYPE_BUTTON = "button";
	public final static String TYPE_NUMFIELD = "numberfield";
	public final static String TYPE_TEXTFIELD = "textfield";
	public final static String TYPE_CHECKBOX = "checkbox";
	public final static String TYPE_DROPDOWN = "dropdown";
	/*
	 * area types
	 */
	public final static String AREA_TYPE_CLICK = "clickable";
	public final static String AREA_TYPE_SLIDER = "slidable";
	/*
	 * transmit types for input groups
	 */
	public final static String TRANSMIT_ON_CHANGE = "onchange";
	public final static String TRANSMIT_ON_CONFIRM = "confirm";
	public final static String TRANSMIT = "transmit";
	public final static String NAME = "name";
	public final static String TYPE = "type";
	public final static String METHOD = "method";
	public final static String OPEN_GROUP = "open";
	public final static String MIN_VALUE = "min";
	public final static String MAX_VALUE = "max";
	public final static String START_VALUE = "value";
	public final static String UNIT = "unit";
	public final static String FILE = "file";
	public final static String FILE_2 = "file2";
	public final static String LEFT = "left";
	public final static String TOP = "top";
	public final static String WIDTH = "width";
	public final static String HEIGHT = "height";
	/*
	 * for slidable areas
	 */
	public static final String DIRECTION = "dir";
	public static final String UP = "up";
	public static final String DOWN = "down";
	// left already exists
	public static final String RIGHT = "right";

	/**
	 * all attributes that must be specified for an {@link InputGroup}
	 */
	public final static String inputGroupAttributes[] = { GROUP_NAME, TRANSMIT };
	/**
	 * all optional attributes for an {@link InputGroup}
	 */
	public final static String inputGroupOptionalAttributes[] = { CATEGORY };
	/**
	 * all attributes that must be specified for a {@link StandardInput}
	 */
	public final static String inputAttributes[] = { TYPE, METHOD };
	/**
	 * all optional attributes for a {@link StandardInput}
	 */
	public final static String inputOptionalAttributes[] = { NAME, MIN_VALUE, MAX_VALUE, START_VALUE, UNIT };
	/**
	 * all attributes that must be specified for an {@link ImageInputArea}
	 */
	public final static String inputImageAreaAttributes[] = { TYPE, LEFT, TOP, WIDTH, HEIGHT };
	/**
	 * all optional attributes for an {@link ImageInputArea}
	 */
	public final static String inputImageAreaOptionalAttributes[] = { METHOD, OPEN_GROUP, FILE, FILE_2, DIRECTION,
			MIN_VALUE, MAX_VALUE, START_VALUE };
	/**
	 * all attributes that must be specified for an {@link ImageInput}
	 */
	public final static String imageInputAttributes[] = { FILE };
	/**
	 * all optional attributes for an {@link ImageInput}
	 */
	public final static String imageInputOptionalAttributes[] = { NAME };
	/*
	 * other constants
	 */
	public static final String TRUE = "True";
	public static final String FALSE = "False";

	/**
	 * @return the currentValue
	 */
	public String getCurrentValue() {
		return currentValue;
	}

	/**
	 * @param d
	 *            the currentValue to set
	 */
	public void setCurrentValue(String d) {
		this.currentValue = d;// ensureValueInRange(d);
	}

	/**
	 * @param d
	 *            the start value to set
	 */
	public void setStartValue(String d) {
		// startValue = ensureValueInRange(startValue);
		this.startValue = d;
		setCurrentValue(d);
	}

	/**
	 * ensures that the double value which is represented by the string (if it
	 * is a double value) is inside the min and max values (if specified)
	 */
	public double ensureValueInRange(double val) {

		if(isMaxValueSet() || isMinValueSet()) {
			/*
			 * if the value is greater than the max value return the max value
			 */
			if(isMaxValueSet() && val > maxValue)
				return maxValue;

			/*
			 * if the value is smaller than the min value return the min value
			 */
			if(isMinValueSet() && val < minValue)
				return minValue;
		}

		/*
		 * no min or max value specified, return the original value
		 */
		return val;
	}

	/**
	 * @return the start value
	 */
	public String getStartValue() {
		return startValue;
	}

	/**
	 * @return <code>true</code> if the start value is set
	 */
	public boolean isStartValueSet() {
		return startValue != null;
	}

	/**
	 * @return <code>true</code> if the min value is set
	 */
	public boolean isMinValueSet() {
		return minValue != Double.NaN;
	}

	/**
	 * @return <code>true</code> if the max value is set
	 */
	public boolean isMaxValueSet() {
		return maxValue != Double.NaN;
	}

	/**
	 * @return <code>true</code> if the {@link #type} allows a min and max
	 *         value, and the values are set
	 */
	public boolean areMinAndMaxValueSet() {
		return isMinValueSet() && isMaxValueSet();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the minValue
	 */
	public double getMinValue() {
		return minValue;
	}

	/**
	 * @param minValue
	 *            the minValue to set
	 */
	public void setMinValue(double minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the maxValue
	 */
	public double getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue
	 *            the maxValue to set
	 */
	public void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * @return the rpcMethod
	 */
	public String getRpcMethod() {
		return rpcMethod;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param rpcMethod
	 *            the rpcMethod to set
	 */
	public void setRpcMethod(String rpcMethod) {
		this.rpcMethod = rpcMethod;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the openGroup
	 */
	public String getOpenGroup() {
		return openGroup;
	}

	/**
	 * @param openGroup
	 *            the openGroup to set
	 */
	public void setOpenGroup(String openGroup) {
		this.openGroup = openGroup;
	}

}
