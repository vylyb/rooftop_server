package share.inputs;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * class that represents a group of {@link Input} object that belong together,
 * e.g. because the devices that are controlled are in the same room
 * 
 * @author Philipp Hempel
 *
 */
public class InputGroup {

	/**
	 * the name of this input group that is displayed in the app. MUST BE SET
	 */
	private String name;
	/**
	 * the procedure for sending values to the server. MUST BE SET
	 */
	private String transmit;
	/**
	 * the category to which this group belongs. optional
	 */
	private String category;
	/**
	 * hash map that maps the name of a method to the {@link Input}
	 */
	private HashMap<String, Input> inputs;
	/**
	 * the id of the group inside the database
	 */
	private int id;

	/**
	 * standard constructor
	 * 
	 * @param id
	 *            value for {@link #id}
	 * @param transmit
	 *            value for {@link #transmit}
	 */
	public InputGroup(int id, String transmit) {
		this.transmit = transmit;
		this.id = id;
		inputs = new HashMap<String, Input>();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the procedure for sending values to the server
	 */
	public String getTransmit() {
		return transmit;
	}

	/**
	 * saves the given {@link Input} in the hash map with the rpc method name as
	 * the key
	 */
	public void addInput(Input input) {
		inputs.put(input.getRpcMethod(), input);
	}

	/**
	 * @return the {@link Input} from the hash map with the given rpc method
	 *         name as the key, or <code>null</code> if the key is not valid
	 */
	public Input getInput(String method) {
		return inputs.get(method);
	}

	@Override
	public String toString() {
		return "InputGroup [name=" + name + ", transmit=" + transmit + ", category=" + category + ", inputs="
				+ inputs.values() + "]";
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public ArrayList<Input> inputsToArray() {
		ArrayList<Input> inputList = new ArrayList<Input>();
		for(String key : inputs.keySet()) {
			inputList.add(inputs.get(key));
		}
		return inputList;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

}
