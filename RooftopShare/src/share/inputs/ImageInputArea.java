package share.inputs;

/**
 * class that represents a part of an {@link ImageInput} that can contain
 * functionality and can react to a user gesture
 * 
 * @author Philipp Hempel
 *
 */
public class ImageInputArea extends Input {

	/**
	 * the x value of the left upper corner. MUST BE SET
	 */
	private double left;
	/**
	 * the y value of the left upper corner. MUST BE SET
	 */
	private double top;
	/**
	 * the width of the area. MUST BE SET
	 */
	private double width;
	/**
	 * the height of the area. MUST BE SET
	 */
	private double height;
	/**
	 * the path to the file with the image. optional
	 */
	private String file;
	/**
	 * if this is a slideable image area, then this saves the direction to where
	 * it can be slided (left, right, up or down)
	 */
	private String direction;

	/**
	 * the path to the file with the second image that is displayed when the
	 * input is toggled.
	 */
	private String togglefile;

	/**
	 * @return the togglefile
	 */
	public String getTogglefile() {
		return togglefile;
	}

	/**
	 * @param togglefile
	 *            the togglefile to set
	 */
	public void setTogglefile(String togglefile) {
		this.togglefile = togglefile;
	}

	/**
	 * constructor that sets all fields that have to be set
	 */
	public ImageInputArea(String type, double left, double top, double width, double height) {
		this.type = type;
		this.left = left;
		this.top = top;
		this.width = width;
		this.height = height;
	}

	/**
	 * @return the x value of the left upper corner
	 */
	public double getLeft() {
		return left;
	}

	/**
	 * @return the y value of the left upper corner
	 */
	public double getTop() {
		return top;
	}

	/**
	 * @return the width of the area
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * @param left
	 *            the left to set
	 */
	public void setLeft(double left) {
		this.left = left;
	}

	/**
	 * @param top
	 *            the top to set
	 */
	public void setTop(double top) {
		this.top = top;
	}

	/**
	 * @param width
	 *            the width to set
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * @param height
	 *            the height to set
	 */
	public void setHeight(double height) {
		this.height = height;
	}

	/**
	 * @return the height of the area
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * @return the file
	 */
	public String getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}

	public boolean usesImage() {
		return file != null;
	}

	@Override
	public String toString() {
		return "ImageInputArea [left=" + left + ", top=" + top + ", width=" + width + ", height=" + height + ", file="
				+ file + ", rpcMethod=" + rpcMethod + ", type=" + type + ", name=" + name + ", minValue=" + minValue
				+ ", maxValue=" + maxValue + ", value=" + startValue + "]";
	}

	/**
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * @param direction
	 *            the direction to set
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}

}
