package share.inputs;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import share.message.NetworkMessage;

/**
 * class that helps creating {@link InputGroup} objects from a
 * {@link JSONObject}
 * 
 * @author Philipp Hempel
 *
 */
public class InputFactory {

	/**
	 * @param json
	 *            the {@link JSONObject} with the data of the {@link InputGroup}
	 * @return {@link ArrayList} of {@link InputGroup} objects if the
	 *         {@link JSONObject} contained the correct data, or
	 *         <code>null</code> if the {@link JSONObject} could not be analyzed
	 *         or contained wrong data
	 */
	public static HashMap<Integer, InputGroup> createInputGroupListFromJson(JSONObject json) {

		if(!json.has(Input.INPUT_GROUP))
			return null;

		try {
			HashMap<Integer, InputGroup> list = new HashMap<Integer, InputGroup>();

			JSONArray groupArray = json.getJSONArray(Input.INPUT_GROUP);
			for(int i = 0; i < groupArray.length(); i++) {
				JSONObject groupObject = (JSONObject) groupArray.get(i);
				// System.out.println(groupObject.toString(2));

				InputGroup group = new InputGroup(groupObject.getInt(NetworkMessage.JSON_ID),
						groupObject.getString(Input.TRANSMIT));

				group.setName(groupObject.getString(Input.GROUP_NAME));
				group.setCategory(groupObject.getString(Input.CATEGORY));

				/*
				 * check for standard inputs
				 */
				if(groupObject.has(Input.INPUT)) {
					JSONArray inputArray = groupObject.getJSONArray(Input.INPUT);

					for(int k = 0; k < inputArray.length(); k++) {
						JSONObject inputObject = (JSONObject) inputArray.get(k);

						/*
						 * create a new StandardInput object with the fields
						 * that must be set
						 */
						StandardInput input = new StandardInput(inputObject.getString(Input.TYPE),
								inputObject.getString(Input.METHOD));
						/*
						 * set the optional fields
						 */
						if(inputObject.has(Input.NAME)) {
							input.setName(inputObject.getString(Input.NAME));
						}
						if(inputObject.has(Input.UNIT)) {
							input.setUnit(inputObject.getString(Input.UNIT));
						}
						if(inputObject.has(Input.START_VALUE)) {
							input.setStartValue(inputObject.getString(Input.START_VALUE));
						}
						if(inputObject.has(Input.MIN_VALUE)) {
							try {
								input.setMinValue(Double.parseDouble(inputObject.getString(Input.MIN_VALUE)));
							} catch(NumberFormatException e) {
							}
						}
						if(inputObject.has(Input.MAX_VALUE)) {
							try {
								input.setMaxValue(Double.parseDouble(inputObject.getString(Input.MAX_VALUE)));
							} catch(NumberFormatException e) {
							}
						}

						// System.out.println(input);
						group.addInput(input);
					}

				}

				/*
				 * check for image area inputs
				 */
				if(groupObject.has(Input.IMAGE_INPUT)) {
					JSONObject inputObject = groupObject.getJSONObject(Input.IMAGE_INPUT);

					/*
					 * create a new ImageInput object with the fields that must
					 * be set
					 */
					ImageInput input = new ImageInput(inputObject.getString(Input.FILE));
					/*
					 * set the optional fields
					 */
					if(inputObject.has(Input.NAME)) {
						input.setName(inputObject.getString(Input.NAME));
					}

					/*
					 * add the image areas
					 */
					if(inputObject.has(Input.AREA)) {
						JSONArray areaArray = inputObject.getJSONArray(Input.AREA);

						for(int l = 0; l < areaArray.length(); l++) {
							JSONObject areaObject = (JSONObject) areaArray.get(l);

							/*
							 * create a new ImageInput object with the fields
							 * that must be set
							 */
							ImageInputArea area = new ImageInputArea(areaObject.getString(Input.TYPE),
									areaObject.getDouble(Input.LEFT), areaObject.getDouble(Input.TOP),
									areaObject.getDouble(Input.WIDTH), areaObject.getDouble(Input.HEIGHT));
							/*
							 * set the optional fields
							 */
							if(areaObject.has(Input.FILE)) {
								area.setFile(areaObject.getString(Input.FILE));
							}
							if(areaObject.has(Input.FILE_2)) {
								area.setTogglefile(areaObject.getString(Input.FILE_2));
							}
							if(areaObject.has(Input.DIRECTION)) {
								area.setDirection(areaObject.getString(Input.DIRECTION));
							}
							if(areaObject.has(Input.MIN_VALUE)) {
								area.setMinValue(areaObject.getDouble(Input.MIN_VALUE));
							}
							if(areaObject.has(Input.MAX_VALUE)) {
								area.setMaxValue(areaObject.getDouble(Input.MAX_VALUE));
							}
							if(areaObject.has(Input.START_VALUE)) {
								area.setStartValue(areaObject.getString(Input.START_VALUE));
							}
							if(areaObject.has(Input.METHOD)) {
								area.setRpcMethod(areaObject.getString(Input.METHOD));
							}
							if(areaObject.has(Input.OPEN_GROUP)) {
								area.setOpenGroup(areaObject.getString(Input.OPEN_GROUP));
							}

							// System.out.println(area);
							input.addArea(area);
						}
					}

					// System.out.println(input);
					group.addInput(input);

				}
				/*
				 * add the group to the list
				 */
				list.put(group.getId(), group);
			}

			/*
			 * everything ok, return the list
			 */
			return list;
		} catch(JSONException e) {
			e.printStackTrace();
		}

		return null;
	}
}
