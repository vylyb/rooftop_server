package share.message;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import share.encoding.Base64Util;
import share.exceptions.EncodingException;
import share.exceptions.ServerMessageChecksumException;
import share.exceptions.ServerMessageEncodingException;
import share.exceptions.ServerMessageLengthException;
import share.exceptions.ServerMessageRegexException;
import share.security.AES;

/**
 * class that represents a message that is exchanged between the server and the
 * app. it helps encoding and decoding the message object to and from a
 * {@link String} that can be encrypeted and sent of the network
 * 
 * @author Philipp Hempel
 *
 */
public abstract class NetworkMessage {

	/*
	 * communication constants, content doesnt really matter as long as it is
	 * unique and short
	 */
	public static final String JSON_ID = "ID";
	public static final String JSON_USER_NAME = "USER";
	public static final String JSON_PASSWORD = "PW";
	public static final String JSON_USER_ROLE = "UROL";
	public static final String JSON_PUBLIC_KEY = "PUBK";
	public static final String JSON_AES_KEY = "AESK";
	public static final String JSON_RANDOM_BITS = "RB";
	public static final String JSON_SALT = "SALT";
	public static final String JSON_CALL_METHOD = "CM";
	public static final String JSON_CALL_PARAM = "CP";
	public static final String JSON_METHOD_CALLS = "CALLS";
	public static final String JSON_ACCOUNT_LIST = "UAL";
	public static final String JSON_USER_ACCESS_RIGHTS = "UAR";
	public static final String MSG_DELETE_USER = "DU";
	public static final String MSG_REQUEST_USER_ACCESS_RIGHTS = "RQUAR";
	public static final String MSG_HELLO = "HELLO";
	public static final String MSG_BYE = "BYE";
	public static final String MSG_ACK = "ACK";
	public static final String MSG_LOGIN_WRONG = "LW";
	public static final String MSG_REREQUEST = "RRQ";
	public static final String MSG_REQUEST_PUBLIC_KEY = "RQPK";
	public static final String MSG_ERROR_RECV_PUBLIC_KEY = "ERPK";
	public static final String MSG_REQUEST_INPUT_CONFIGURATION = "RICON";
	public static final String MSG_FILE_CONTENT = "RQFC";
	public static final String MSG_REQUEST_USER_ACCOUNT_DETAIL = "RQUA";
	public static final String MSG_REQUEST_USER_ACCOUNT_LIST = "RQAL";

	/**
	 * number of characters the indicator for the {@link #type} takes up in the
	 * header
	 */
	public final static int TYPE_DIGITS = 1;
	/**
	 * number of characters the indicator for the length of the {@link #payload}
	 * takes up in the header
	 */
	public final static int LENGTH_DIGITS = 6;
	/**
	 * number of characters the indicator for the length of the checksum takes
	 * up in the header
	 */
	public final static int CHECK_DIGITS = 8;

	/**
	 * regex for message structure: 1 char - message type (hex) | 6 char -
	 * length of payload (hex) | 8 char - checksum of payload | nonempty, base64
	 * encoded payload <br>
	 * see <a href=
	 * "http://www.regexplanet.com/cookbook/ahJzfnJlZ2V4cGxhbmV0LWhyZHNyDwsSBlJlY2lwZRiqjsIMDA/index.html"
	 * >http://www.regexplanet.com/cookbook/
	 * ahJzfnJlZ2V4cGxhbmV0LWhyZHNyDwsSBlJlY2lwZRiqjsIMDA/index.html</a>
	 */
	public final static Pattern REGEX = Pattern.compile("[0-9a-fA-F]{" + (TYPE_DIGITS + LENGTH_DIGITS + CHECK_DIGITS)
			+ "}[A-Za-z0-9+/]+[=]{0,2}");

	/**
	 * the type of this message
	 */
	protected NetworkMessageType type;

	/**
	 * the data of this message
	 */
	protected String payload;

	/**
	 * constructor for a new network message
	 * 
	 * @param type
	 *            the type of the message
	 * @param payload
	 *            the payload of the message
	 */
	public NetworkMessage(NetworkMessageType type, String payload) {
		this.type = type;
		this.payload = payload;
	}

	/**
	 * getter for {@link #type}
	 * 
	 * @return
	 */
	public NetworkMessageType getType() {
		return type;
	}

	/**
	 * getter for {@link #payload}
	 * 
	 * @return
	 */
	public String getPayload() {
		return payload;
	}

	/**
	 * method that returns a {@link String} representation of this message.<br>
	 * the payload is base64-encoded. the message will then be encrypted using
	 * the aes key
	 */
	@Override
	public String toString() {
		try {
			String load = Base64Util.toBase64(payload);
			String len = Integer.toHexString(load.length());
			if(len.length() <= LENGTH_DIGITS) {
				while(len.length() < LENGTH_DIGITS)
					len = "0" + len;
				return Integer.toHexString(type.toID()) + len + generateChecksum(load) + load;
			}
		} catch(EncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * formats the message to a readable string
	 * 
	 * @return
	 */
	public String printMessage() {
		return "[Type: " + getClass().getSimpleName() + ", Payload: " + getPayload() + ", Checksum: "
				+ generateChecksum(getPayload()) + "]";
	}

	/**
	 * checks if two {@link NetworkMessage} objects are equal.
	 * 
	 * @return <code>true</code> if both messages have the same {@link #type}
	 *         and their {@link #payload} is equal; else <code>false</code>
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof NetworkMessage) {
			if(((NetworkMessage) obj).type != type)
				return false;
			if(((NetworkMessage) obj).payload.compareTo(payload) != 0)
				return false;

			return true;
		}
		return false;
	}

	/**
	 * creates a {@link NetworkMessage} object from the given {@link String}, if
	 * this is possible
	 * 
	 * @param string
	 *            the String that describes the message
	 * @return {@link NetworkMessage} object if the parsing of the
	 *         {@link String} was successful
	 * @throws ServerMessageRegexException
	 *             if the input strign doesn't match the regex
	 * @throws ServerMessageLengthException
	 *             if the length of the encoded payload doesn't match the length
	 *             given in the header
	 * @throws ServerMessageChecksumException
	 *             if the sha1 checksum of the decoded payload doesn't match the
	 *             checksum given in the header
	 * @throws ServerMessageEncodingException
	 *             if an error occured during the conversion from/to base64
	 * @throws ServerMessageNumberException
	 *             if there was a problem
	 * @throws EncodingException
	 *             if an error occured during the conversion from/to base64
	 * @throws NumberFormatException
	 *             if there was an error parsing a number from a string
	 */
	public static NetworkMessage fromString(String string) throws NullPointerException, ServerMessageRegexException,
			ServerMessageLengthException, ServerMessageChecksumException, NumberFormatException, EncodingException {

		if(string == null) {
			throw new NullPointerException("Input string was null!");
		}
		if(!REGEX.matcher(string).matches()) {
			throw new ServerMessageRegexException();
		}

		String strType = string.substring(0, TYPE_DIGITS);
		NetworkMessageType type = NetworkMessageType.fromString(strType);

		String strLength = string.substring(TYPE_DIGITS, TYPE_DIGITS + LENGTH_DIGITS);
		int testLength = Integer.parseInt(strLength, 16);

		String checksum = string.substring(TYPE_DIGITS + LENGTH_DIGITS, TYPE_DIGITS + LENGTH_DIGITS + CHECK_DIGITS);

		String encodedPayload = string.substring(TYPE_DIGITS + LENGTH_DIGITS + CHECK_DIGITS, string.length());

		NetworkMessage message = null;

		if(encodedPayload.length() != testLength) {
			throw new ServerMessageLengthException();
		}

		if(!stringMatchesChecksum(encodedPayload, checksum)) {
			throw new ServerMessageChecksumException(encodedPayload, checksum);
		}

		String decodedPayload = Base64Util.stringFromBase64(encodedPayload);

		switch (type){
		case ERROR:
			message = new ErrorNetworkMessage(decodedPayload);
			break;

		case INFO:
			message = new InfoNetworkMessage(decodedPayload);
			break;

		case WARN:
			message = new WarningNetworkMessage(decodedPayload);
			break;

		case JSON:
			message = new JsonNetworkMessage(decodedPayload);
			break;
		}

		return message;
	}

	/**
	 * helper method
	 * 
	 * @return <code>true</code> if {@link #type} equals
	 *         {@link NetworkMessageType}.JSON, else <code>false</code>
	 */
	public boolean isJsonMessage() {
		return isType(NetworkMessageType.JSON);
	}

	/**
	 * helper method
	 * 
	 * @return <code>true</code> if {@link #type} equals
	 *         {@link NetworkMessageType}.INFO, else <code>false</code>
	 */
	public boolean isInfoMessage() {
		return isType(NetworkMessageType.INFO);
	}

	/**
	 * helper method
	 * 
	 * @return <code>true</code> if {@link #type} equals
	 *         {@link NetworkMessageType}.WARN, else <code>false</code>
	 */
	public boolean isWarningMessage() {
		return isType(NetworkMessageType.WARN);
	}

	/**
	 * helper method
	 * 
	 * @return <code>true</code> if {@link #type} equals
	 *         {@link NetworkMessageType}.ERROR, else <code>false</code>
	 */
	public boolean isErrorMessage() {
		return isType(NetworkMessageType.ERROR);
	}

	/**
	 * helper method to check if the message is an acknowledgement
	 * 
	 * @return
	 */
	public boolean isAckMessage() {
		return isInfoMessage() && payloadEquals(MSG_ACK);
	}

	/**
	 * helper method to check if the message is a 'hello' message, indicating
	 * that a client wants to log in
	 * 
	 * @return
	 */
	public boolean isHelloMessage() {
		return isJsonMessage() && ((JsonNetworkMessage) this).getJsonObject().has(MSG_HELLO);
	}

	/**
	 * helper method
	 * 
	 * @return <code>true</code> if this.{@link #payload} equals the given
	 *         {@link String}, else <code>false</code>
	 */
	public boolean payloadEquals(String payload) {
		return this.payload.compareTo(payload) == 0;
	}

	/**
	 * helper method
	 * 
	 * @return <code>true</code> if this.{@link #payload} equals the given
	 *         {@link String} (without case sensitivity), else
	 *         <code>false</code>
	 */
	public boolean payloadEqualsNCS(String payload) {
		return this.payload.compareToIgnoreCase(payload) == 0;
	}

	/**
	 * helper method
	 * 
	 * @return <code>true</code> if the given {@link NetworkMessageType} object
	 *         equals this.{@link #type}, else <code>false</code>
	 */
	public boolean isType(NetworkMessageType msgType) {
		return type == msgType;
	}

	/**
	 * helper method
	 * 
	 * @return <code>true</code> if this.{@link #isInfoMessage()} returns
	 *         <code>true</code> and this.{@link #payload} equals
	 *         {@link CommunicationConstants} .COM_BYE, else <code>false</code>
	 */
	public boolean isByeMessage() {
		return isInfoMessage() && payloadEquals(MSG_BYE);
	}

	/**
	 * @return standard {@link ErrorNetworkMessage} to rerequest the last sent
	 *         {@link NetworkMessage}
	 */
	public static ErrorNetworkMessage createReRequestMessage() {
		return new ErrorNetworkMessage(MSG_REREQUEST);
	}

	/**
	 * 
	 * @return an {@link InfoNetworkMessage} that contains the constant
	 *         {@link String} for an acknowledgement
	 */
	public static InfoNetworkMessage createAckMessage() {
		return new InfoNetworkMessage(MSG_ACK);
	}

	/**
	 * method to encrypt the given {@link NetworkMessage} using the AES
	 * algorithm
	 * 
	 * @param message
	 * @param aesKey
	 * @return <code>byte</code>-Array with the encrypted data
	 */
	public static byte[] encryptAes(NetworkMessage message, byte[] aesKey) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			UnsupportedEncodingException {
		return AES.encrypt(message.toString(), aesKey);
	}

	/**
	 * method to encrypt this {@link NetworkMessage} object using the AES
	 * algorithm
	 * 
	 * @param aesKey
	 *            the AES key that will be used to encrypt this message
	 * @return <code>byte</code>-Array with the encrypted data
	 */
	public byte[] encryptAes(byte[] aesKey) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		return encryptAes(this, aesKey);
	}

	/**
	 * method that encrypts a <code>byte</code>-Array using the AES algorithm
	 * and generates a {@link NetworkMessage} object from the decrypted data
	 * 
	 * @param encrypted
	 *            the encrypted data
	 * @param aesKey
	 *            the AES key that will be used to encrypt this message
	 * @return {@link NetworkMessage} object that was parsed from the decrypted
	 *         string
	 */
	public static NetworkMessage decryptAes(byte[] encrypted, byte[] aesKey) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
			UnsupportedEncodingException, EncodingException, NumberFormatException, NullPointerException,
			ServerMessageRegexException, ServerMessageLengthException, ServerMessageChecksumException {
		return NetworkMessage.fromString(new String(AES.decrypt(encrypted, aesKey)));
	}

	/**
	 * generates a checksum for the given string using the CRC32-algorithm
	 * (Cyclic Redundancy Check)
	 */
	public static String generateChecksum(String string) {
		CRC32 crc = new CRC32();
		crc.update(string.getBytes());
		String hex = Long.toHexString(crc.getValue());
		/*
		 * to make sure that the hex string has the correct length, add trailing
		 * zeros if necessary
		 */
		while(hex.length() < CHECK_DIGITS) {
			hex = "0" + hex;
		}
		return hex;
	}

	/**
	 * @return <code>true</code> if the checksum that was generated from the
	 *         given string matches the given checksum
	 */
	public static boolean stringMatchesChecksum(String string, String checksum) {
		return generateChecksum(string).compareTo(checksum) == 0;
	}
}
