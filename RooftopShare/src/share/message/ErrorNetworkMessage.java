package share.message;

/**
 * {@link NetworkMessage} that contains an error message
 * 
 * @author Philipp Hempel
 *
 */
public class ErrorNetworkMessage extends NetworkMessage {

	public ErrorNetworkMessage(String message) {
		super(NetworkMessageType.ERROR, message);
	}

}
