package share.message;

/**
 * {@link NetworkMessage} that contains a warning message
 * 
 * @author Philipp Hempel
 *
 */
public class WarningNetworkMessage extends NetworkMessage {

	public WarningNetworkMessage(String message) {
		super(NetworkMessageType.WARN, message);
	}
}
