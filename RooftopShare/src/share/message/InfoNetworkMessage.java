package share.message;

/**
 * {@link NetworkMessage} that contains an information message
 * 
 * @author Philipp Hempel
 *
 */
public class InfoNetworkMessage extends NetworkMessage {

	public InfoNetworkMessage(String message) {
		super(NetworkMessageType.INFO, message);
	}

}
