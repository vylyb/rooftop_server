package share.message;

import org.json.JSONObject;

/**
 * {@link NetworkMessage} that contains the string representation of a {@link JSONObject}
 * 
 * @author Philipp Hempel
 *
 */
public class JsonNetworkMessage extends NetworkMessage {

	public JsonNetworkMessage(String message) {
		super(NetworkMessageType.JSON, message);
	}

	public JsonNetworkMessage(JSONObject json) {
		super(NetworkMessageType.JSON, json.toString());
	}

	/**
	 * method to generate a {@link JSONObject} from the payload of this message
	 * 
	 * @return {@link JSONObject}, or <code>null</code> if there were errors
	 */
	public JSONObject getJsonObject() {
		try {
			return new JSONObject(getPayload());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
