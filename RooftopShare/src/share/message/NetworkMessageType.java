package share.message;

/**
 * enumeration of types of network messages. can convert the enum type from/to
 * integers
 * 
 * @author Philipp Hempel
 * 
 */
public enum NetworkMessageType {

	/**
	 * Info Message; ID: 1
	 */
	INFO,

	/**
	 * Warning Message; ID: 2
	 */
	WARN,

	/**
	 * Error Message; ID: 3
	 */
	ERROR,

	/**
	 * Message with structured data in json format; ID: 5
	 */
	JSON;

	public static NetworkMessageType fromID(int id) {
		switch (id){

		case 1:
			return INFO;

		case 2:
			return WARN;

		case 3:
			return ERROR;

		case 5:
			return JSON;

		}
		return null;
	}

	public static NetworkMessageType fromString(String string) throws NumberFormatException {
		return fromID(Integer.parseInt(string));
	}

	public static int toID(NetworkMessageType type) {
		switch (type){

		case INFO:
			return 1;

		case WARN:
			return 2;

		case ERROR:
			return 3;

		case JSON:
			return 5;

		}

		return -1;
	}

	public int toID() {
		return toID(this);
	}
}
