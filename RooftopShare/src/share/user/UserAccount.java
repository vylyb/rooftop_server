package share.user;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import share.exceptions.UserManagementException;
import share.message.NetworkMessage;

/**
 * class that represents an user of the app
 * 
 * @author Philipp Hempel
 * 
 */
public class UserAccount {

	/**
	 * constant that indicates the role of this user as an administrator who can
	 * change inputs in the server, can add/delete users and can change the
	 * roles of other users
	 */
	public final static int ROLE_ADMIN = 1;
	/**
	 * constant that indicates the role of this user as a regular guest with the
	 * right to access all controls, but without admin rights
	 */
	public final static int ROLE_REGULAR = 2;
	/**
	 * constant that indicates the role of this user as a guest who has only
	 * access to controls that are explicitly opened for guests
	 */
	public final static int ROLE_GUEST = 3;
	/**
	 * string that is used in the user management activity
	 */
	private static final String[] STRINGS = { "Admin", "Regular Users", "Guest Users" };

	@Override
	public String toString() {
		return "AppUser [username=" + username + ", passwordHash=" + passwordHash + ", role: "
				+ (role == 1 ? "Admin" : (role == 2 ? "Regular" : "Guest")) + "]";
	}

	/**
	 * the login name of the user
	 */
	private String username;
	/**
	 * the hashed password of the user
	 */
	private String passwordHash;
	/**
	 * the role of the user, indicating the rights the user has
	 */
	private int role;
	/**
	 * the unique id of the user in the database
	 */
	private int id;
	/**
	 * list with the input group names where this user has access to
	 */
	private ArrayList<String> accessRights;

	/**
	 * list of input groups where this user has access to, only used in the
	 * server
	 */

	/**
	 * constructor for a new user object, makes sure that the given values are
	 * correct
	 * 
	 * @throws UserManagementException
	 *             if the name or role is not valid (using
	 *             {@link #checkRole(int)}), or if one of the parameters is
	 *             <code>null</code>
	 */
	public UserAccount(int id, String username, String passwordHash, int role) throws UserManagementException {
		/*
		 * check if the role is valid
		 */
		checkRole(role);
		if(username == null)
			throw new UserManagementException("Constructor of a User Object does not accept an User Name that is null!");
		checkUserName(username);

		this.username = username;
		this.passwordHash = passwordHash;
		this.role = role;
		this.id = id;

		accessRights = new ArrayList<String>();
	}

	public String getName() {
		return username;
	}

	public int getId() {
		return id;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public int getRole() {
		return role;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	/**
	 * setter for {@link #role}
	 * 
	 * @param role
	 *            the new role
	 * @throws UserManagementException
	 *             if the given role is not valid
	 */
	public void setRole(int role) throws UserManagementException {
		checkRole(role);
		this.role = role;
	}

	/**
	 * checks the given role number
	 * 
	 * @throws UserManagementException
	 *             if the given role is one of the specified roles
	 */
	public static void checkRole(int role) throws UserManagementException {
		if(!(role == UserAccount.ROLE_ADMIN || role == UserAccount.ROLE_REGULAR || role == UserAccount.ROLE_GUEST))
			throw new UserManagementException("The given role identifier " + role + " is not valid!");
	}

	/**
	 * checks the given user name
	 * 
	 * @throws UserManagementException
	 *             if the username does not exclusivly consists of small or big
	 *             letters of the english alphabet, numbers and underscores
	 */
	public static void checkUserName(String name) throws UserManagementException {
		if(!name.matches("[a-zA-Z0-9_]+"))
			throw new UserManagementException("Username '" + name + "' is not valid!");
	}

	/**
	 * @return a {@link JSONObject} with the content of this {@link UserAccount}
	 *         object
	 */
	public JSONObject toJsonObject() {
		JSONObject json = new JSONObject();

		try {
			json.put(NetworkMessage.JSON_ID, getId());
			json.put(NetworkMessage.JSON_USER_NAME, getName());
			json.put(NetworkMessage.JSON_USER_ROLE, getRole());
			/*
			 * put the names of the input groups where this user has access to
			 * into a json array
			 */
			if(accessRights != null) {
				JSONArray array = new JSONArray();
				for(String group : accessRights) {
					array.put(group);
				}
				json.put(NetworkMessage.JSON_USER_ACCESS_RIGHTS, array);
			}
			return json;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @param json
	 *            the {@link JSONObject} with the data for the
	 *            {@link UserAccount}
	 * @return {@link UserAccount} object with the data from given
	 *         {@link JSONObject}, where the password hash is <code>null</code>
	 * @throws JSONException
	 * @throws UserManagementException
	 */
	public static UserAccount fromJsonObject(JSONObject json) throws UserManagementException, JSONException {
		UserAccount user = new UserAccount(json.getInt(NetworkMessage.JSON_ID),
				json.getString(NetworkMessage.JSON_USER_NAME), null, json.getInt(NetworkMessage.JSON_USER_ROLE));
		/*
		 * add the names of the input groups where this user has access to
		 */
		user.resetAccessRights();
		if(json.has(NetworkMessage.JSON_USER_ACCESS_RIGHTS)) {
			JSONArray array = json.getJSONArray(NetworkMessage.JSON_USER_ACCESS_RIGHTS);
			for(int i = 0; i < array.length(); i++) {
				user.setAccessForInputGroup(array.getString(i));
			}
		}
		return user;
	}

	/**
	 * adds the given input group name to the list with the group names this
	 * user has access to
	 */
	public void setAccessForInputGroup(String group) {
		setAccessForInputGroup(group, true);
	}

	/**
	 * @param allow
	 *            if <code>true</code>, the method adds the given input group
	 *            name to the list with the group names this user has access to.
	 *            if <code>false</code>, the group name is removed
	 */
	public void setAccessForInputGroup(String group, boolean allow) {
		if(allow && !accessRights.contains(group)) {
			accessRights.add(group);
		} else if(!allow && accessRights.contains(group)) {
			accessRights.remove(group);
		}
	}

	/**
	 * creates a new empty list of group names this user has access to
	 */
	private void resetAccessRights() {
		accessRights = new ArrayList<String>();
	}

	/**
	 * getter for {@link #accessRights}
	 * 
	 * @return {@link #accessRights}
	 */
	public ArrayList<String> getAccessRights() {
		return accessRights;
	}

	/**
	 * @param role
	 *            the role number
	 * @return the string representation for the given role number
	 */
	public static String getTextForRoleNumber(int role) {
		if(role < 1 || role > STRINGS.length)
			return null;
		return STRINGS[role - 1];
	}

	public static int getRoleNumberForText(String text) {
		for(int i = 0; i < STRINGS.length; i++) {
			if(text.compareTo(STRINGS[i]) == 0)
				return i + 1;
		}
		return 0;
	}
}
