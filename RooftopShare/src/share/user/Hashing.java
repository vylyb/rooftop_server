package share.user;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.spongycastle.openssl.EncryptionException;

import share.security.AES;
import share.security.EncryptionUtil;

/**
 * class that hashes passwords. based on <a
 * href="https://crackstation.net/hashing-security.htm#javasourcecode"
 * >https://crackstation.net/hashing-security.htm#javasourcecode</a>
 * 
 * @author Philipp Hempel
 */
public final class Hashing {
	public static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";

	public static final int SALT_BYTE_SIZE = 24;
	public static final int HASH_BYTE_SIZE = 24;
	public static final int PBKDF2_ITERATIONS = 1000;

	public static final int ITERATION_INDEX = 0;
	public static final int SALT_INDEX = 1;
	public static final int PBKDF2_INDEX = 2;

	/**
	 * helper method for {@link #createHash(char[])}
	 * 
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws EncryptionException
	 */
	public static String createHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException,
			EncryptionException {
		return createHash(password.toCharArray());
	}

	/**
	 * create a hash from the given password using the given salt in char array
	 * format
	 * 
	 * @param password
	 * @param salt
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws EncryptionException
	 */
	public static String createHash(char[] password, byte[] salt) throws NoSuchAlgorithmException,
			InvalidKeySpecException, EncryptionException {
		/*
		 * hash the password
		 */
		byte[] hash = pbkdf2(password, salt, PBKDF2_ITERATIONS, HASH_BYTE_SIZE);

		return PBKDF2_ITERATIONS + ":" + EncryptionUtil.toHex(salt) + ":" + EncryptionUtil.toHex(hash);
	}

	/**
	 * helper method for {@link #createHash(char[], byte[])}
	 * 
	 * @param password
	 * @param salt
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws EncryptionException
	 */
	public static String createHash(String password, String salt) throws NoSuchAlgorithmException,
			InvalidKeySpecException, EncryptionException {
		return createHash(password.toCharArray(), EncryptionUtil.fromHex(salt));
	}

	/**
	 * create a hash from the given password with a random salt in char array
	 * format
	 * 
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws EncryptionException
	 */
	public static String createHash(char[] password) throws NoSuchAlgorithmException, InvalidKeySpecException,
			EncryptionException {

		return createHash(password, EncryptionUtil.generateSalt(SALT_BYTE_SIZE));
	}

	/**
	 * applies the PBKDF2 algorithm to the password in the given char array
	 * 
	 * @param password
	 * @param salt
	 * @param iterations
	 * @param bytes
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws EncryptionException
	 */
	private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes)
			throws NoSuchAlgorithmException, InvalidKeySpecException, EncryptionException {
		PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
		byte[] result = skf.generateSecret(spec).getEncoded();

		return result;
	}

	/**
	 * helper method for {@link #validatePassword(char[], String)}
	 * 
	 * @param password
	 * @param correctHash
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws EncryptionException
	 */
	public static boolean validatePassword(String password, String correctHash) throws NoSuchAlgorithmException,
			InvalidKeySpecException, EncryptionException {
		return validatePassword(password.toCharArray(), correctHash);

	}

	/**
	 * checks if the given password matches the hash that has been saved
	 * 
	 * @param password
	 * @param correctHash
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws EncryptionException
	 */
	private static boolean validatePassword(char[] password, String correctHash) throws NoSuchAlgorithmException,
			InvalidKeySpecException, EncryptionException {
		/*
		 * Decode the hash into its parameters
		 */
		String[] params = correctHash.split(":");
		int iterations = Integer.parseInt(params[ITERATION_INDEX]);
		byte[] salt = EncryptionUtil.fromHex(params[SALT_INDEX]);
		byte[] hash = EncryptionUtil.fromHex(params[PBKDF2_INDEX]);
		/*
		 * Compute the hash of the provided password, using the same salt,
		 * iteration count, and hash length
		 */

		byte[] testHash = pbkdf2(password, salt, iterations, HASH_BYTE_SIZE);
		/*
		 * Compare the hashes in constant time. The password is correct if both
		 * hashes match.
		 */
		boolean match = AES.byteArraysAreEqual(hash, testHash);

		return match;
	}

	/**
	 * @return random hex string to be used as a salt
	 * @throws EncryptionException
	 */
	public static byte[] generateSalt() {
		return EncryptionUtil.generateSalt(SALT_BYTE_SIZE);
	}

}
