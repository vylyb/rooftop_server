package share.security;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAKeyGenParameterSpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.spongycastle.openssl.EncryptionException;

/**
 * class that implements the rsa (rivest, shamir, adleman) algorithm
 * 
 * @author Philipp Hempel
 *
 */
public class RSA {

	static {
		Security.insertProviderAt(new BouncyCastleProvider(), 1);
	}

	/**
	 * the algorithm to be used
	 */
	protected final static String transformation = "RSA";
	/**
	 * the implementation provider
	 */
	protected static String provider = "BC";
	/**
	 * the size of the generated key in bits
	 */
	protected final static int KEYSIZE = 1024;

	/**
	 * converts the hexadecimal {@link String} into a {@link PublicKey} object
	 * 
	 * @param hex
	 *            the {@link String} with the data of the public key
	 * @return {@link PublicKey} object
	 */
	public static PublicKey publicKeyFromHex(String hex) throws EncryptionException {
		return publicKeyFromByteArray(EncryptionUtil.fromHex(hex));
	}

	/**
	 * converts the given byte array into a {@link PublicKey} object
	 * 
	 * @param bytes
	 *            byte array with the data of the public key
	 * @return {@link PublicKey} object
	 */
	public static PublicKey publicKeyFromByteArray(byte[] bytes) {
		try {
			KeyFactory factory = KeyFactory.getInstance(transformation, provider);
			X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(bytes);
			return factory.generatePublic(x509KeySpec);
		} catch(NoSuchAlgorithmException | InvalidKeySpecException | NoSuchProviderException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * generates a rsa key pair containing the public and the private key
	 * 
	 * @return the generated {@link KeyPair}
	 */
	public static KeyPair generateKeyPair() {
		/*
		 * create and initialize a new rsa key generator
		 */
		try {
			SecureRandom random = new SecureRandom();
			RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(KEYSIZE, RSAKeyGenParameterSpec.F4);
			KeyPairGenerator generator = KeyPairGenerator.getInstance(transformation, provider);
			generator.initialize(spec, random);

			return generator.generateKeyPair();
		} catch(NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * method to encrypt a plain text by using the public key of the
	 * communication partner
	 * 
	 * @param plainText
	 * @param key
	 * @return the encrypted String
	 */
	public static byte[] encrypt(byte[] plainText, PublicKey key) {
		try {
			final Cipher cipher = Cipher.getInstance(transformation, provider);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return cipher.doFinal(plainText);
		} catch(NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | NoSuchProviderException
				| NoSuchPaddingException | InvalidKeyException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * method to decrypt a cipher text with rsa using the own private key
	 * 
	 * @param encryptedText
	 * @param key
	 * @return
	 */
	public static byte[] decrypt(byte[] encryptedText, PrivateKey key) {
		try {
			final Cipher cipher = Cipher.getInstance(transformation, provider);
			cipher.init(Cipher.DECRYPT_MODE, key);
			return cipher.doFinal(encryptedText);
		} catch(InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
