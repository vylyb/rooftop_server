package share.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import share.exceptions.EncodingException;

/**
 * class that implements the advanced encryption standard algorithm (aes)
 * 
 * @author Philipp Hempel
 *
 */
public class AES {

	public final static String encryptionAlgorithm = "AES";
	public final static String stringEncoding = "UTF-8";
	public final static int KEY_BYTES = 16;
	public final static int KEY_BITS = 8 * KEY_BYTES;

	/**
	 * encrypts the given plain text to a byte array
	 * 
	 * @param plainText
	 *            {@link String} with the plain text that will be encrypted
	 * @param key
	 *            byte array that contains the aes key
	 * @return byte array with the data of the encrypted plain text
	 */
	public static byte[] encrypt(String plainText, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		/*
		 * create the key spec
		 */
		SecretKeySpec keySpec = new SecretKeySpec(key, encryptionAlgorithm);

		/*
		 * instantiate a new AES cipher
		 */
		Cipher cipher = Cipher.getInstance(encryptionAlgorithm);
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);

		/*
		 * encrypt the input string
		 */
		byte[] encryptedTextBytes = cipher.doFinal(plainText.getBytes(stringEncoding));

		return encryptedTextBytes;
	}

	/**
	 * decrypts the encypted byte array to a plain text
	 * 
	 * @param encrypted
	 *            byte array with the data of the encrypted plain text
	 * @param key
	 *            byte array that contains the aes key
	 * @return {@link String} with the decrypted plain text
	 */
	public static byte[] decrypt(byte[] encrypted, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException,
			EncodingException {

		/*
		 * create the key spec
		 */
		SecretKeySpec keySpec = new SecretKeySpec(key, encryptionAlgorithm);

		/*
		 * instantiate a new AES cipher
		 */
		Cipher cipher = Cipher.getInstance(encryptionAlgorithm);
		cipher.init(Cipher.DECRYPT_MODE, keySpec);

		return cipher.doFinal(encrypted);
	}

	/**
	 * generates a random aes key
	 * 
	 * @return byte array with the data of the aes key
	 */
	public static byte[] generateRandomKey() {
		byte[] key = new byte[KEY_BYTES];
		SecureRandom random = new SecureRandom();
		random.nextBytes(key);
		return key;
	}

	/**
	 * @return <code>true</code> if the arrays a and b have the same content
	 */
	public static boolean byteArraysAreEqual(byte[] a, byte[] b) {
		/*
		 * if one array is null and the other one is not, they are not equal
		 */
		if((a == null && b != null) || (a != null && b == null)) {
			return false;
		}
		/*
		 * if the arrays have a different length, they are not equal
		 */
		if(a.length != b.length) {
			return false;
		}
		/*
		 * compare each entry of one array with the corresponding byte of the
		 * other array, if one of the pairs is not equal, both arrays are not
		 * equal
		 */
		for(int i = 0; i < a.length; i++) {
			if(a[i] != b[i]) {
				return false;
			}
		}
		/*
		 * if the method didn't return false yet, both arrays are equal
		 */
		return true;
	}

	/**
	 * @param value
	 *            the integer value that will be encoded
	 * @return {@link String} with the hexadezimal representation of the given
	 *         value, containing leading zeros
	 */
	public static String formatHexString(int value) {
		String str = Integer.toHexString(value);
		while(str.length() < 8) {
			str = "0" + str;
		}
		String hex = "";
		for(int i = 0; i < 8; i += 2) {
			hex += str.substring(i, i + 1) + str.substring(i + 1, i + 2) + " ";
		}
		return "0x " + hex;
	}

}
