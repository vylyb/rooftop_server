package share.security;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.spongycastle.openssl.EncryptionException;

import share.encoding.Base64Util;
import share.exceptions.EncodingException;

/**
 * class that provides some helper methods that are used in the encryption
 * process
 * 
 * @author Philipp Hempel
 *
 */
public class EncryptionUtil {

	/**
	 * method that generates some random data and encodes it to base64
	 * 
	 * @return {@link String} with random, base64-encoded data
	 * @throws EncodingException
	 */
	public static String generateRandomBits() throws EncodingException {
		SecureRandom random = new SecureRandom();

		/*
		 * create a byte array of random length
		 */
		byte[] randomBits = new byte[random.nextInt(5) + 5];
		/*
		 * fill it with random data
		 */
		random.nextBytes(randomBits);

		/*
		 * encode the random data to base64
		 */

		return Base64Util.toBase64(randomBits);

	}

	/**
	 * generates a random salt with the given size
	 * 
	 * @param size
	 *            number of bytes that the salt will have
	 * @return byte array with the random salt
	 */
	public static byte[] generateSalt(int size) {
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[size];
		random.nextBytes(salt);
		return salt;
	}

	/**
	 * converts the given byte array into a string in hex format
	 */
	public static String toHex(byte[] array) {
		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
		if(paddingLength > 0)
			return String.format("%0" + paddingLength + "d", 0) + hex;
		else
			return hex;

	}

	/**
	 * converts the given string in hex format to a byte array
	 */
	public static byte[] fromHex(String hex) throws EncryptionException {
		byte[] binary = new byte[hex.length() / 2];
		try {
			for(int i = 0; i < binary.length; i++) {
				binary[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
			}
		} catch(NumberFormatException e) {
			throw new EncryptionException(e.getMessage());
		}
		return binary;
	}

}
