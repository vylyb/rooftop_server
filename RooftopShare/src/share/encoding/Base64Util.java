package share.encoding;

import share.exceptions.Base64DecodingException;
import share.exceptions.Base64EncodingException;
import share.exceptions.EncodingException;

/**
 * class that encodes and decodes strings from/to base 64 format
 * 
 * @author Philipp Hempel
 * 
 */
public class Base64Util {

	public final static String base64chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	private final static int baseMask = base64chars.length() - 1;

	public static String toBase64(String text) throws EncodingException {
		if(text == null)
			throw new Base64EncodingException("Input is null!");

		if(text.length() == 0)
			return "";

		return encodeBase64String(text);
	}

	/**
	 * based on <a href=
	 * "http://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64#Java"
	 * >http://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/
	 * Base64#Java</a>
	 */
	private static String encodeBase64String(String text) {
		String base64string = "", padding = "";
		int counter = text.length() % 3;

		if(counter > 0) {
			for(; counter < 3; counter++) {
				padding += "=";
				text += "\0";
			}
		}

		for(counter = 0; counter < text.length(); counter += 3) {

			int threeByteBlock = (text.charAt(counter) << 16) + (text.charAt(counter + 1) << 8)
					+ (text.charAt(counter + 2));

			int block1 = (threeByteBlock >> 18) & baseMask, block2 = (threeByteBlock >> 12) & baseMask, block3 = (threeByteBlock >> 6)
					& baseMask, block4 = threeByteBlock & baseMask;

			base64string += "" + base64chars.charAt(block1) + base64chars.charAt(block2) + base64chars.charAt(block3)
					+ base64chars.charAt(block4);
		}

		return base64string.substring(0, base64string.length() - padding.length()) + padding;
	}

	/**
	 * based on <a href=
	 * "http://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64#Java_2"
	 * >http://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/
	 * Base64#Java_2</a>
	 */
	private static byte[] decodeBase64(String text) {
		text = text.replaceAll("[^" + base64chars + "=]", "");

		String padding = (text.charAt(text.length() - 1) == '=' ? (text.charAt(text.length() - 2) == '=' ? "AA" : "A")
				: "");
		String result = "";
		text = text.substring(0, text.length() - padding.length()) + padding;

		for(int charcounter = 0; charcounter < text.length(); charcounter += 4) {

			int n = (base64chars.indexOf(text.charAt(charcounter)) << 18)
					+ (base64chars.indexOf(text.charAt(charcounter + 1)) << 12)
					+ (base64chars.indexOf(text.charAt(charcounter + 2)) << 6)
					+ base64chars.indexOf(text.charAt(charcounter + 3));

			result += "" + (char) ((n >>> 16) & 0xFF) + (char) ((n >>> 8) & 0xFF) + (char) (n & 0xFF);
		}

		return result.substring(0, result.length() - padding.length()).getBytes();
	}

	public static String stringFromBase64(String text) throws EncodingException {
		if(text == null)
			throw new Base64DecodingException("Input is null!");

		if(text.length() == 0)
			return "";

		return new String(decodeBase64(text));

	}

	public static byte[] bytesFromBase64(String text) throws EncodingException {
		if(text == null)
			throw new Base64DecodingException("Input is null!");

		if(text.length() == 0)
			return new byte[] {};

		return decodeBase64(text);

	}

	public static String toBase64(byte[] bytes) throws EncodingException {
		return toBase64(new String(bytes));
	}

}
