package share.exceptions;

/**
 * exception that is thrown when a string could not be decoded from base 64
 * format
 * 
 * @author Philipp Hempel
 * 
 */
public class Base64DecodingException extends EncodingException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4018263186205716845L;

	public Base64DecodingException(String message) {
		super(message);
	}

}
