package share.exceptions;

/**
 * exception that is thrown when a string could not be en-/decoded in base 64
 * format
 * 
 * @author Philipp Hempel
 * 
 */
public class EncodingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2556730692459010241L;
	private String message;

	public EncodingException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}
}
