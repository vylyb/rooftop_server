package share.exceptions;

import share.message.NetworkMessage;

/**
 * exception that is thrown when the checksum of a {@link NetworkMessage} was
 * wrong
 * 
 * @author Philipp Hempel
 * 
 */
public class ServerMessageChecksumException extends Exception {

	private String payload;
	private String checksum;

	public ServerMessageChecksumException(String payload, String checksum) {
		this.payload = payload;
		this.checksum = checksum;
	}

	@Override
	public String getMessage() {
		return "Payload: " + payload + ", Given Checksum: " + checksum + ", Generated Checksum: "
				+ NetworkMessage.generateChecksum(payload);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8000748618295747660L;

}
