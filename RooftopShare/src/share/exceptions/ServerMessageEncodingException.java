package share.exceptions;

import share.message.NetworkMessage;

/**
 * exception that is thrown when a {@link NetworkMessage} could not be encoded
 * 
 * @author Philipp Hempel
 * 
 */
public class ServerMessageEncodingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -990844902165543684L;
	private String message;

	public ServerMessageEncodingException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
