package share.exceptions;

import share.message.NetworkMessage;

/**
 * exception that is thrown when a {@link NetworkMessage} did not match the
 * regular expressions that defines a correct {@link NetworkMessage}
 * 
 * @author Philipp Hempel
 * 
 */
public class ServerMessageRegexException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1916935677887242545L;

}
