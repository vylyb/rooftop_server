package share.exceptions;

/**
 * exception that is thrown when a string could not be encoded to base 64 format
 * 
 * @author Philipp Hempel
 * 
 */
public class Base64EncodingException extends EncodingException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5407293938537912830L;

	public Base64EncodingException(String message) {
		super(message);
	}

}
