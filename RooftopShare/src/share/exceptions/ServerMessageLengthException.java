package share.exceptions;

import share.message.NetworkMessage;

/**
 * exception that is thrown when the length of a {@link NetworkMessage} was wrong
 * 
 * @author Philipp Hempel
 * 
 */
public class ServerMessageLengthException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5065806783637264490L;

}
