package share.exceptions;

/**
 * exception that is thrown when a constraint in the user management was broken
 * 
 * @author Philipp Hempel
 * 
 */
public class UserManagementException extends Exception {

	private String message;

	public UserManagementException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1351137558418660910L;

}
