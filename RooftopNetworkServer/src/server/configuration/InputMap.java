package server.configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import server.RooftopNetworkServer;
import server.database.DatabaseUtil;
import share.encoding.Base64Util;
import share.exceptions.EncodingException;
import share.inputs.ImageInput;
import share.inputs.ImageInputArea;
import share.inputs.Input;
import share.inputs.InputFactory;
import share.inputs.InputGroup;
import share.inputs.StandardInput;
import share.message.NetworkMessage;

/**
 * this class is used to load the configuration of the inputs that are used
 * inside the server and the app. inside the server context, an input means a
 * representation of a connection to the rpc server. inside the app context, an
 * input means a graphical input that is used to control a device that is
 * connected to the smart home syste.m<br>
 * the inputs are organised in groups. a group represents an activity in the
 * app, so that one user can only control inputs that belong to the same group
 * at one time. that is to avoid conflicting commands. inside the server, these
 * groups are used to collect the current states of the inputs of an activity,
 * so that the current values are displayed in the app.
 * 
 * @author Philipp Hempel
 * 
 */
public class InputMap {

	/**
	 * hashmpa of all existing input groups, maps the id of the input group
	 * against the {@link InputGroup} object
	 */
	private HashMap<Integer, InputGroup> inputGroups;

	/**
	 * has map of the base64 encoded content of the image files, maps the
	 * original filename against the data
	 */
	private HashMap<String, String> images;

	public InputMap() {
		inputGroups = new HashMap<Integer, InputGroup>();
		images = new HashMap<String, String>();
	}

	/**
	 * checks if the given attribute describes a filename, if so the file will
	 * be saved in {@link #images}
	 * 
	 * @param attribute
	 *            the (possible) descriptor for an image file
	 * @param tmp
	 *            the (possible) filename of an image file
	 */
	private void savePossibleImage(String attribute, String tmp) {
		/*
		 * check if the attribute is a filename
		 */
		if(attributeIsFileName(attribute)) {
			saveImageContent(RooftopNetworkServer.IMAGE_PATH + tmp);
		}
	}

	/**
	 * @return <code>true</code> if the given attribute describes a filename,
	 *         else <code>false</code>
	 */
	private boolean attributeIsFileName(String attribute) {

		return(attribute.compareTo(Input.FILE) == 0 || attribute.compareTo(Input.FILE_2) == 0);
	}

	/**
	 * reads the given optional attribute name from the given {@link ResultSet}
	 * and puts it into the given {@link JSONObject}. if the node does not have
	 * the attribute, this method returns the original given {@link JSONObject}
	 * 
	 * @param attribute
	 *            the name of the attribute
	 * @return the {@link JSONObject} with the added entry
	 */
	private JSONObject addOptionalAttributeToJson(String attribute, ResultSet groups, JSONObject json) {
		/*
		 * check if there is an entry at this table cell
		 */
		try {
			Object entry = groups.getObject(attribute);
			if(entry == null)
				return json;

			/*
			 * check the type of the object
			 */
			if(entry instanceof String) {
				savePossibleImage(attribute, (String) entry);
				json.put(attribute, (String) entry);
			} else if(entry instanceof Double) {
				json.put(attribute, ((Double) entry).doubleValue());
			}
		} catch(SQLException e) {
		} catch(JSONException e) {
		}

		return json;
	}

	/**
	 * reads the given attribute name from the given {@link ResultSet} and puts
	 * it into the given {@link JSONObject}
	 * 
	 * @param attribute
	 *            the name of the attribute
	 * @return the {@link JSONObject} with the added entry
	 * @throws JSONException
	 * @throws SQLException
	 */
	private JSONObject addAttributeToJson(String attribute, ResultSet groups, JSONObject json) throws JSONException,
			SQLException {
		String tmp = groups.getString(attribute);
		savePossibleImage(attribute, tmp);
		json.put(attribute, tmp);
		return json;
	}

	/**
	 * saves the content of the file with the given filename in the
	 * {@link HashMap} {@link #images}
	 */
	private void saveImageContent(String filename) {
		if(images.containsKey(filename))
			return;

		String imageData;
		if((imageData = imageFileToBase64(filename)) != null) {
			images.put(filename, imageData);
		}
	}

	/**
	 * helper method for the log method in {@link RooftopNetworkServer}
	 */
	private void log(String message) {
		RooftopNetworkServer.log(message, getClass().getSimpleName());
	}

	/**
	 * helper method for the error method in {@link RooftopNetworkServer}
	 */
	private void error(String message) {
		RooftopNetworkServer.error(message, getClass().getSimpleName());
	}

	public String imageFileToBase64(String filename) {
		return imageFileToBase64(new File(filename));
	}

	public String imageFileToBase64(File file) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line, data = "";
			while((line = reader.readLine()) != null) {
				data += line;
			}
			reader.close();
			return Base64Util.toBase64(data);
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		} catch(EncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getImageFile(String filename) {
		return images.get(filename);
	}

	public void printImages() {
		for(String key : images.keySet()) {
			log(key + "\t" + images.get(key));
		}
	}

	/**
	 * saves the content of the input map into the database, deletes all entries
	 * first. this should only be used to create a new input map database from a
	 * file, else the tables should only be updated!
	 * 
	 * @return <code>true</code> if saving was successful
	 * @throws SQLException
	 *             if something went wrong
	 */
	public boolean saveToDatabase() throws SQLException {
		/*
		 * start the database connection
		 */
		DatabaseUtil database = new DatabaseUtil(RooftopNetworkServer.DB_URL, RooftopNetworkServer.DB_NAME,
				RooftopNetworkServer.DB_USERNAME, RooftopNetworkServer.DB_PASSWORD);
		/*
		 * drop all existing tables
		 */
		resetAllTables();

		log("Saving " + inputGroups.size() + " Input Groups\n");

		/*
		 * create the ids that are used as primary keys throughout the tables
		 */
		int inputID = 1;
		int areaID = 1;

		for(Integer groupID : inputGroups.keySet()) {
			InputGroup group = inputGroups.get(groupID);

			log("Saving Input Group " + group.getName());

			database.execute("INSERT INTO " + Input.INPUT_GROUP + " VALUES(" + groupID + ",'" + group.getName() + "','"
					+ group.getTransmit() + "'," + DatabaseUtil.formatOptionalString(group.getCategory()) + ")");

			ArrayList<Input> inputs = group.inputsToArray();
			log("Group has " + inputs.size() + " Input(s)");

			for(Input input : inputs) {
				if(input instanceof StandardInput) {

					log("Saving Standard Input " + input.getName());

					database.execute("INSERT INTO " + Input.INPUT + " VALUES(" + inputID + "," + groupID + ",'"
							+ input.getType() + "','" + input.getRpcMethod() + "',"
							+ DatabaseUtil.formatOptionalString(input.getName()) + ","
							+ DatabaseUtil.formatOptionalDouble(input.getMinValue()) + ","
							+ DatabaseUtil.formatOptionalDouble(input.getMaxValue()) + ","
							+ DatabaseUtil.formatOptionalString(input.getStartValue()) + ","
							+ DatabaseUtil.formatOptionalString(((StandardInput) input).getUnit()) + ")");

				} else if(input instanceof ImageInput) {

					log("Saving Image Input " + input.getName());

					database.execute("INSERT INTO " + Input.IMAGE_INPUT + " VALUES(" + inputID + "," + groupID + ",'"
							+ ((ImageInput) input).getFile() + "',"
							+ DatabaseUtil.formatOptionalString(input.getName()) + ")");

					log("Image Input has " + ((ImageInput) input).countAreas() + " Areas");

					for(int i = 0; i < ((ImageInput) input).countAreas(); i++) {

						ImageInputArea area = ((ImageInput) input).getArea(i);
						log("Saving Image Input Area " + area);

						database.execute("INSERT INTO " + Input.AREA + " VALUES(" + areaID + "," + inputID + ",'"
								+ area.getType() + "'," + area.getLeft() + "," + area.getTop() + "," + area.getWidth()
								+ "," + area.getHeight() + "," + DatabaseUtil.formatOptionalString(area.getRpcMethod())
								+ "," + DatabaseUtil.formatOptionalString(area.getOpenGroup()) + ","
								+ DatabaseUtil.formatOptionalString(area.getFile()) + ","
								+ DatabaseUtil.formatOptionalString(area.getTogglefile()) + ","
								+ DatabaseUtil.formatOptionalString(area.getDirection()) + ","
								+ DatabaseUtil.formatOptionalDouble(area.getMinValue()) + ","
								+ DatabaseUtil.formatOptionalDouble(area.getMaxValue()) + ","
								+ DatabaseUtil.formatOptionalString(area.getStartValue()) + ")");

						areaID++;
					}
				}
				inputID++;
			}
			groupID++;
		}

		return true;
	}

	/**
	 * helper method for
	 * {@link #loadFromDatabase(String, String, String, String)}, uses the
	 * preferences the {@link RooftopNetworkServer} got from the command line
	 * arguments
	 * 
	 * @return {@link JSONObject} with the content of the array
	 *         {@link #inputGroups}
	 * @throws SQLException
	 */
	public JSONObject loadFromDatabase() throws SQLException {
		return loadFromDatabase(RooftopNetworkServer.DB_URL, RooftopNetworkServer.DB_NAME,
				RooftopNetworkServer.DB_USERNAME, RooftopNetworkServer.DB_PASSWORD);
	}

	/**
	 * loads the input map from the database
	 * 
	 * @param dburl
	 *            url (ip:port) of the database server
	 * @param dbname
	 *            name of the database
	 * @param dbuser
	 *            user name of the database
	 * @param dbpw
	 *            user password of the database
	 * 
	 * @return {@link JSONObject} with the content of the array
	 *         {@link #inputGroups}
	 * @throws SQLException
	 */
	public JSONObject loadFromDatabase(String dburl, String dbname, String dbuser, String dbpw) throws SQLException {

		inputGroups = new HashMap<Integer, InputGroup>();

		/*
		 * start the database connection
		 */
		DatabaseUtil database = new DatabaseUtil(dburl, dbname, dbuser, dbpw);

		JSONObject root = new JSONObject();

		log("Loading InputMap from Database " + database);

		int groupID = -1;
		int inputID = -1;

		/*
		 * load the input groups from the database
		 */
		ResultSet groups = database.executeQuery("select * from " + Input.INPUT_GROUP);

		while(groups.next()) {

			log("Loading Input Group: " + groups.getString(Input.GROUP_NAME));

			JSONObject jsonGroup = new JSONObject();

			groupID = groups.getInt("id");

			boolean groupValid = true;
			try {
				/*
				 * read the id and add it to the json object
				 */
				jsonGroup.put(NetworkMessage.JSON_ID, groupID);
				/*
				 * read attributes and add them to the json object
				 */
				for(String attr : Input.inputGroupAttributes)
					jsonGroup = addAttributeToJson(attr, groups, jsonGroup);

			} catch(Exception e) {
				e.printStackTrace();
				groupValid = false;
			}
			if(groupValid) {
				/*
				 * read optional attributes
				 */
				for(String attr : Input.inputGroupOptionalAttributes)
					jsonGroup = addOptionalAttributeToJson(attr, groups, jsonGroup);

				/*
				 * load the standard inputs of the current input group from the
				 * database
				 */
				ResultSet inputs = database.executeQuery("select * from " + Input.INPUT + " where parent = " + groupID);

				while(inputs.next()) {

					JSONObject jsonInput = new JSONObject();

					/*
					 * image inputs are displayed exclusively in a view, so only
					 * add inputs if there is already an image input in the
					 * group
					 */
					if(!(jsonGroup.has(Input.IMAGE_INPUT))) {
						try {
							/*
							 * read attributes that must be set
							 */
							for(String attr : Input.inputAttributes)
								jsonInput = addAttributeToJson(attr, inputs, jsonInput);
							/*
							 * read optionale attributes
							 */
							for(String attr : Input.inputOptionalAttributes)
								jsonInput = addOptionalAttributeToJson(attr, inputs, jsonInput);

							/*
							 * add the json object for this input to the json
							 * object for the group
							 */
							jsonGroup.append(Input.INPUT, jsonInput);

						} catch(Exception e) {
							e.printStackTrace();
						}
					}

				} /*
				 * load the image inputs of the current input group from the
				 * database
				 */
				inputs = database.executeQuery("select * from " + Input.IMAGE_INPUT + " where parent = " + groupID);

				while(inputs.next()) {

					inputID = inputs.getInt("id");

					JSONObject jsonInput = new JSONObject();

					/*
					 * image inputs are displayed exclusively in a view, so only
					 * add one image input and nothing else
					 */
					boolean inputImageValid = !(jsonGroup.has(Input.IMAGE_INPUT) || jsonGroup.has(Input.INPUT));

					if(inputImageValid) {
						try {
							/*
							 * read attributes that must be set
							 */
							for(String attr : Input.imageInputAttributes)
								jsonInput = addAttributeToJson(attr, inputs, jsonInput);

							/*
							 * read optionale attributes
							 */
							for(String attr : Input.imageInputOptionalAttributes)
								jsonInput = addOptionalAttributeToJson(attr, inputs, jsonInput);

						} catch(Exception e) {
							e.printStackTrace();
							inputImageValid = false;
						}

						if(inputImageValid) {
							/*
							 * load the image areas of the current image input
							 * from the database
							 */
							ResultSet areas = database.executeQuery("select * from " + Input.AREA + " where parent = "
									+ inputID);

							while(areas.next()) {

								JSONObject jsonArea = new JSONObject();

								try {
									/*
									 * read attributes that must be set
									 */
									for(String attr : Input.inputImageAreaAttributes)
										jsonArea = addAttributeToJson(attr, areas, jsonArea);

									/*
									 * read optional attributes
									 */
									for(String attr : Input.inputImageAreaOptionalAttributes)
										jsonArea = addOptionalAttributeToJson(attr, areas, jsonArea);

									/*
									 * add the area to the input map and the
									 * json object
									 */
									jsonInput.append(Input.AREA, jsonArea);
								} catch(Exception e) {
									e.printStackTrace();
								}
							}

							/*
							 * put the image input to the group object
							 */
							try {
								jsonGroup.put(Input.IMAGE_INPUT, jsonInput);
							} catch(JSONException e) {
								e.printStackTrace();
							}
						}
					}
				}
				/*
				 * add the input group json object to the root json object
				 */
				try {
					root.append(Input.INPUT_GROUP, jsonGroup);
				} catch(JSONException e) {
					e.printStackTrace();
				}
			}
		}

		/*
		 * use the input factory to generate the list of input groups from the
		 * root json object
		 */
		inputGroups = InputFactory.createInputGroupListFromJson(root);

		try {
			log(root.toString(2));
		} catch(JSONException e) {
		}

		return root;
	}

	/**
	 * drops all input map tables in the database, and creates them new
	 * 
	 * @return
	 */
	public boolean resetAllTables() {
		try {
			DatabaseUtil database = new DatabaseUtil(RooftopNetworkServer.DB_URL, RooftopNetworkServer.DB_NAME,
					RooftopNetworkServer.DB_USERNAME, RooftopNetworkServer.DB_PASSWORD);
			/*
			 * drop all input map tables
			 */
			database.execute(DatabaseUtil.COMMAND_DROP + " " + Input.AREA);
			database.execute(DatabaseUtil.COMMAND_DROP + " " + Input.INPUT);
			database.execute(DatabaseUtil.COMMAND_DROP + " " + Input.IMAGE_INPUT);
			database.execute(DatabaseUtil.COMMAND_DROP + " " + Input.INPUT_GROUP);

			/*
			 * create the table with the input groups
			 */
			createNewTable(database, Input.INPUT_GROUP, Input.inputGroupAttributes, Input.inputGroupOptionalAttributes);
			/*
			 * create the table with the standard inputs, reference the input
			 * group table for the foreign key
			 */
			createNewTable(database, Input.INPUT, Input.inputAttributes, Input.inputOptionalAttributes,
					Input.INPUT_GROUP);
			/*
			 * create the table with the image inputs, reference the input group
			 * table for the foreign key
			 */
			createNewTable(database, Input.IMAGE_INPUT, Input.imageInputAttributes, Input.imageInputOptionalAttributes,
					Input.INPUT_GROUP);
			/*
			 * create the table with the image input areas, reference the image
			 * input table for the foreign key
			 */
			createNewTable(database, Input.AREA, Input.inputImageAreaAttributes,
					Input.inputImageAreaOptionalAttributes, Input.IMAGE_INPUT);
			return true;
		} catch(SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * creates a new table with the given name in the given database, creates
	 * the primary key `id` and the foreign key `parent`
	 * 
	 * @param database
	 *            the database
	 * @param tableName
	 *            the name of the new table
	 * @param columns
	 *            column names that can not be null
	 * @param optionalColumns
	 *            column names that can be null
	 * @param foreignTable
	 *            the name of the table which is reference by the foreign key
	 *            `parent`, if this is <code>null</code>, the table uses no
	 *            foreign key
	 * @throws SQLException
	 */
	public void createNewTable(DatabaseUtil database, String tableName, String[] columns, String[] optionalColumns,
			String foreignTable) throws SQLException {
		String query = DatabaseUtil.COMMAND_CREATE + " `" + tableName + "` (" + DatabaseUtil.ID + " "
				+ DatabaseUtil.DEFINE_INT_KEY + ", ";

		/*
		 * set foreign key if parent table is specified
		 */
		if(foreignTable != null) {
			query += DatabaseUtil.PARENT + " " + DatabaseUtil.DEFINE_INT_KEY + ", ";
		}

		/*
		 * create columns that can NOT be null
		 */
		for(String col : columns) {
			query += "`" + col + "` TEXT NOT NULL,";
		}
		/*
		 * create columns that can be null
		 */
		for(String col : optionalColumns) {
			query += "`" + col + "` TEXT,";
		}

		/*
		 * set primary key
		 */
		query += " PRIMARY KEY(" + DatabaseUtil.ID + ")";
		/*
		 * set foreign key if parent table is specified
		 */
		if(foreignTable != null) {
			query += ", FOREIGN KEY(" + DatabaseUtil.PARENT + ") REFERENCES " + foreignTable + "(" + DatabaseUtil.ID
					+ ")";
		}

		query += ")";

		database.execute(query);
	}

	/**
	 * helper method for
	 * {@link #createNewTable(DatabaseUtil, String, String[], String[], String)}
	 */
	public void createNewTable(DatabaseUtil database, String tableName, String[] columns, String[] optionalColumns)
			throws SQLException {
		createNewTable(database, tableName, columns, optionalColumns, null);
	}

	public InputGroup getInputGroupForId(int id) {
		return inputGroups.get(id);
	}
}
