package server.thread;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import server.RooftopNetworkServer;

/**
 * endless background thread that waits for connecting clients and starts a new
 * {@link ClientCommunicationThread} for each new client
 * 
 * @author Philipp Hempel
 *
 */
public class ServerMainThread extends Thread {

	/**
	 * the socket of the server
	 */
	private ServerSocket server;

	public ServerMainThread(ServerSocket server) {
		this.server = server;
	}

	@Override
	public void run() {
		while(true) {
			try {

				log("Server waiting at " + InetAddress.getLocalHost().getHostAddress() + ":" + server.getLocalPort());
				Socket client = server.accept();
				log("Client connected from " + client.getRemoteSocketAddress());

				ClientCommunicationThread thread = new ClientCommunicationThread(client);
				thread.start();

			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void log(String message) {
		RooftopNetworkServer.log(message, getClass().getSimpleName());
	}

}
