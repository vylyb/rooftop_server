package server.thread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import server.RooftopNetworkServer;
import share.exceptions.EncodingException;
import share.exceptions.ServerMessageChecksumException;
import share.exceptions.ServerMessageLengthException;
import share.exceptions.ServerMessageRegexException;
import share.exceptions.UserManagementException;
import share.inputs.Input;
import share.message.ErrorNetworkMessage;
import share.message.InfoNetworkMessage;
import share.message.JsonNetworkMessage;
import share.message.NetworkMessage;
import share.message.WarningNetworkMessage;
import share.security.AES;
import share.security.EncryptionUtil;
import share.security.RSA;
import share.user.Hashing;
import share.user.UserAccount;

/**
 * thread that handles the communication routine with a newly connected client.
 * this contains the exchange of login data, exchange of the aes key for this
 * session using the rsa protocol, and the ongoing exchange of encrypted
 * messages
 * 
 * @author Philipp Hempel
 * 
 */
public class ClientCommunicationThread extends Thread {

	/**
	 * how much bytes of a message will be displayed (to avoiding flooding the
	 * screen)
	 */
	private static final int MESSAGE_SHOW_BYTES = 500;
	/**
	 * the socket of the client
	 */
	private Socket client;
	/**
	 * indicates if the communication is running
	 */
	public boolean running;
	/**
	 * the writer where the messages to the client are written to
	 */
	private PrintWriter outgoing;
	/**
	 * the reader where the messages from the client are read from
	 */
	private BufferedReader incoming;
	/**
	 * indicaters whether the communication is encrypted and the messages have
	 * to be en/decrypted (during the communcation routine), or if they are in
	 * plain text (before an aes-key has been exchanged)
	 */
	private boolean encryptedCommunication;
	/**
	 * the aes key that will be randomly generated and sent to the client after
	 * a successful login. only valid in this session.
	 */
	private byte[] aesKey;
	/**
	 * some random bits that are generated to obscure the hash value of the
	 * password
	 */
	private String randomData;
	/**
	 * the public rsa key of the client; used to encrypt the aes-key
	 */
	private PublicKey userKey;
	/**
	 * the user account of the client that authentified successfully
	 */
	private UserAccount connectedUser;

	/**
	 * constructor for this thread
	 * 
	 * @param client
	 *            the socket of the client
	 * @throws SocketException
	 */
	public ClientCommunicationThread(Socket client) throws SocketException {
		this.client = client;
		/**
		 * set a timeout to the clients socket
		 */
		this.client.setSoTimeout(RooftopNetworkServer.SERVER_TIMEOUT * 1000);
	}

	/**
	 * helper method that prints the given message, together with the IP adress
	 * of the client, to the console and to the window, if it exists
	 */
	private void log(String message) {
		String me = getClass().getSimpleName() + printClientAdress();
		RooftopNetworkServer.log(message, me);
	}

	/**
	 * helper method that prints the given error, together with the IP adress of
	 * the client, to the console and to the window, if it exists
	 */
	private void error(String message) {
		String me = getClass().getSimpleName() + printClientAdress();
		RooftopNetworkServer.error(message, me);
	}

	private String printClientAdress() {
		try {
			return " " + InetAddress.getLocalHost().getHostAddress();
		} catch(UnknownHostException e) {
		}
		return "Unknown Client";

	}

	@Override
	public void run() {
		try {
			outgoing = new PrintWriter(client.getOutputStream());
			incoming = new BufferedReader(new InputStreamReader(client.getInputStream()));
			int requests = 0;

			/*
			 * loop that handles the communication in plain text, contains the
			 * login and exchange of the aes-key
			 */
			running = true;
			encryptedCommunication = false;
			while(running) {

				/*
				 * receive and send data (echange of the aes key). if the aes
				 * key was exchanged successfully, the method receiveData() will
				 * return a Boolean object with the value TRUE. after this, the
				 * encrypted communication begins
				 */
				while(requests < RooftopNetworkServer.MAX_REQUESTS) {
					try {
						if(receiveData() == Boolean.TRUE) {
							running = false;
							encryptedCommunication = true;
							requests = RooftopNetworkServer.MAX_REQUESTS;
						}
					} catch(NumberFormatException | NullPointerException | ServerMessageRegexException
							| ServerMessageLengthException | ServerMessageChecksumException | EncodingException e) {
						/*
						 * the received data could not be converted to a server
						 * message object. rerequest the data
						 */
						reRequestMessage(false);
						requests++;
					}
				}
				/*
				 * if the rerequests of the message exceed the limit, exit this
				 * loop and skip the following encrypted communication loop. the
				 * connection will then be closed
				 */
				if(running && requests == RooftopNetworkServer.MAX_REQUESTS) {
					running = false;
					encryptedCommunication = false;
				}
				requests = 0;
			}

			/*
			 * start the exchange routine with encrypted messages if the login
			 * and the aes key exchange was successful
			 */
			if(encryptedCommunication) {
				log("*** Starting AES-encrypted communication");
			}

			while(encryptedCommunication) {
				requests = 0;
				while(!receiveEncryptedData() && requests < RooftopNetworkServer.MAX_REQUESTS) {
					reRequestMessage(true);
					requests++;
				}
			}

			client.close();
			log("Connection closed");
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	private void reRequestMessage(boolean encrypt) {
		if(encrypt) {
			try {
				sendEncryptedMessage(NetworkMessage.createReRequestMessage());
			} catch(InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		} else {
			sendMessage(NetworkMessage.createReRequestMessage());
		}
	}

	/**
	 * method that reads a line from the clients output stream and calls the
	 * method {@link #receiveMessage(String)} that handles the incoming data
	 * 
	 * @return an object depending on the return value of
	 *         {@link #receiveMessage(String)}. if there were any ecxeptions, it
	 *         returns <code>null</code>
	 * @throws EncodingException
	 * @throws ServerMessageChecksumException
	 * @throws ServerMessageLengthException
	 * @throws ServerMessageRegexException
	 * @throws NullPointerException
	 * @throws NumberFormatException
	 */
	private Object receiveData() throws NumberFormatException, NullPointerException, ServerMessageRegexException,
			ServerMessageLengthException, ServerMessageChecksumException, EncodingException {

		try {

			String message = incoming.readLine();

			if(message != null) {
				return receiveMessage(message);
			}
		} catch(SocketTimeoutException e) {
			timeout();
		} catch(SocketException e) {
			running = false;
		} catch(IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * helper method that is called when a timeout happens
	 */
	private void timeout() {
		try {
			error("Timeout, Client didn't send data for " + (client.getSoTimeout() / 1000) + " seconds");
		} catch(SocketException e1) {
		} finally {
			running = false;
			encryptedCommunication = false;
		}
	}

	/**
	 * method that receives a line of data and decrypts it using the aes key
	 * that has been exchanged with the client
	 * 
	 * @return <code>false</code> if the message could not be received and has
	 *         to be rerequested
	 */
	private boolean receiveEncryptedData() {
		if(!encryptedCommunication)
			return false;

		String message;
		try {
			message = incoming.readLine();
		} catch(IOException e) {
			return false;
		}

		if(message != null) {
			try {
				NetworkMessage decryptedMessage = null;
				byte[] encryptedBytes = EncryptionUtil.fromHex(message);
				decryptedMessage = NetworkMessage.fromString(new String(AES.decrypt(encryptedBytes, aesKey)));
				int bytes = decryptedMessage.getPayload().length();
				log("Received (encrypted) Message: '"
						+ decryptedMessage.getPayload().substring(0, Math.min(bytes, MESSAGE_SHOW_BYTES))
						+ (bytes > MESSAGE_SHOW_BYTES ? "..." : "") + "' (" + bytes + " bytes)");

				if(!communicationRoutine(decryptedMessage))
					try {
						sendEncryptedMessage(NetworkMessage.createAckMessage());
					} catch(Exception e) {
						e.printStackTrace();
					}

			} catch(Exception e) {
				e.printStackTrace();
				return false;
			}

		}

		return true;
	}

	/**
	 * method that encrypts the {@link NetworkMessage} using the AES key that
	 * has been exchanged with the client, and sends it to the client
	 * 
	 * @param message
	 * @throws UnsupportedEncodingException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private void sendEncryptedMessage(NetworkMessage message) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		String payload = message.getPayload();
		int bytes = payload.length();
		log("Sending Encrypted Message: '" + payload.substring(0, Math.min(bytes, MESSAGE_SHOW_BYTES))
				+ (bytes > MESSAGE_SHOW_BYTES ? "..." : "") + "' (" + bytes + " bytes)");

		byte[] encrypted = AES.encrypt(message.toString(), aesKey);
		sendLine(EncryptionUtil.toHex(encrypted));
	}

	/**
	 * helper method that writes the given String to the client's output stream
	 * writer.
	 * 
	 * @param message
	 */
	public void sendLine(String message) {
		try {
			if(client.isConnected() && !client.isClosed()) {
				int bytes = message.length();
				log("Sending Data: '" + message.substring(0, Math.min(bytes, MESSAGE_SHOW_BYTES))
						+ (bytes > MESSAGE_SHOW_BYTES ? "..." : "") + "' (" + bytes + " bytes)");
				outgoing.println(message);
				outgoing.flush();
			} else {
				running = false;
				encryptedCommunication = false;
				return;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * helper method that transforms the given {@link NetworkMessage} into a
	 * string and writes it to the client's output stream writer
	 * 
	 * @param message
	 */
	public void sendMessage(NetworkMessage message) {
		log("Sending Message " + message.printMessage());
		sendLine(message.toString());
	}

	/**
	 * handles the given {@link NetworkMessage}. if a specific answer was sent
	 * to the server, return <code>true</code>. else the server will send a
	 * standard ack-message
	 * 
	 * @param msg
	 *            the (previously decrypted) {@link NetworkMessage} received
	 *            from the client
	 * @return <code>true</code> if a specific message was already send to the
	 *         server
	 */
	private boolean communicationRoutine(NetworkMessage msg) {

		try {
			switch (msg.getType()){

			case INFO:
				/*
				 * client requested the json object with the configuration of
				 * the input map
				 */
				if(msg.payloadEquals(NetworkMessage.MSG_REQUEST_INPUT_CONFIGURATION)) {
					log("Received Request for the input configuration");
					try {
						sendEncryptedMessage(new JsonNetworkMessage(RooftopNetworkServer.configurationObject));
						return true;
					} catch(Exception e) {
						e.printStackTrace();
						return false;
					}
				}

				/*
				 * client requested information about the user account
				 */
				else if(msg.payloadEquals(NetworkMessage.MSG_REQUEST_USER_ACCOUNT_DETAIL)) {
					log("Received Request for User Account Detail");
					try {
						JSONObject json = new JSONObject();
						json.put(NetworkMessage.JSON_USER_ROLE, connectedUser.getRole());
						sendEncryptedMessage(new JsonNetworkMessage(json));
						return true;
					} catch(Exception e) {
						e.printStackTrace();
						return false;
					}
				}

				/*
				 * client requested information about the user account
				 */
				else if(msg.payloadEquals(NetworkMessage.MSG_REQUEST_USER_ACCOUNT_LIST)) {
					log("Received Request for User Account List");
					try {
						JSONArray array = new JSONArray();
						for(UserAccount user : RooftopNetworkServer.userManagement.getUserList()) {
							if(user.getRole() != UserAccount.ROLE_ADMIN) {
								log("Saving user " + user);
								array.put(user.toJsonObject());
							}
						}
						JSONObject json = new JSONObject();
						json.put(NetworkMessage.JSON_ACCOUNT_LIST, array);
						sendEncryptedMessage(new JsonNetworkMessage(json));
						return true;
					} catch(Exception e) {
						e.printStackTrace();
						return false;
					}
				}

				break;

			case ERROR:
				break;

			case WARN:
				break;

			case JSON:
				JSONObject json = ((JsonNetworkMessage) msg).getJsonObject();

				/*
				 * client requests the content of an image file
				 */
				if(json.has(NetworkMessage.MSG_FILE_CONTENT)) {
					String filename = RooftopNetworkServer.IMAGE_PATH + json.getString(NetworkMessage.MSG_FILE_CONTENT);
					log("Client Requested Image File " + filename);

					String imageData = RooftopNetworkServer.inputMap.getImageFile(filename);
					log("Image Data: " + imageData.length() + " bytes");

					if(imageData != null) {
						json = new JSONObject();
						json.put(NetworkMessage.MSG_FILE_CONTENT, imageData);
						sendEncryptedMessage(new JsonNetworkMessage(json));
						return true;
					}
				}

				/*
				 * client calls multiple rpc methods (in a json array)
				 */
				else if(json.has(NetworkMessage.JSON_METHOD_CALLS)) {
					log("Client called RPC Methods");

					JSONArray calls = json.getJSONArray(NetworkMessage.JSON_METHOD_CALLS);
					for(int i = 0; i < calls.length(); i++) {
						JSONObject call = calls.getJSONObject(i);
						if(call.has(NetworkMessage.JSON_CALL_METHOD)) {
							callRpcMethodFromJsonObject(call);
						}
					}
				}

				/*
				 * client calls single rpc method (in a json object)
				 */
				else if(json.has(NetworkMessage.JSON_CALL_METHOD)) {
					log("Client called RPC Method");
					callRpcMethodFromJsonObject(json);
				}

				/*
				 * client requested information about the access rights of a
				 * user account. only send data if the request came from the
				 * admin account
				 */
				else if(json.has(NetworkMessage.MSG_REQUEST_USER_ACCESS_RIGHTS)) {
					if(connectedUser.getRole() == UserAccount.ROLE_ADMIN) {
						String username = json.getString(NetworkMessage.MSG_REQUEST_USER_ACCESS_RIGHTS);
						log("Client Requested Access Rights for User " + username);
						/*
						 * get the input groups where the user has access to and
						 * put the data into a json array
						 */
						json.put(NetworkMessage.JSON_USER_ACCESS_RIGHTS, new JSONArray(
								RooftopNetworkServer.userManagement.getAccessRightsForUsername(username)));
						sendEncryptedMessage(new JsonNetworkMessage(json));
					} else {
						sendEncryptedMessage(new ErrorNetworkMessage("Only the admin can access the user management"));
					}
					return true;
				}

				/*
				 * client sent changed user data. ignore if the message did not
				 * came from the admin account
				 */
				else if(json.has(NetworkMessage.JSON_USER_ACCESS_RIGHTS)) {
					if(connectedUser.getRole() == UserAccount.ROLE_ADMIN) {
						/*
						 * get the user object for this user and change the data
						 */
						try {
							UserAccount user = UserAccount.fromJsonObject(json);
							log("Client sent changed data for User " + user.getName());
							/*
							 * add the original password hash that was saved in
							 * the user management, since the app does know it
							 * and can not send it
							 */
							String pwhash = RooftopNetworkServer.userManagement.getPasswordHashForUser(user.getName());
							user.setPasswordHash(pwhash);
							RooftopNetworkServer.userManagement.changeUser(user);
						} catch(JSONException | UserManagementException e) {
							e.printStackTrace();
							sendEncryptedMessage(new ErrorNetworkMessage("Could not change the user: " + e.getMessage()));
							return true;
						}
						return false;
					} else {
						sendEncryptedMessage(new ErrorNetworkMessage("Only the admin can access the user management"));
						return true;
					}
				}

				/*
				 * client wants to delete the user with the given id. ignore if
				 * the message did not came from the admin account
				 */
				else if(json.has(NetworkMessage.MSG_DELETE_USER)) {
					if(connectedUser.getRole() == UserAccount.ROLE_ADMIN) {
						/*
						 * get the user object for this user and change the data
						 */
						try {
							String username = json.getString(NetworkMessage.MSG_DELETE_USER);
							log("Client wants to delete User width id " + username);
							if(RooftopNetworkServer.userManagement.removeUser(username)) {
								return false;
							}
						} catch(Exception e) {
							e.printStackTrace();
							sendEncryptedMessage(new ErrorNetworkMessage("Could not delete the user: " + e.getMessage()));
							return true;
						}
						sendEncryptedMessage(new ErrorNetworkMessage("Could not delete the user"));
						return true;
					} else {
						sendEncryptedMessage(new ErrorNetworkMessage("Only the admin can access the user management"));
						return true;
					}
				}
				break;

			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * analyzes the given {@link JSONObject} with the rpc method and its
	 * parameters and calls the method in the rpc server
	 * 
	 * @param call
	 *            {@link JSONObject} with rpc method and parameters
	 * @return <code>true</code> if everything went ok, else <code>false</code>
	 */
	private boolean callRpcMethodFromJsonObject(JSONObject call) {
		try {
			String method = call.getString(NetworkMessage.JSON_CALL_METHOD);
			if(call.has(NetworkMessage.JSON_CALL_PARAM)) {
				/*
				 * check if there is one parameter or multiple
				 */
				Object param = call.get(NetworkMessage.JSON_CALL_PARAM);
				if(param instanceof String) {
					/*
					 * one parameter, call the method directly with one
					 * parameter. check the content and format the parameter
					 * correctly
					 */

					log("Calling method " + method + "(" + formatParameter(((String) param)) + ")");
					try {
						if(RooftopNetworkServer.executeBoolResult(method, (String) param)) {
							sendEncryptedMessage(NetworkMessage.createAckMessage());
						} else {
							sendMessage(new WarningNetworkMessage("RPC Server answered with 'false' to the call"));
						}
						return true;
					} catch(Exception e) {
						e.printStackTrace();
						sendEncryptedMessage(new ErrorNetworkMessage(e.getMessage()));
					}
				} else if(param instanceof JSONArray) {
					/*
					 * multiple parameters, format them, put them into a list
					 * and call the method with the list of parameters
					 */
					ArrayList<String> params = new ArrayList<String>();
					for(int i = 0; i < ((JSONArray) param).length(); i++) {
						params.add(formatParameter(((JSONArray) param).getString(i)));
					}
					log("Calling method " + method + params);
					try {
						if(RooftopNetworkServer.executeBoolResult(method, params)) {
							sendEncryptedMessage(NetworkMessage.createAckMessage());
						} else {
							sendEncryptedMessage(new WarningNetworkMessage(
									"RPC Server answered with 'false' to the call"));
						}
						return true;
					} catch(Exception e) {
						e.printStackTrace();
						sendEncryptedMessage(new ErrorNetworkMessage(e.getMessage()));
					}
				}
			} else {
				/*
				 * call has no parameter, just call the method
				 */
				log("Calling method " + method + "()");
				try {
					if(RooftopNetworkServer.executeBoolResult(method)) {
						sendEncryptedMessage(NetworkMessage.createAckMessage());
					} else {
						sendEncryptedMessage(new WarningNetworkMessage("RPC Server answered with 'false' to the call"));
					}
					return true;
				} catch(Exception e) {
					e.printStackTrace();
					sendEncryptedMessage(new ErrorNetworkMessage(e.getMessage()));
				}
			}

		} catch(Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * method that analyzes the content of a parameter and determines whether
	 * it's a {@link Boolean}, a {@link Double} or a {@link String} and makes
	 * sure that it has the correct format for a python method call
	 * 
	 * @param parameter
	 *            the original parameter in a {@link String}
	 * @return the formatted parameter
	 */
	private String formatParameter(String parameter) {
		/*
		 * check if it is a boolean value, if then return it directly
		 */
		if(parameter.compareTo(Input.TRUE) == 0 || parameter.compareTo(Input.FALSE) == 0) {
			return parameter;
		}
		/*
		 * check if it is a double value, if then return it directly
		 */
		if(parameter.matches("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?")) {
			return parameter;
		}
		/*
		 * the parameter is a string, put it in ticks
		 */
		return "\"" + parameter + "\"";
	}

	/**
	 * method that trys to create a {@link NetworkMessage} object from the given
	 * {@link String} and depending on the type and the content of the message
	 * performs further actions
	 * 
	 * @param data
	 * @return an object if an object from the message is needed, else null
	 * @throws EncodingException
	 * @throws ServerMessageChecksumException
	 * @throws ServerMessageLengthException
	 * @throws ServerMessageRegexException
	 * @throws NullPointerException
	 * @throws NumberFormatException
	 */
	public Object receiveMessage(String data) throws NumberFormatException, NullPointerException,
			ServerMessageRegexException, ServerMessageLengthException, ServerMessageChecksumException,
			EncodingException {

		/*
		 * convert the received data to a server message
		 */
		NetworkMessage message = NetworkMessage.fromString(data);

		/*
		 * analyse the received message
		 */
		try {

			log("Received Message: " + message + "\n");

			/*
			 * check if the client wants to start the communication
			 */
			if(message.isHelloMessage()) {
				JSONObject json = new JSONObject();
				/*
				 * if the user is registered, send the salt which was used when
				 * the password hash was created
				 */
				String savedHash = RooftopNetworkServer.userManagement
						.getPasswordHashForUser(((JsonNetworkMessage) message).getJsonObject().getString(
								NetworkMessage.JSON_USER_NAME));

				if(savedHash != null) {
					String[] parts = savedHash.split("\\:");
					if(parts.length == 3) {
						json.put(NetworkMessage.JSON_SALT, parts[Hashing.SALT_INDEX]);
						log("Salt for user "
								+ ((JsonNetworkMessage) message).getJsonObject().getString(
										NetworkMessage.JSON_USER_NAME) + ": " + parts[Hashing.SALT_INDEX]);
					}
				}

				/*
				 * if the user has no account, send some random data that looks
				 * like a salt, to confuse the possible attacker
				 */
				else {
					json.put(NetworkMessage.JSON_SALT, EncryptionUtil.toHex(Hashing.generateSalt()));
					error("User has no account, sending garbage");
				}

				/*
				 * send message with random bits (5-10 bytes) to the client
				 */
				json.put(NetworkMessage.JSON_RANDOM_BITS, randomData = EncryptionUtil.generateRandomBits());
				sendMessage(new JsonNetworkMessage(json));

			}

			else if(message.isByeMessage()) {
				log("Received Bye Message");
				running = false;
				return null;
			}

			else if(message.isAckMessage()) {
				log("Received Ack Message");
				return message.getPayload();
			}

			else {
				/*
				 * else answer the messages from the client depending on the
				 * type of the message and the payload
				 */

				if(message.isJsonMessage()) {
					log("Received a JSON Message");
					/*
					 * get the json object and analyze its content
					 */
					JSONObject json = ((JsonNetworkMessage) message).getJsonObject();

					/*
					 * ***USER LOGIN***
					 * 
					 * check if it is a object containing the values of an user
					 * account, then
					 */
					if(isUserLoginData(json)) {
						try {
							log("Received Login Data");

							/*
							 * get the username and the hashed password from the
							 * json object
							 */
							String userName = json.getString(NetworkMessage.JSON_USER_NAME);

							/*
							 * get the hashed password from the usermanagement
							 * and check if it matches the supplied password
							 * hash
							 */
							String savedHash = RooftopNetworkServer.userManagement.getPasswordHashForUser(userName);
							if(savedHash == null) {
								/*
								 * the supplied login data is wrong (in fact the
								 * user has no account but don't tell that to
								 * the client)
								 */
								error("User not found, user authentification failed");
								sendMessage(new ErrorNetworkMessage(NetworkMessage.MSG_LOGIN_WRONG));
								running = false;
								return null;

							} else {

								/*
								 * check if the submitted password hash,
								 * containing the random bits of the server and
								 * the client, match the saved password hash. if
								 * so, the user is authentificated and is
								 * allowed to communicate with the server.
								 */
								String passwordHash = json.getString(NetworkMessage.JSON_PASSWORD);
								String clientsRandomBits = json.getString(NetworkMessage.JSON_RANDOM_BITS);
								log("Client hash:  " + passwordHash);
								log("Saved hash:   " + savedHash);
								log("Own Bits: " + randomData);
								log("Clients Bits: " + clientsRandomBits);
								String hashData = savedHash + randomData + clientsRandomBits;
								log("Data to hash: " + hashData);

								if(!Hashing.validatePassword(hashData, passwordHash)) {
									/*
									 * the supplied login data is wrong
									 * (password doesn't match)
									 */
									error("Password wrong, user authentification failed");
									sendMessage(new ErrorNetworkMessage(NetworkMessage.MSG_LOGIN_WRONG));
									running = false;
									return null;
								}
								log("User authentification successful");

								/*
								 * the client is authentificated, request the
								 * public key from the client
								 */
								log("Requesting Public Key");
								sendMessage(new InfoNetworkMessage(NetworkMessage.MSG_REQUEST_PUBLIC_KEY));
								userKey = (PublicKey) receiveData();
								log("Received Public Key  (Format: " + userKey.getFormat() + ", Algorithm: "
										+ userKey.getAlgorithm() + ")");
								/*
								 * if the public key could not be extracted from
								 * the message , send an error message and end
								 * the communication with the client
								 */

								if(userKey == null) {
									error("Could not get Public Key");
									sendMessage(new ErrorNetworkMessage(NetworkMessage.MSG_ERROR_RECV_PUBLIC_KEY));
									running = false;
									return null;
								}
								/*
								 * generate a random aes key for this client.
								 * use the public key to encrypt the key and use
								 * a json message send it to the client
								 */

								log("Generating AES Key");
								aesKey = AES.generateRandomKey();
								json = new JSONObject();
								String key = EncryptionUtil.toHex(RSA.encrypt(aesKey, userKey));
								json.put(NetworkMessage.JSON_AES_KEY, key);
								log("Sending AES Key: " + EncryptionUtil.toHex(aesKey) + " (" + (aesKey.length * 8)
										+ " bits)");
								log("Encrypted AES Key: " + key);
								sendMessage(new JsonNetworkMessage(json));

								/*
								 * if the client answers with an acknowledge,
								 * then everything is fine and the encrypted
								 * communication can start.
								 */
								Object answer = receiveData();
								if(answer instanceof String) {
									if(((String) answer).compareTo(NetworkMessage.MSG_ACK) == 0) {
										log("Client acknowledged, starting encrypted communication");
										sendMessage(NetworkMessage.createAckMessage());

										/*
										 * save the user account of the
										 * connected client for later use
										 */
										connectedUser = RooftopNetworkServer.userManagement
												.getAccountForUsername(userName);
										log("User logged in with account " + connectedUser);

										/*
										 * encrypted communication routine can
										 * start!
										 */
										return Boolean.TRUE;
									}
								}
							}
						} catch(Exception e) {
							e.printStackTrace();
						}
					}

					/*
					 * *** PUBLIC KEY DATA ***
					 * 
					 * receive the public key object the client has sent and
					 * return it
					 */
					else if(isPublicKeyData(json)) {
						/*
						 * extract the public key data from the json object
						 */
						try {
							return RSA.publicKeyFromHex(json.getString(NetworkMessage.JSON_PUBLIC_KEY));
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				}

				else {

					/*
					 * to not block the client send a standard answer
					 */
					sendMessage(NetworkMessage.createAckMessage());
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * helper method that checks if the message that was received contains the
	 * public rsa key of the client in json format
	 * 
	 * @param json
	 * @return
	 */
	private boolean isPublicKeyData(JSONObject json) {
		return json.has(NetworkMessage.JSON_PUBLIC_KEY);
	}

	/**
	 * helper method that checks if the message that was received contains the
	 * login data of the client in json format
	 * 
	 * @param json
	 * @return
	 */
	private boolean isUserLoginData(JSONObject json) {
		return json.has(NetworkMessage.JSON_PASSWORD) && json.has(NetworkMessage.JSON_USER_NAME)
				&& json.has(NetworkMessage.JSON_RANDOM_BITS);
	}

}
