package server.user;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.spongycastle.openssl.EncryptionException;

import server.RooftopNetworkServer;
import server.database.DatabaseUtil;
import share.exceptions.UserManagementException;
import share.inputs.Input;
import share.message.NetworkMessage;
import share.user.Hashing;
import share.user.UserAccount;

/**
 * class that manages the user accounts for the apps that have access to the
 * server. user accounts and user rights are loaded from the database
 * 
 * @author Philipp Hempel
 * 
 */
public class UserManagement {

	/**
	 * column name for the id
	 */
	public final static String COL_ID = "id";
	/**
	 * column name for the username
	 */
	public final static String COL_USERNAME = "name";
	/**
	 * column name for the password hash
	 */
	public final static String COL_PASSWORD_HASH = "pwhash";
	/**
	 * column name for the role of the user
	 */
	public final static String COL_ROLE = "role";
	/**
	 * column name for the user id of an access right (foreign key, references
	 * id in table user)
	 */
	public final static String COL_USER_KEY = "user";
	/**
	 * column names for the table with the users
	 */
	private final static String[] userAttributes = { COL_USERNAME, COL_PASSWORD_HASH, COL_ROLE };

	/**
	 * hashmap of users that have been loaded from the file and/or database,
	 * key: username, value: {@link UserAccount} object
	 */
	private HashMap<String, UserAccount> users;
	/**
	 * the helper class that handles the connection to the database
	 */
	private DatabaseUtil database;
	/**
	 * saves the highest id of users, so when a new user is added, it gets the
	 * id {@link #maxID}+1
	 */
	private int maxID = 0;

	/**
	 * deletes all users from the list and sets all saved attributes to
	 * <code>null</code>
	 */
	public void reset() {
		users = new HashMap<String, UserAccount>();
	}

	/**
	 * load the users from the database
	 * 
	 * @param databaseAdress
	 *            url of the database
	 * @throws SQLException
	 */
	public UserManagement(String databaseAdress) throws SQLException, NullPointerException {
		database = new DatabaseUtil(databaseAdress, RooftopNetworkServer.DB_NAME, RooftopNetworkServer.DB_USERNAME,
				RooftopNetworkServer.DB_PASSWORD);

	}

	/**
	 * loads the users from the database
	 * 
	 * @throws SQLException
	 */
	public boolean loadFromDatabase() throws SQLException {

		log("Loading from database '" + RooftopNetworkServer.DB_NAME + "'");

		ResultSet results = database.executeQuery("SELECT * FROM " + RooftopNetworkServer.DB_TABLE_USERS);

		if(users == null)
			users = new HashMap<String, UserAccount>();

		while(results.next()) {
			/*
			 * check if the id of the user in the database is higher than the
			 * current maximum id, if so, update the value
			 */
			int id = results.getInt(COL_ID);
			if(id > maxID) {
				maxID = id;
			}

			try {
				users.put(results.getString(COL_USERNAME),
						new UserAccount(id, results.getString(COL_USERNAME), results.getString(COL_PASSWORD_HASH),
								results.getInt(COL_ROLE)));
			} catch(UserManagementException e) {
				error(e.getMessage());
			}
			log("Added User from Database '" + RooftopNetworkServer.DB_NAME + "' : "
					+ users.get(results.getString(COL_USERNAME)));
		}

		return true;

	}

	/**
	 * counts the users
	 * 
	 * @return number of users in the list
	 */
	public int countUsers() {
		if(users == null)
			return 0;

		return users.size();
	}

	/**
	 * adds the given user to the list and hashes the given password
	 * 
	 * @param user
	 *            the user to add
	 * @param password
	 *            the password of the user in plain text
	 * @return <code>true</code> if the user did not exist yet and was
	 *         successfully added, else <code>false</code>
	 * @throws UserManagementException
	 *             if the creation of the new {@link UserAccount} object threw
	 *             an {@link UserManagementException}, or a second admin would
	 *             be added
	 * @throws SQLException
	 */
	public boolean addUser(String user, String password, int role) throws UserManagementException, SQLException {

		if(users == null)
			users = new HashMap<String, UserAccount>();

		/*
		 * if there is already an admin, dont allow a second one
		 */
		if(getAdmin() != null) {
			if(role == UserAccount.ROLE_ADMIN)
				throw new UserManagementException("There is already an admin account!");
		}

		if(userExists(user))
			return false;

		/*
		 * add the new user and assign an id that is not used yet
		 */
		try {
			UserAccount newUser = new UserAccount(++maxID, user, Hashing.createHash(password), role);
			users.put(user, newUser);
			if(!saveUserInDatabase(newUser))
				return false;
			log("Created new user " + newUser);
			return true;
		} catch(EncryptionException | NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * deletes the given user if it exists
	 * 
	 * @param user
	 *            the name of the user to remove
	 * @return <code>true</code> if successful, else <code>false</code>
	 * @throws UserManagementException
	 *             if the admin account would be removed
	 * @throws SQLException
	 */
	public boolean removeUser(String user) throws UserManagementException, SQLException {
		if(users == null)
			return false;

		/*
		 * make sure that the admin accoun can not be deleted
		 */
		if(user.compareTo(getAdmin().getName()) == 0)
			throw new UserManagementException("The admin account can not be removed!");

		if(!users.containsKey(user))
			return false;

		log("Deleting user " + user);
		users.remove(user);

		/*
		 * delete the user from the database after deleting the access rights
		 * that reference this user
		 */
		if(database.execute("DELETE FROM " + RooftopNetworkServer.DB_TABLE_USER_ACCESS_RIGHTS + " WHERE "
				+ COL_USER_KEY + " IN (SELECT " + COL_ID + " FROM " + RooftopNetworkServer.DB_TABLE_USERS + " WHERE "
				+ COL_USERNAME + "='" + user + "')")) {
			return database.execute("DELETE FROM " + RooftopNetworkServer.DB_TABLE_USERS + " WHERE " + COL_USERNAME
					+ "='" + user + "'");
		}
		return false;
	}

	/**
	 * gets the saved password for the given user
	 * 
	 * @param user
	 *            the name of the user
	 * @return the password for the user if the user exists, else
	 *         <code>null</code>
	 */
	public String getPasswordHashForUser(String user) {
		if(users == null)
			return null;

		for(String key : users.keySet()) {
			if(user.compareToIgnoreCase(users.get(key).getName()) == 0) {
				return users.get(key).getPasswordHash();
			}
		}
		return null;
	}

	/**
	 * checks if the given user exists
	 *
	 * @return <code>true</code> if a password could be retreived for the given
	 *         user, else false
	 */
	public boolean userExists(String user) {
		return getPasswordHashForUser(user) != null;
	}

	/**
	 * tries to change the password of the given user to the given new password
	 * 
	 * @param username
	 * @param password
	 * @return <code>true</code> if successfull
	 * @throws SQLException
	 */
	public boolean changePasswordForUser(String username, String password) throws SQLException {
		if(users == null)
			return false;

		UserAccount user = null;

		for(String key : users.keySet()) {
			UserAccount u = users.get(key);
			if(username.compareToIgnoreCase(u.getName()) == 0) {
				try {
					users.put(key,
							user = new UserAccount(u.getId(), username, Hashing.createHash(password), u.getRole()));
					log("Updated password for user " + username);
					return saveUserInDatabase(user);

				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}

		return false;
	}

	/**
	 * checks if the database url is set and saves the data in the database
	 * 
	 * @param reset
	 *            indicates wheteher the original database shall be wiped first
	 *            (<code>true</code>) or if the values shall just be updated (
	 *            <code>false</code>)
	 * 
	 * @return <code>true</code> if all users have been successfully saved
	 * @throws UserManagementException
	 *             if there is no admin account
	 */
	public boolean saveUsersInDatabase(boolean reset) throws UserManagementException {

		/*
		 * make sure exactly one admin account exists
		 */
		if(getAdmin() == null)
			throw new UserManagementException("No Admin Account!");

		/*
		 * if the parameter says so, drop all user tables and create them new
		 */
		if(reset)
			resetAllTables();

		/*
		 * save the content of the user list in the database
		 */
		for(String key : users.keySet()) {

			UserAccount user = users.get(key);

			try {
				if(reset) {
					/*
					 * insert the user data into the table
					 */
					database.execute("INSERT INTO " + RooftopNetworkServer.DB_TABLE_USERS + " VALUES(" + user.getId()
							+ ",'" + user.getName() + "','" + user.getPasswordHash() + "'," + user.getRole() + ")");
				} else {
					/*
					 * update the user data in the table
					 */
					database.execute("UPDATE " + RooftopNetworkServer.DB_TABLE_USERS + " SET " + COL_USERNAME + "='"
							+ user.getName() + "', " + COL_PASSWORD_HASH + "='" + user.getPasswordHash() + "', "
							+ COL_ROLE + "=" + user.getRole() + " WHERE " + COL_ID + "=" + user.getId());
				}

				/*
				 * remove the existing access right entries for this user and
				 * insert new values
				 */
				database.execute("DELETE FROM " + RooftopNetworkServer.DB_TABLE_USER_ACCESS_RIGHTS + " WHERE "
						+ COL_USER_KEY + "=" + user.getId());

				/*
				 * save the content of the access right list in the database
				 */
				if(user.getAccessRights() != null) {
					for(String group : user.getAccessRights()) {
						try {
							/*
							 * get the group id
							 */
							ResultSet result = database.executeQuery("SELECT id FROM " + Input.INPUT_GROUP + " WHERE "
									+ Input.GROUP_NAME + "='" + group + "'");
							if(!result.next())
								return false;
							int groupId = result.getInt(1);
							/*
							 * insert the user data into the table
							 */
							database.execute("INSERT INTO " + RooftopNetworkServer.DB_TABLE_USER_ACCESS_RIGHTS
									+ " VALUES(0," + user.getId() + "," + groupId + ")");

						} catch(SQLException e) {
							e.printStackTrace();
							return false;
						}

					}
				}

			} catch(SQLException e) {
				e.printStackTrace();
				return false;
			}

		}

		return true;
	}

	/**
	 * convinient method for {@link #saveUsersInDatabase(boolean)} with the
	 * parameter <code>false</code>
	 * 
	 * @return result of {@link #saveUsersInDatabase(boolean)}
	 * @throws UserManagementException
	 *             if {@link #saveUsersInDatabase(boolean)} throws an
	 *             {@link UserManagementException}
	 */
	public boolean saveUsersInDatabase() throws UserManagementException {
		return saveUsersInDatabase(false);
	}

	/**
	 * prints all users that are in the list
	 */
	public void printUsers() {
		if(users == null)
			return;

		for(String key : users.keySet()) {
			log(users.get(key).toString());
		}
	}

	/**
	 * drops all user tables in the database, and creates them new
	 * 
	 * @return
	 */
	private boolean resetAllTables() {
		try {
			/*
			 * drop all input map tables
			 */
			database.execute(DatabaseUtil.COMMAND_DROP + " " + RooftopNetworkServer.DB_TABLE_USER_ACCESS_RIGHTS);
			database.execute(DatabaseUtil.COMMAND_DROP + " " + RooftopNetworkServer.DB_TABLE_USERS);

			/*
			 * create the table with the users
			 */
			createNewTable(database, RooftopNetworkServer.DB_TABLE_USERS, userAttributes);

			/*
			 * create the table with the user rights, reference the user table
			 * for the foreign key
			 */
			String query = DatabaseUtil.COMMAND_CREATE + " `" + RooftopNetworkServer.DB_TABLE_USER_ACCESS_RIGHTS
					+ "` (" + DatabaseUtil.ID + " " + DatabaseUtil.DEFINE_INT_KEY + " AUTO_INCREMENT, ";

			query += RooftopNetworkServer.DB_TABLE_USERS + " " + DatabaseUtil.DEFINE_INT_KEY + " NOT NULL,";
			query += Input.INPUT_GROUP + " " + DatabaseUtil.DEFINE_INT_KEY + " NOT NULL,";

			/*
			 * set primary key
			 */
			query += " PRIMARY KEY(" + DatabaseUtil.ID + "), ";
			/*
			 * set foreign keys
			 */
			query += "FOREIGN KEY(" + RooftopNetworkServer.DB_TABLE_USERS + ") REFERENCES "
					+ RooftopNetworkServer.DB_TABLE_USERS + "(" + DatabaseUtil.ID + "), ";
			query += "FOREIGN KEY(" + Input.INPUT_GROUP + ") REFERENCES " + Input.INPUT_GROUP + "(" + DatabaseUtil.ID
					+ "))";

			database.execute(query);

			return true;
		} catch(SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * creates a new table with the given name in the given database, creates
	 * the primary key `id` and the foreign key `parent`
	 * 
	 * @param database
	 *            the database
	 * @param tableName
	 *            the name of the new table
	 * @param columns
	 *            column names that can not be null
	 * @param optionalColumns
	 *            column names that can be null
	 * @param foreignTable
	 *            the name of the table which is reference by the foreign key
	 *            `parent`, if this is <code>null</code>, the table uses no
	 *            foreign key
	 * @throws SQLException
	 */
	public void createNewTable(DatabaseUtil database, String tableName, String[] columns, String foreignTable)
			throws SQLException {
		String query = DatabaseUtil.COMMAND_CREATE + " `" + tableName + "` (" + DatabaseUtil.ID + " "
				+ DatabaseUtil.DEFINE_INT_KEY + ", ";

		/*
		 * set foreign key if parent table is specified
		 */
		if(foreignTable != null) {
			query += DatabaseUtil.PARENT + " " + DatabaseUtil.DEFINE_INT_KEY + ", ";
		}
		/*
		 * create columns that can NOT be null
		 */
		for(String col : columns) {
			query += "`" + col + "` TEXT NOT NULL,";
		}

		/*
		 * set primary key
		 */
		query += " PRIMARY KEY(" + DatabaseUtil.ID + ")";
		/*
		 * set foreign key if parent table is specified
		 */
		if(foreignTable != null) {
			query += ", FOREIGN KEY(" + DatabaseUtil.PARENT + ") REFERENCES " + foreignTable + "(" + DatabaseUtil.ID
					+ ")";
		}

		query += ")";

		database.execute(query);
	}

	/**
	 * helper method for
	 * {@link #createNewTable(DatabaseUtil, String, String[], String[], String)}
	 */
	public void createNewTable(DatabaseUtil database, String tableName, String[] columns) throws SQLException {
		createNewTable(database, tableName, columns, null);
	}

	private void log(String message) {
		RooftopNetworkServer.log(message, getClass().getSimpleName());
	}

	private void error(String message) {
		RooftopNetworkServer.error(message, getClass().getSimpleName());
	}

	/**
	 * @return an {@link ArrayList} with all {@link UserAccount} object entries
	 *         from the {@link HashMap} {@link #users}
	 */
	public ArrayList<UserAccount> getUserList() {
		ArrayList<UserAccount> list = new ArrayList<UserAccount>();

		for(String key : users.keySet()) {
			list.add(users.get(key));
		}

		return list;
	}

	/**
	 * @return (possibly empty) {@link ArrayList} of {@link UserAccount} objects
	 *         that have the same role as the given role, or <code>null</code>
	 *         if the given role was none of the specified role constants
	 */
	public ArrayList<UserAccount> getUsersForRole(int role) {
		/*
		 * check if the given role is valid
		 */
		try {
			UserAccount.checkRole(role);
		} catch(UserManagementException e) {
			return null;
		}

		/*
		 * add all users with the given role to the list
		 */
		ArrayList<UserAccount> list = new ArrayList<UserAccount>();
		for(String key : users.keySet()) {
			if(users.get(key).getRole() == role)
				list.add(users.get(key));
		}

		return list;
	}

	/**
	 * @param username
	 *            the name of the user
	 * @return {@link UserAccount} object for the given user name if it exists,
	 *         else <code>null</code>
	 */
	public UserAccount getAccountForUsername(String username) {
		return users.get(username);
	}

	/**
	 * @return the {@link UserAccount} with the role 'admin' if there is
	 *         <b>exactly one</b> user with this role, or <code>null</code> if
	 *         there is no admin
	 * @throws UserManagementException
	 *             if there is more than one admin account
	 */
	public UserAccount getAdmin() throws UserManagementException {
		ArrayList<UserAccount> list = getUsersForRole(UserAccount.ROLE_ADMIN);

		/*
		 * if there is no admin, return null
		 */
		if(list.isEmpty())
			return null;

		/*
		 * if there is exactly one admin, return it
		 */
		if(list.size() == 1)
			return list.get(0);

		throw new UserManagementException("There must be exactly ONE Admin Account, but it was " + list.size());
	}

	/**
	 * checks if there is <b>one</b> admin acount
	 * 
	 * @return <code>true</code> if there is one admin, <code>false</code> if
	 *         there is no admin
	 * @throws UserManagementException
	 *             if {@link #getAdmin()} throws an
	 *             {@link UserManagementException}
	 */
	public boolean checkAdmin() throws UserManagementException {
		return getAdmin() != null;
	}

	/**
	 * method that changes the name of the {@link UserAccount} with the old name
	 * to the new name
	 * 
	 * @param oldName
	 *            the name of a user
	 * @param newName
	 *            the name of the same user after this method returned
	 *            succesfully
	 * @return {@link UserAccount} object with the new name if such a user
	 *         existed and was successfully changed, else <code>null</code>
	 * @throws UserManagementException
	 *             if the new user name is not valid
	 * @throws SQLException
	 */
	public UserAccount changeUserName(String oldName, String newName) throws UserManagementException, SQLException {
		/*
		 * check if the new name is valied
		 */
		UserAccount.checkUserName(newName);
		/*
		 * check that the user with the old name exists
		 */
		if(!userExists(oldName))
			return null;
		/*
		 * check that no other user with the new name already exists
		 */
		if(userExists(newName))
			throw new UserManagementException("The user name can not be changed to '" + newName + "' (already exists)");

		/*
		 * create a new user and put it into the map
		 */
		UserAccount newUser = users.get(oldName);
		newUser.setUsername(newName);
		users.put(newName, newUser);
		/*
		 * remove the old user
		 */
		users.remove(oldName);

		/*
		 * save the changed user entry in the database
		 */
		if(saveUserInDatabase(newUser))
			return newUser;

		return null;
	}

	/**
	 * replaces the entry with the name of the given {@link UserAccount} with
	 * the given {@link UserAccount} object
	 * 
	 * @param newUser
	 *            the new user
	 * @return <code>true</code> if an entry with the name of the given
	 *         {@link UserAccount} existed and was replaced, else
	 *         <code>false</code>. the method checks if the new user has the
	 *         same username as the admin, and if this is the case, the new user
	 *         will only be added if its role is also admin.
	 * @throws UserManagementException
	 * @throws SQLException
	 */
	public boolean changeUser(UserAccount newUser) throws UserManagementException, SQLException {

		/*
		 * check if a user with the same name as the new user exists
		 */
		if(!userExists(newUser.getName()))
			return false;

		/*
		 * check if the new user has the same name as the admin
		 */
		if(getAdmin().getName().compareTo(newUser.getName()) == 0) {
			/*
			 * check if the role is also admin, else throw an exception
			 */
			if(newUser.getRole() != UserAccount.ROLE_ADMIN) {
				throw new UserManagementException("The admin can not be changed to an account with another role!");
			}
		}

		users.put(newUser.getName(), newUser);

		return saveUserInDatabase(newUser);

	}

	/**
	 * updates all entries for the given user in the database
	 * 
	 * @param user
	 *            the {@link UserAccount} that will be updated in the database
	 * @return <code>true</code> if the saving in the database was successful
	 * @throws SQLException
	 */
	private boolean saveUserInDatabase(UserAccount user) throws SQLException {
		if(user == null)
			return false;

		int id = user.getId();

		/*
		 * delete all entries in all tables that reference this user
		 */
		database.execute("DELETE FROM " + RooftopNetworkServer.DB_TABLE_USER_ACCESS_RIGHTS + " WHERE " + COL_USER_KEY
				+ "=" + id);
		/*
		 * delete the user from the user table
		 */
		database.execute("DELETE FROM " + RooftopNetworkServer.DB_TABLE_USERS + " WHERE " + COL_ID + "=" + id);
		/*
		 * insert the data into the tables
		 */
		database.execute("INSERT INTO " + RooftopNetworkServer.DB_TABLE_USERS + " VALUES(" + id + ",'" + user.getName()
				+ "','" + user.getPasswordHash() + "'," + user.getRole() + ")");
		if(!database.executeQuery(
				"SELECT * FROM " + RooftopNetworkServer.DB_TABLE_USERS + " WHERE " + COL_ID + "=" + id).next())
			return false;

		ResultSet result = null;
		for(String group : user.getAccessRights()) {
			result = database.executeQuery("SELECT " + COL_ID + " FROM " + RooftopNetworkServer.DB_TABLE_GROUPS
					+ " WHERE " + Input.GROUP_NAME + "='" + group + "'");
			if(result.next()) {
				database.execute("INSERT INTO " + RooftopNetworkServer.DB_TABLE_USER_ACCESS_RIGHTS + " VALUES(0,"
						+ user.getId() + "," + result.getInt(COL_ID) + ")");
			} else
				return false;
		}

		return true;
	}

	/**
	 * changes the role of the {@link UserAccount} with the given name
	 * 
	 * @param username
	 *            the name of the user
	 * @param role
	 *            the new role
	 * @return the changed {@link UserAccount} object if the setting of the new
	 *         role was successfull, else <code>null</code>
	 * @throws UserManagementException
	 *             if the role was not valid
	 * @throws SQLException
	 */
	public UserAccount changeUserRole(String username, int role) throws UserManagementException, SQLException {
		/*
		 * check that the user with the old name exists
		 */
		if(!userExists(username))
			return null;

		/*
		 * change the role of the user
		 */
		users.get(username).setRole(role);

		if(saveUserInDatabase(users.get(username)))
			return users.get(username);

		return null;
	}

	/**
	 * @return list of the names of all input groups where the given user has
	 *         access to
	 * @throws UserManagementException
	 * @throws SQLException
	 */
	public ArrayList<String> getAccessRightsForUsername(String username) throws UserManagementException, SQLException {
		if(!userExists(username))
			throw new UserManagementException("User '" + username + "' doesn't exist");

		/*
		 * get the names of the input groups from the database where the given
		 * user has access
		 */
		ResultSet results = database.executeQuery("SELECT " + Input.INPUT_GROUP + "." + Input.GROUP_NAME + " FROM "
				+ RooftopNetworkServer.DB_TABLE_USER_ACCESS_RIGHTS + "," + RooftopNetworkServer.DB_TABLE_USERS + ","
				+ Input.INPUT_GROUP + " WHERE " + RooftopNetworkServer.DB_TABLE_USERS + "." + Input.NAME + " = '"
				+ username + "' AND " + RooftopNetworkServer.DB_TABLE_USER_ACCESS_RIGHTS + "."
				+ RooftopNetworkServer.DB_TABLE_USERS + " = " + RooftopNetworkServer.DB_TABLE_USERS + ".id AND "
				+ RooftopNetworkServer.DB_TABLE_USER_ACCESS_RIGHTS + "." + Input.INPUT_GROUP + " = "
				+ Input.INPUT_GROUP + ".id");

		ArrayList<String> access = new ArrayList<String>();

		while(results.next()) {
			access.add(results.getString(Input.GROUP_NAME));
		}

		return access;
	}

	/**
	 * @return the {@link UserAccount} with the given id, or <code>null</code>
	 *         if no such user exists
	 */
	public UserAccount getAccountForUserId(int id) {
		for(String key : users.keySet()) {
			if(users.get(key).getId() == id)
				return users.get(key);
		}
		return null;
	}

}
