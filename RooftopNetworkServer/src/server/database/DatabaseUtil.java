package server.database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import server.RooftopNetworkServer;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

/**
 * helper class that handles the communication with a database
 * 
 * @author Philipp Hempel
 *
 */
public class DatabaseUtil {

	/**
	 * column name for the primary key
	 */
	public static final String ID = "`id`";

	/**
	 * column name for the foreign key
	 */
	public static final String PARENT = "`parent`";

	/**
	 * definition of integer values, mostly used for keys
	 */
	public static final String DEFINE_INT_KEY = "int NOT NULL";
	/**
	 * command that creates a table
	 */
	public static final String COMMAND_CREATE = "CREATE TABLE IF NOT EXISTS";
	/**
	 * command that drops a table
	 */
	public static final String COMMAND_DROP = "DROP TABLE IF EXISTS";
	/**
	 * the name of the database that is currently used
	 */
	private String databaseName;
	/**
	 * the {@link Connection} object that handles the communication with the
	 * mysql server over the network
	 */
	private Connection connection;
	/**
	 * the server address of the mysql server, in the form [ip]:[port]
	 */
	private String databaseAddress;

	/**
	 * cosntructor for the access to the database
	 * 
	 * @param databaseAdress
	 *            adress of the database in the form [ip]:[port]
	 * @param databaseName
	 *            the name of the database
	 * @param userName
	 *            the name of the database user
	 * @param password
	 *            the password of the database user
	 * @throws SQLException
	 *             if the connection could not be established
	 */
	public DatabaseUtil(String databaseAdress, String databaseName, String userName, String password)
			throws SQLException {
		this.databaseName = databaseName;
		this.databaseAddress = databaseAdress;
		this.connection = getConnection(databaseAdress, userName, password);
		execute("USE " + databaseName);
	}

	@Override
	public String toString() {
		return databaseName + " at " + databaseAddress;
	}

	public String getDatabaseAddress() {
		return databaseAddress;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	/**
	 * creates a database connection
	 * 
	 * @param adress
	 *            the adress of the database as a {@link String} in the form
	 *            'host:port'
	 * @param user
	 *            name of the database
	 * @param password
	 *            password of the database user
	 * @return {@link Connection} object if connection was successfull
	 * @throws SQLException
	 */
	public Connection getConnection(String adress, String user, String password) throws SQLException {

		Properties connectionProps = new Properties();
		connectionProps.put("user", user);
		connectionProps.put("password", password);

		return (Connection) DriverManager.getConnection("jdbc:mysql://" + adress + "/", connectionProps);
	}

	/**
	 * executes a query with the given query string that returns a
	 * {@link ResultSet}, using the exisiting connection
	 */
	public ResultSet executeQuery(String query) throws SQLException {
		Statement statement = (Statement) connection.createStatement();

		log("Executing query '" + query + "'");
		return statement.executeQuery(query);
	}

	/**
	 * executes a query with the given query string that returns a boolean if
	 * the query was executed succesfully, using the exisiting connection
	 */
	public boolean execute(String query) throws SQLException {
		Statement statement = (Statement) connection.createStatement();

		log("Executing query '" + query + "'");
		return statement.execute(query);
	}

	/**
	 * method that loads the content of the script at the given filename and
	 * executes the sql statements in it
	 * 
	 * @throws SQLException
	 */
	public boolean executeSqlScript(String filename) throws SQLException {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));

			String line = "";
			String scriptContent = "";

			while((line = reader.readLine()) != null) {
				/*
				 * check if the line is not in a single line comment
				 */
				if(!line.startsWith("--")) {
					/*
					 * check if the line is not empty
					 */
					if(line.length() > 0) {
						/*
						 * add the line without linebreaks to the content
						 */
						scriptContent += line;
					}
				}

			}
			reader.close();

			/*
			 * delete all multi line comments from the content
			 */
			scriptContent = scriptContent.replaceAll("(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)", "  ");
			/*
			 * delete all white spaces before and after semicolons
			 */
			scriptContent = scriptContent.replaceAll("(\\s)*;(\\s)*", ";");
			/*
			 * delete all multiple semicolons
			 */
			scriptContent = scriptContent.replaceAll("(;)+", ";");
			/*
			 * split the script content along semicolons into single commands
			 */
			String commands[] = scriptContent.split(";");

			/*
			 * select the database
			 */
			execute("USE " + databaseName);
			/*
			 * execute the single commands
			 */
			for(String command : commands) {
				execute(command);
			}

			return true;
		} catch(IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * if the given double value is <code>NaN</code>, it returns the string
	 * "null", else it returns the value as a string
	 */
	public static String formatOptionalDouble(double value) {
		return(Double.isNaN(value) ? "null" : "" + value);
	}

	/**
	 * if the given string is <code>null</code>, it returns the string "null",
	 * else it puts the string into ticks (')
	 */
	public static String formatOptionalString(String string) {
		return(string != null ? "'" + string + "'" : "null");
	}

	private void log(String message) {
		RooftopNetworkServer.log(message, getClass().getSimpleName());
	}

	// private void error(String message) {
	// RooftopNetworkServer.error(message, getClass().getSimpleName());
	// }

}
