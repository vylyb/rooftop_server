package server.rpc;

import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import server.RooftopNetworkServer;

/**
 * handles the communication with the rpc server
 * 
 * @author Philipp Hempel
 *
 */
public class RpcClient extends XmlRpcClient {

	public RpcClient(String url) throws MalformedURLException, XmlRpcException, ConnectException {
		/*
		 * set the client configuration
		 */
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		config.setServerURL(new URL(url));
		setConfig(config);

		log("Connected to RPC Server at " + config.getServerURL());
	}

	/**
	 * convinient method for {@link #executeBoolResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @throws XmlRpcException
	 */
	public boolean executeBoolResult(String method) throws XmlRpcException {
		return executeBoolResult(method, new ArrayList<String>());
	}

	/**
	 * convinient method for {@link #executeBoolResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @param param
	 *            the single parameter
	 * @throws XmlRpcException
	 */
	public boolean executeBoolResult(String method, String param) throws XmlRpcException {
		ArrayList<String> params = new ArrayList<String>();
		params.add(param);
		return executeBoolResult(method, params);

	}

	/**
	 * executes the given method with the given parameters in the rpc server
	 * 
	 * @return the boolean value of the result of the call
	 * @param method
	 *            the method name
	 * @param params
	 *            the {@link List} of parameters
	 */
	public boolean executeBoolResult(String method, List<String> params) throws XmlRpcException {
		printMethodCall(method, params);
		try {
			return ((Boolean) execute(method, params)).booleanValue();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * convinient method for {@link #executeStringResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @throws XmlRpcException
	 */
	public String executeStringResult(String method) throws XmlRpcException {
		return executeStringResult(method, new ArrayList<String>());
	}

	/**
	 * convinient method for {@link #executeStringResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @param param
	 *            the single parameter
	 * @throws XmlRpcException
	 */
	public String executeStringResult(String method, String param) throws XmlRpcException {
		ArrayList<String> params = new ArrayList<String>();
		params.add(param);
		try {
			return executeStringResult(method, params);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * executes the given method with the given parameters in the rpc server
	 * 
	 * @return the {@link String} value of the result of the call
	 * @param method
	 *            the method name
	 * @param params
	 *            the {@link List} of parameters
	 */
	public String executeStringResult(String method, List<String> params) throws XmlRpcException {
		printMethodCall(method, params);
		return (String) execute(method, params);
	}

	/**
	 * convinient method for {@link #executeStringResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @throws XmlRpcException
	 */
	public double executeNumberResult(String method) throws XmlRpcException {
		return executeNumberResult(method, new ArrayList<String>());
	}

	/**
	 * convinient method for {@link #executeNumberResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @param param
	 *            the single parameter
	 * @throws XmlRpcException
	 */
	public double executeNumberResult(String method, String param) throws XmlRpcException {
		ArrayList<String> params = new ArrayList<String>();
		params.add(param);
		return executeNumberResult(method, params);

	}

	/**
	 * executes the given method with the given parameters in the rpc server
	 * 
	 * @return the double value of the result of the call
	 * 
	 * @param method
	 *            the method name
	 * @param params
	 *            the {@link List} of parameters
	 */
	public double executeNumberResult(String method, List<String> params) throws XmlRpcException {
		printMethodCall(method, params);
		try {
			Object result = execute(method, params);

			if(result instanceof Double)
				return ((Double) result).doubleValue();

			if(result instanceof Integer)
				return ((Integer) result).doubleValue();

		} catch(Exception e) {
			e.printStackTrace();
		}
		return Double.NaN;
	}

	private void printMethodCall(String method, List<String> params) {
		if(params.size() < 1) {
			log("Calling RPC Method " + method + "()");
		} else {
			log("Calling RPC Method " + method + params);
		}
	}

	private void log(String message) {
		RooftopNetworkServer.log(message, getClass().getSimpleName());
	}

}
