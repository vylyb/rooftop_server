package server.rpc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import share.inputs.ImageInput;
import share.inputs.ImageInputArea;
import share.inputs.Input;
import share.inputs.InputGroup;
import share.inputs.StandardInput;

/**
 * factory class that uses the input map to generate a Python script with the
 * correct method declarations
 * 
 * @author Philipp Hempel
 *
 */
public class RpcHandlerScriptGenerator {

	public static boolean generateRpcHandler(HashMap<Integer, InputGroup> hashMap, String className,
			File file) {
		if(file == null)
			return false;

		/*
		 * check if the file name has the correct ending for a python script
		 */
		if(!file.getName().toLowerCase().endsWith(".py"))
			return false;

		/*
		 * if the file does not exist, create a new file
		 */
		if(!file.exists()) {
			try {
				if(!file.createNewFile())
					return false;
			} catch(IOException e) {
				e.printStackTrace();
				return false;
			}
		}

		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));

			pythonComment(writer, "Generated Request Handler");
			pythonComment(writer, "Time: " + (new Date()));
			emptyLine(writer);
			pythonLine(writer, "class " + className + ":");

			emptyLine(writer);
			pythonComment(
					writer,
					"function that returns true if called, to check if the connection is working and the rpc server answers method calls",
					1);
			pythonLine(writer, "def checkConnection(self):", 1);
			pythonLine(writer, "print \"Call for connection check, returning True\"", 2);
			pythonLine(writer, "return True", 2);

			for(Integer id : hashMap.keySet()) {
				InputGroup group = hashMap.get(id);
				if(!generatePythonScriptForInputGroup(group, writer))
					return false;
				emptyLine(writer);
			}

			writer.close();

		} catch(IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * generates a Python script that defines a request handler for the rpc
	 * server with the method declarations of all inputs from the given input
	 * group
	 * 
	 * @param group
	 *            {@link InputGroup} for which the request handler will be
	 *            generate
	 * @return <code>true</code> if all went ok
	 */
	public static boolean generatePythonScriptForInputGroup(InputGroup group, BufferedWriter writer) {

		/*
		 * generate and save the content for the python script
		 */
		try {

			emptyLine(writer);
			String headline = "Methods for Input Group '" + group.getName() + "'";
			String border = "";
			for(int i = 0; i < headline.length(); i++) {
				border += "#";
			}
			pythonComment(writer, border + " #", 1);
			pythonComment(writer, headline + " #", 1);
			pythonComment(writer, border + " #", 1);

			for(Input input : group.inputsToArray()) {
				if(input instanceof StandardInput) {
					/*
					 * write the method for the standard input
					 */
					writeMethodForInput(writer, input.getName(), input.getClass().getSimpleName(), input.getType(),
							input.getRpcMethod());
				} else if(input instanceof ImageInput) {
					/*
					 * write a method for each image input area
					 */
					for(int i = 0; i < ((ImageInput) input).countAreas(); i++) {
						ImageInputArea area = ((ImageInput) input).getArea(i);
						writeMethodForInput(writer, "Area" + (i + 1), area.getClass().getSimpleName(), Input.AREA,
								area.getRpcMethod());
					}
				}
			}

			/*
			 * everything ok, return true
			 */
			return true;
		} catch(IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	private static void writeMethodForInput(BufferedWriter writer, String inputName, String inputClassName,
			String inputType, String method) throws IOException {

		if(method == null) {
			emptyLine(writer);
			pythonComment(writer, "No Method defined for " + inputClassName + " '" + inputName + "'", 1);
			return;
		}

		emptyLine(writer);

		if(inputName != null) {
			pythonComment(writer, "Method for " + inputClassName + " '" + inputName + "'", 1);
		} else {
			pythonComment(writer, "Method for " + inputClassName, 1);
		}

		/*
		 * buttons dont call methods with parameters, so dont define the method
		 * with a value
		 */
		if(inputType.compareTo(Input.TYPE_BUTTON) == 0) {
			pythonLine(writer, "def " + method + "(self):", 1);
			pythonLine(writer, "print \"Method Call for " + method + "()\"", 2);
		} else {
			pythonLine(writer, "def " + method + "(self, value):", 1);
			pythonLine(writer, "print \"Method Call for " + method + "(\" + str(value) + \")\"", 2);
		}

		pythonComment(writer, "TODO implement functionality", 2);
		pythonLine(writer, "return True", 2);
	}

	/**
	 * helper method, calls {@link #emptyLines(BufferedWriter, int)} with line
	 * parameter 1
	 * 
	 * @param writer
	 * @throws IOException
	 */
	private static void emptyLine(BufferedWriter writer) throws IOException {
		emptyLines(writer, 1);
	}

	/**
	 * writes as many empty lines as specified to the writer
	 */
	private static void emptyLines(BufferedWriter writer, int lines) throws IOException {
		for(int i = 0; i < lines; i++)
			writer.write("\n");
	}

	/**
	 * helper method, calls {@link #pythonComment(BufferedWriter, String, int)}
	 * with the indentation 0
	 */
	private static void pythonComment(BufferedWriter writer, String string) throws IOException {
		pythonComment(writer, string, 0);
	}

	/**
	 * helper method, calls {@link #pythonLine(BufferedWriter, String, int)}
	 * with the indentation 0
	 */
	private static void pythonLine(BufferedWriter writer, String string) throws IOException {
		pythonLine(writer, string, 0);
	}

	/**
	 * writes the given string to the {@link BufferedWriter} and adds as many
	 * tabulator spaces in front as indicated by the given indentation, plus
	 * adds the indicator for a one line comment (#)
	 * 
	 */
	private static void pythonComment(BufferedWriter writer, String string, int indent) throws IOException {
		pythonLine(writer, "# " + string, indent);
	}

	/**
	 * writes the given string to the {@link BufferedWriter} and adds as many
	 * tabulator spaces in front as indicated by the given indentation
	 * 
	 */
	private static void pythonLine(BufferedWriter writer, String string, int indent) throws IOException {
		for(int i = 0; i < indent; i++) {
			string = "\t" + string;
		}
		System.out.println("'" + string + "'");
		writer.write(string + "\n");
	}

}
