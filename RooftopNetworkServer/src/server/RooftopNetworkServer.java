package server;

import java.io.File;
import java.io.IOException;
import java.net.BindException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.xmlrpc.XmlRpcException;
import org.json.JSONObject;

import server.configuration.InputMap;
import server.rpc.RpcClient;
import server.rpc.RpcHandlerScriptGenerator;
import server.thread.ServerMainThread;
import server.user.UserManagement;
import share.inputs.InputFactory;

/*
 * TODO implement user management with rights
 * 
 * TODO app requests values, server requests values from the rpc server and updates the database
 * 
 * TODO clientcommunicationthread ger�t in endlosschleife wenn client verbindung abbricht
 */
/**
 * class that starts the {@link ServerMainThread} and connects to the database
 * and the rpc server
 * 
 * @author Philipp Hempel
 *
 */
public class RooftopNetworkServer {

	/**
	 * the first port on which the server will listen, if the port is not free,
	 * the next port will be used, until a free port is found
	 */
	private static int port = 4444;
	/**
	 * the main thread of the server which will receive new connections from
	 * clients
	 */
	private static ServerMainThread thread;
	/**
	 * instance of the user management class
	 */
	public static UserManagement userManagement;
	/**
	 * a json object that contains the inputs the client can control
	 */
	public static JSONObject configurationObject;
	/**
	 * the socket of this server
	 */
	private static ServerSocket server;
	/**
	 * saves all valid input groups and their inputs, a json representation of
	 * the map is sent to the app
	 */
	public static InputMap inputMap;
	/**
	 * the time the server will wait for a client to send a message before the
	 * connection will be closed. 0 = no timeout
	 */
	public static final int SERVER_TIMEOUT = 0;
	/**
	 * the maximum of times a message that could not be received correctly will
	 * be rerequested
	 */
	public static final int MAX_REQUESTS = 3;
	/**
	 * the relative path to the folder which contains the images and drawings
	 * used in the app
	 */
	public static final String IMAGE_PATH = "config/images/";
	/**
	 * the request handler of the rpc server
	 */
	public static String RPC_HANDLER = "";
	/**
	 * the http url of the rpc server
	 */
	private static String RPC_URL = "";
	/**
	 * the url of the database
	 */
	public static String DB_URL = "";
	/**
	 * the name of the database
	 */
	public static String DB_NAME = "rooftopserverdatabase";
	/**
	 * the name of the table with the users
	 */
	public static final String DB_TABLE_USERS = "user";
	/**
	 * the name of the table with the input groups
	 */
	public static final String DB_TABLE_GROUPS = "inputgroup";
	/**
	 * the name of the table with the user rights
	 */
	public static final String DB_TABLE_USER_ACCESS_RIGHTS = "access";
	/**
	 * the username of the database
	 */
	public static final String DB_USERNAME = "root";
	/**
	 * the password of the database
	 */
	public static String DB_PASSWORD = "";
	/**
	 * the client that communicates with the rpc server
	 */
	private static RpcClient rpcClient;
	/**
	 * command line argument for the port
	 */
	private final static String OPT_PORT = "port";
	/**
	 * command line argument for the rpc url
	 */
	private final static String OPT_RPC_URL = "rpcu";
	/**
	 * command line argument for the rpc request handler
	 */
	private final static String OPT_RPC_HANDLER = "rpch";
	/**
	 * command line argument for the database url
	 */
	private final static String OPT_DB_URL = "dbu";
	/**
	 * command line argument for the database password
	 */
	private final static String OPT_DB_PW = "dbp";
	/**
	 * command line argument that indicates that a new python file with a rpc
	 * request handler will be generated under the given path. will only work if
	 * a name for the handler is specified by the option
	 * {@link #OPT_RPC_HANDLER}
	 */
	private final static String OPT_PYTHON = "py";
	/**
	 * command line flag that will print a list of parameters to the console
	 */
	private final static String FLAG_HELP = "help";
	/**
	 * list of command line options (where e.g the string 'option' from this
	 * array stands for the command line argument '-option value')
	 */
	private final static String[] commandLineOptions = { OPT_DB_PW, OPT_DB_URL, OPT_PORT, OPT_RPC_HANDLER, OPT_RPC_URL,
			OPT_PYTHON };
	/**
	 * list of command line flag options (where e.g the string 'flag' from this
	 * array stands for the command line argument '--flag')
	 */
	private final static String[] commandLineFlags = { FLAG_HELP };
	/**
	 * regular expression that checks if a string matches a correct ip followed
	 * by a portnumber
	 */
	private static final String IP_REGEX = "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\:[0-9]+";
	/**
	 * {@link HashMap} that holds the values of the options from
	 * {@link #commandLineOptions} and {@link #commandLineFlags} if they are set
	 */
	public static HashMap<String, String> commandLineValues = new HashMap<String, String>();
	/**
	 * indicates whether a python script will be generated with the rpc request
	 * handler methods
	 */
	private static boolean generateRequestHandlerScript = false;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		/*
		 * analyze parameters
		 */
		for(int i = 0; i < args.length; i++) {
			String arg = args[i];
			/*
			 * look for flag arguments (starting with --)
			 */
			if(arg.startsWith("--")) {
				for(int k = 0; k < commandLineFlags.length; k++) {
					if(commandLineFlags[k].compareTo(arg.substring(2, arg.length())) == 0) {
						commandLineValues.put(commandLineFlags[k], "");
						k = commandLineFlags.length;
					}
				}
			}
			/*
			 * look for option arguments (starting with - and followed by a
			 * value)
			 */
			if(arg.startsWith("-")) {
				for(int k = 0; k < commandLineOptions.length; k++) {
					if(commandLineOptions[k].compareTo(arg.substring(1, arg.length())) == 0) {
						if(i < args.length - 1) {
							if(!args[i + 1].startsWith("-")) {
								commandLineValues.put(commandLineOptions[k], args[i + 1]);
								k = commandLineOptions.length;
								i++;
							}
						}
					}
				}
			}
		}
		/*
		 * set the options specified by the parameters if they are correct
		 */
		for(String key : commandLineValues.keySet()) {
			String val = commandLineValues.get(key);
			if(val.length() > 0) {

				/*
				 * analyze options
				 */
				switch (key){

				case OPT_DB_URL:
					/*
					 * check if the value is a correct url (ip:port)
					 */
					if(val.matches(IP_REGEX)) {
						DB_URL = val;
						log("Changed the URL of the database to '" + DB_URL + "'");
					}
					break;

				case OPT_DB_PW:
					DB_PASSWORD = val;
					log("Set the database password to '" + DB_PASSWORD + "'");
					break;

				case OPT_PORT:
					/*
					 * check if the value is a correct port number
					 */
					if(val.matches("[0-9]+")) {
						port = Integer.parseInt(val);
						log("Changed the Server Port to '" + port + "'");
					}
					break;

				case OPT_RPC_HANDLER:
					/*
					 * check if the value is a correct python class name
					 */
					if(val.matches("([A-Z]+[a-zA-Z0-9]*)+")) {
						RPC_HANDLER = val;
						log("Changed the Request Handler of the RPC Server to '" + RPC_HANDLER + "'");
					}
					break;

				case OPT_RPC_URL:
					/*
					 * check if the value is a correct url (ip:port)
					 */
					if(val.matches(IP_REGEX)) {
						RPC_URL = val;
						log("Changed the URL of the RPC Server to '" + RPC_URL + "'");
					}
					break;

				case OPT_PYTHON:
					/*
					 * check if the value is a python file path
					 */
					if(val.matches("(\\.\\.){0,1}(\\.\\.\\/)*([/]{0,1}[a-zA-Z_]+[a-zA-Z0-9_]*)+\\.py")) {
						log("Will generate a Python Script for the RPC Request Handler at '" + val + "'");
						generateRequestHandlerScript = true;
					}
					break;
				}
			} else {
				log("Command Line Flag " + key + " is set");

				/*
				 * analyze flags
				 */
				switch (key){

				case FLAG_HELP:
					log("Available Options: "
							+ OPT_DB_URL
							+ " - IP and Port (IP:Port) of the Database, "
							+ OPT_DB_PW
							+ " - Password for User root of the Database, "
							+ OPT_PORT
							+ " - Port of the server, "
							+ OPT_RPC_URL
							+ " - IP and Port (IP:Port) of the RPC Server, "
							+ OPT_RPC_HANDLER
							+ " - Name of the Request Handler Class of the RPC Server, "
							+ OPT_PYTHON
							+ " - File Name for a new Python File with the RPC Request Handler Class (also needs Option -"
							+ OPT_RPC_HANDLER + "). Available Flags: " + FLAG_HELP + " - Display this list");
					break;
				}
			}
		}

		/*
		 * initiate and test the input map
		 */
		try {
			log("Starting the connection to the Input Map Database...");
			inputMap = new InputMap();
			configurationObject = inputMap.loadFromDatabase();
		} catch(SQLException e) {
			error("Input Map Error: " + e.getMessage());
			e.printStackTrace();
			log("Server will not start.");
			return;
		}

		if(generateRequestHandlerScript) {
			/*
			 * generate a python file from the input map
			 */
			RpcHandlerScriptGenerator.generateRpcHandler(InputFactory
					.createInputGroupListFromJson(configurationObject), RPC_HANDLER,
					new File(commandLineValues.get(OPT_PYTHON)));
		}

		/*
		 * initiate and test the rpc client
		 */
		try {
			log("Starting the connection to the RPC Server...");
			rpcClient = new RpcClient("http://" + RPC_URL + (!RPC_URL.endsWith("/") ? "/" : "") + RPC_HANDLER);
			if(executeBoolResult("checkConnection")) {
				log("Calling methods on the RPC Server works!");
			} else {
				error("Calling methods on the RPC Server failed!");
				return;
			}

		} catch(XmlRpcException | ConnectException | MalformedURLException e) {
			error("RPC Client Error: " + e.getMessage());
			e.printStackTrace();
			log("Server will not start.");
			return;
		}

		/*
		 * initiate and test the user management
		 */
		try {
			log("Starting the connection to the User Management Database...");
			userManagement = new UserManagement(DB_URL);
			userManagement.loadFromDatabase();
		} catch(NullPointerException | SQLException e) {
			error("User Management Error: " + e.getMessage());
			e.printStackTrace();
			log("Server will not start.");
			return;
		}

		/*
		 * try to start the server with the first free port beginning from the
		 * given start port
		 */
		try {
			log("Starting the Server Socket...");
			for(int i = port; !startServer(i); i++)
				;
		} catch(IOException e) {
			error("Server Error: " + e.getMessage());
			e.printStackTrace();
			log("Server will not start.");
			return;
		}

		log("Starting the Main Thread...");
		thread = new ServerMainThread(server);
		thread.start();
	}

	/**
	 * convinient method for {@link #executeBoolResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @throws XmlRpcException
	 */
	public static boolean executeBoolResult(String method) throws XmlRpcException {
		return executeBoolResult(method, new ArrayList<String>());
	}

	/**
	 * convinient method for {@link #executeBoolResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @param param
	 *            the single parameter
	 * @throws XmlRpcException
	 */
	public static boolean executeBoolResult(String method, String param) throws XmlRpcException {
		ArrayList<String> params = new ArrayList<String>();
		params.add(param);
		return executeBoolResult(method, params);

	}

	/**
	 * convinient method for {@link #executeBoolResult(String, List)}
	 */
	public static boolean executeBoolResult(String method, String[] params) throws XmlRpcException {
		return executeBoolResult(method, toArrayList(params));
	}

	/**
	 * executes the given method with the given parameters in the rpc server.
	 * calls {@link RpcClient}
	 * 
	 * @return the boolean value of the result of the call
	 * @param method
	 *            the method name
	 * @param params
	 *            the {@link List} of parameters
	 * @throws XmlRpcException
	 */
	public static boolean executeBoolResult(String method, List<String> params) throws XmlRpcException {

		return rpcClient.executeBoolResult(method, params);
	}

	/**
	 * convinient method for {@link #executeStringResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @throws XmlRpcException
	 */
	public static String executeStringResult(String method) throws XmlRpcException {
		return executeStringResult(method, new ArrayList<String>());
	}

	/**
	 * convinient method for {@link #executeStringResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @param param
	 *            the single parameter
	 * @throws XmlRpcException
	 */
	public static String executeStringResult(String method, String param) throws XmlRpcException {
		ArrayList<String> params = new ArrayList<String>();
		params.add(param);
		return executeStringResult(method, params);

	}

	/**
	 * convinient method for {@link #executeStringResult(String, List)}
	 */
	public static String executeStringResult(String method, String[] params) throws XmlRpcException {
		return executeStringResult(method, toArrayList(params));
	}

	/**
	 * executes the given method with the given parameters in the rpc server.
	 * calls {@link RpcClient}
	 * 
	 * @return the {@link String} value of the result of the call
	 * @param method
	 *            the method name
	 * @param params
	 *            the {@link List} of parameters
	 * @throws XmlRpcException
	 */
	public static String executeStringResult(String method, List<String> params) throws XmlRpcException {

		return rpcClient.executeStringResult(method, params);
	}

	/**
	 * convinient method for {@link #executeStringResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @throws XmlRpcException
	 */
	public static double executeNumberResult(String method) throws XmlRpcException {
		return executeNumberResult(method, new ArrayList<String>());
	}

	/**
	 * convinient method for {@link #executeNumberResult(String, List)}
	 * 
	 * @param method
	 *            the method name
	 * @param param
	 *            the single parameter
	 * @throws XmlRpcException
	 */
	public static double executeNumberResult(String method, String param) throws XmlRpcException {
		ArrayList<String> params = new ArrayList<String>();
		params.add(param);
		return executeNumberResult(method, params);

	}

	/**
	 * convinient method for {@link #executeNumberResult(String, List)}
	 */
	public static double executeNumberResult(String method, String[] params) throws XmlRpcException {
		return executeNumberResult(method, toArrayList(params));
	}

	/**
	 * executes the given method with the given parameters in the rpc server.
	 * calls {@link RpcClient}
	 * 
	 * @return the double value of the result of the call
	 * 
	 * @param method
	 *            the method name
	 * @param params
	 *            the {@link List} of parameters
	 * @throws XmlRpcException
	 */
	public static double executeNumberResult(String method, List<String> params) throws XmlRpcException {

		return rpcClient.executeNumberResult(method, params);
	}

	/**
	 * puts the content of the array params into an {@link ArrayList}
	 * 
	 * @return {@link ArrayList} of {@link String} with the same content in the
	 *         same order as in the given {@link String} array
	 */
	private static ArrayList<String> toArrayList(String[] params) {
		if(params == null)
			return null;

		ArrayList<String> list = new ArrayList<String>();
		for(String p : params) {
			list.add(p);
		}

		return list;
	}

	/**
	 * helper method that prints the given message to the console and to the
	 * window, if it exists
	 * 
	 * @param message
	 */
	public static void log(String message) {
		log(message, RooftopNetworkServer.class.getSimpleName());
	}

	public static void log(String message, String who) {
		message = "[" + who + "]\n\t" + message;

		System.out.println(message);
	}

	/**
	 * helper method that prints the given error to the console and to the
	 * window, if it exists
	 * 
	 * @param message
	 */
	public static void error(String message) {
		error(message, RooftopNetworkServer.class.getSimpleName());
	}

	public static void error(String message, String who) {
		message = "[" + who + "]\n\tERROR\t" + message;

		System.err.println(message);
	}

	/**
	 * helper method that starts a new socket at the given port. if the port is
	 * not free, this method is called recursively with the port number
	 * incremented by 1, until a free port is found
	 * 
	 * @param port
	 * @throws IOException
	 */
	private static boolean startServer(int port) throws IOException {
		try {
			server = new ServerSocket(port);
			return true;
		} catch(BindException e) {
			error("Could not create a Socket at Port " + port);
			return false;
		}
	}
}